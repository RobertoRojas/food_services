var Config = require('/libs/Config');
//var db = require('/mods/db');

function push() {
}

push.register = function(e) {

};

push.OLDregister = function(e) {

	function sendToken(token) {
		var xhr = Ti.Network.createHTTPClient({
			onload : function() {
				Ti.API.debug("[TiPush] Token sent to our backend");
			},
			onerror : function() {
				Ti.API.error("[TiPush] Can't send tokend to our backend");
			},
			timeout : 20000
		});
		xhr.open('POST', Config.SERVER_BASE_URL + 'push_subscribe');
		xhr.setRequestHeader('ubqti_api_version', Config.ubqti_api_version);
		xhr.setRequestHeader('ubqti_api_token', db.selectKEYVAL('ubqti_api_token'));
		xhr.send({
			device_type : Config.device_type,
			device_id : token
		});
	}

	function androidClick(data) {

		Ti.API.log('ANDROID CLICK');
		Ti.API.log(JSON.stringify(data));
		// if (data.chatRoomId) {
		// Ti.API.error('data.chatRoomId: ' + data.chatRoomId);
		// Ti.API.error('data.chatRoomName: ' + data.chatRoomName);
		// var room = {
		// id : data.chatRoomId,
		// name : data.chatRoomName
		// };
		// var Window = require('/ui/Chat');
		// new Window(null, null, null, room);
		// }

	}


	Config.TiGoosh.registerForPushNotifications({
		callback : function(e) {
			Ti.API.debug("[TiPush]:", JSON.stringify(e));
			var data = JSON.parse(e.data || '');
			if (e.inBackground) {
				androidClick(data);
			} else {
				Ti.API.log(JSON.stringify(e));
			}
		},
		success : function(e) {
			Ti.API.debug('[TiPush] TOKEN:', e.deviceToken);
			sendToken(e.deviceToken);
		},
		error : function(err) {
			Ti.API.debug('[TiPush] ERROR:', err);
		}
	});

};

module.exports = push;
