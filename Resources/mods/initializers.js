var Config = require('/libs/Config');
var gps = require('com.ubqti.android');
var db = require('/mods/db');
var moment = require('/libs/moment');
moment.locale(Ti.Locale.currentLanguage);

function init() {
}

init.startService = function() {
	gps.startGps();
};

init.refreshCallbackService = function(callback) {
	gps.registerCallbacks({
		refresh : callback
	});
};

init.stopService = function() {
	gps.stopGps();
};

init.newAudit = function() {

	var last_loc = db.selectKEYVAL('last_loc');
	if (last_loc == '') {
		last_loc = null;
	} else {
		last_loc = JSON.parse(last_loc);
	}

	var audit = {
		filled_date : '',
		filled_lat : '',
		filled_lon : '',
		filled_accu : '',

		sent_date : '',
		sent_lat : '',
		sent_lon : '',
		sent_accu : '',

		user_sent_date : '',
		user_sent_lat : '',
		user_sent_lon : '',
		user_sent_accu : '',

		first_validation_date : '',
		first_validation_lat : '',
		first_validation_lon : '',
		first_validation_accu : '',

		first_view_date : moment.utc().valueOf(),
		first_view_lat : (last_loc != null) ? last_loc.lat : '',
		first_view_lon : (last_loc != null) ? last_loc.lon : '',
		first_view_accu : (last_loc != null) ? last_loc.acc : ''
	};

	return audit;

};

init.newMiniAudit = function() {

	var last_loc = db.selectKEYVAL('last_loc');
	if (last_loc == '') {
		last_loc = null;
	} else {
		last_loc = JSON.parse(last_loc);
	}

	var audit = {
		user_sent_date : moment.utc().valueOf(),
		user_sent_lat : (last_loc != null) ? last_loc.lat : '',
		user_sent_lon : (last_loc != null) ? last_loc.lon : '',
		user_sent_accu : (last_loc != null) ? last_loc.acc : ''
	};

	return audit;

};

module.exports = init;
