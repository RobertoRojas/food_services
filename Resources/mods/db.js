var Config = require('/libs/Config');
var pictures = require('/mods/gallery');
var moment = require('/libs/moment');
moment.locale(Ti.Locale.currentLanguage);

exports.checkDB = function() {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	// TIMESTAMP:  moment.utc().valueOf());
	// UTC DATE:   moment.utc().format().toString();
	// LOCAL DATE: moment.utc().local().format().toString();
	// Ti.API.log('last_loc: ' + db.selectKEYVAL('last_loc'));

	// this.dropFORMS();
	// this.dropREADYTOSEND();

	db.execute('CREATE TABLE IF NOT EXISTS keyval (key TEXT PRIMARY KEY, val TEXT);');
	db.execute('CREATE TABLE IF NOT EXISTS general (id INTEGER PRIMARY KEY, code TEXT, name TEXT, json_values TEXT);');
	db.execute('CREATE TABLE IF NOT EXISTS form_masters (id TEXT PRIMARY KEY, name TEXT, identifier TEXT, forder INTEGER, json_form TEXT);');
	db.execute('CREATE TABLE IF NOT EXISTS resources (id TEXT PRIMARY KEY, sku TEXT, product_name TEXT, category_type TEXT, category_name TEXT);');
	db.execute('CREATE TABLE IF NOT EXISTS routes (id TEXT PRIMARY KEY, qc_id TEXT, qm_id TEXT, json_values TEXT);');
	db.execute('CREATE TABLE IF NOT EXISTS tasks (id TEXT PRIMARY KEY, route_id TEXT, stop INTEGER, priority INTEGER, json_values TEXT);');
	db.execute('CREATE TABLE IF NOT EXISTS forms (form_id INTEGER PRIMARY KEY, task_id TEXT, route_id TEXT, priority INTEGER, type TEXT, json_audit TEXT, json_values TEXT, sent INTEGER, last_updated TEXT);');
	db.execute('CREATE TABLE IF NOT EXISTS readytosend (id INTEGER PRIMARY KEY, ref_id TEXT, request_method TEXT, request_url TEXT, json_data TEXT, file TEXT, created_at TEXT);');
	db.execute('CREATE TABLE IF NOT EXISTS geoloc (id INTEGER PRIMARY KEY, type TEXT, json_data TEXT, process_date TEXT, created_at TEXT);');

	this.insertKEYVAL(Config.KEYVAL_INIT);
	this.updateKEYVAL(Config.KEYVAL_STATIC);

	this.cleanBD();

	db.close();
};

exports.cleanBD = function() {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	var limit = moment.utc().subtract(15, 'days').format().toString();
	var old = [];

	var res = db.execute('SELECT form_id, last_updated FROM forms');
	while (res.isValidRow()) {
		if (moment.utc(res.fieldByName('last_updated')).isBefore(limit)) {
			old.push(res.fieldByName('form_id'));
		}
		res.next();
	}

	for (var index in old) {
		var form = this.selectFORMSbyID(old[index]);
		var _taskId = (form.task != null) ? form.task : 'travelAlert';
		var _formType = form.type;

		var files = [];

		for (var f in form.values) {
			for (var stage in form.values[f]) {
				if ( typeof (form.values[f][stage]) != 'undefined' && form.values[f][stage] != null) {
					var _gallery = pictures.files(_taskId, _formType, stage);
					if (_gallery) {
						for (var g in _gallery) {
							files.push(_gallery[g]);
						}
					}
				}
			}
		}

		if (files.length > 0) {
			for (var _f in files) {
				var _file = Ti.Filesystem.getFile(files[_f]);
				if (_file.exists()) {
					_file.deleteFile();
				}
			}
		}

		db.execute('DELETE FROM forms WHERE form_id = ?', old[index]);
	}

	db.execute('DELETE FROM geoloc WHERE process_date = ?', '');

	res.close();
	db.close();
};

exports.dropBD = function() {
	var db = Ti.Database.open(Config.DATABASE_NAME);
	db.execute('DROP TABLE IF EXISTS general');
	db.execute('DROP TABLE IF EXISTS form_masters');
	db.execute('DROP TABLE IF EXISTS resources');
	db.execute('DROP TABLE IF EXISTS routes');
	db.execute('DROP TABLE IF EXISTS tasks');
	db.execute('DROP TABLE IF EXISTS forms');
	db.close();
};

// KEYVAL

exports.dropKEYVAL = function() {
	var db = Ti.Database.open(Config.DATABASE_NAME);
	db.execute('DROP TABLE IF EXISTS keyval');
	db.execute('CREATE TABLE IF NOT EXISTS keyval (key TEXT PRIMARY KEY, val TEXT);');
	db.close();

	if (Config.mode == 0) {
		Ti.API.warn('db.dropKEYVAL');
	}
};

exports.selectKEYVAL = function(key) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	var val = null;

	var res = db.execute('SELECT key, val FROM keyval WHERE key = ?', key);
	while (res.isValidRow()) {
		val = res.fieldByName('val');
		res.next();
	}

	res.close();
	db.close();

	if (Config.mode == 0) {
		Ti.API.info('db.selectKEYVAL: ' + key + ' => ' + val);
	}

	return val;
};

exports.insertKEYVAL = function(params) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	for (var key in params) {
		var val = params[key];

		var res = db.execute('SELECT val FROM keyval WHERE key = ?', key);

		if (res.rowCount == 0) {
			if (Config.mode == 0) {
				Ti.API.info('db.insertKEYVAL newval: ' + key + ' => ' + val);
			}

			db.execute('INSERT INTO keyval (key, val) VALUES (?,?)', key, val);
		} else {
			if (Config.mode == 0) {
				while (res.isValidRow()) {
					val = res.fieldByName('val');
					res.next();
				}
				Ti.API.info('db.insertKEYVAL oldval: ' + key + ' => ' + val);
			}
		}

		res.close();
	}

	db.close();
};

exports.updateKEYVAL = function(params) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	for (var key in params) {
		var val = params[key];

		if (Config.mode == 0) {
			Ti.API.info('db.updateKEYVAL: ' + key + ' => ' + val);
		}

		db.execute('UPDATE keyval SET val = ? WHERE key = ?', val, key);
	}

	db.close();
};

exports.deleteKEYVAL = function(key) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	if (Config.mode == 0) {
		Ti.API.info('db.deleteKEYVAL: ' + key);
	}

	db.execute('DELETE FROM keyval WHERE key = ?', key);

	db.close();
};

// FORM MASTERS

exports.dropFORMMASTERS = function() {
	var db = Ti.Database.open(Config.DATABASE_NAME);
	db.execute('DROP TABLE IF EXISTS form_masters');
	db.execute('CREATE TABLE IF NOT EXISTS form_masters (id TEXT PRIMARY KEY, name TEXT, identifier TEXT, forder INTEGER, json_form TEXT);');
	db.close();

	if (Config.mode == 0) {
		Ti.API.warn('db.dropFORMMASTERS');
	}
};

exports.selectFORMMASTERSbyID = function(id) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	var element = null;

	var res = db.execute('SELECT id, name, identifier, forder, json_form FROM form_masters WHERE id = ?', id);
	while (res.isValidRow()) {
		element = {
			id : res.fieldByName('id'),
			name : res.fieldByName('name'),
			identifier : res.fieldByName('identifier'),
			forder : res.fieldByName('forder'),
			form : JSON.parse(res.fieldByName('json_form'))
		};
		res.next();
	}

	res.close();
	db.close();

	if (Config.mode == 0) {
		Ti.API.info('db.selectFORMMASTERSbyID: ' + id);
	}

	return element;
};

exports.selectFORMMASTERSbyIDENTIFIER = function(identifier) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	var element = null;

	var res = db.execute('SELECT id, name, identifier, forder, json_form FROM form_masters WHERE identifier = ?', identifier);
	while (res.isValidRow()) {
		element = {
			id : res.fieldByName('id'),
			name : res.fieldByName('name'),
			identifier : res.fieldByName('identifier'),
			forder : res.fieldByName('forder'),
			form : JSON.parse(res.fieldByName('json_form'))
		};
		res.next();
	}

	res.close();
	db.close();

	if (Config.mode == 0) {
		Ti.API.info('db.selectFORMMASTERSbyIDENTIFIER: ' + identifier);
	}

	return element;
};

exports.selectFORMMASTERSasARRAY = function() {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	var result = [];

	var res = db.execute('SELECT id, name, identifier, forder, json_form FROM form_masters');
	while (res.isValidRow()) {
		var element = {
			id : res.fieldByName('id'),
			name : res.fieldByName('name'),
			identifier : res.fieldByName('identifier'),
			forder : res.fieldByName('forder'),
			form : JSON.parse(res.fieldByName('json_form'))
		};
		result.push(element);
		res.next();
	}

	res.close();
	db.close();

	if (Config.mode == 0) {
		Ti.API.info('db.selectFORMMASTERSasARRAY: ' + result.length);
	}

	return result;
};

exports.selectFORMMASTERSasOBJECT = function() {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	var result = {};

	var res = db.execute('SELECT id, name, identifier, forder, json_form FROM form_masters');
	while (res.isValidRow()) {
		var element = {
			id : res.fieldByName('id'),
			name : res.fieldByName('name'),
			identifier : res.fieldByName('identifier'),
			forder : res.fieldByName('forder'),
			form : JSON.parse(res.fieldByName('json_form'))
		};
		result[res.fieldByName('id')] = element;
		res.next();
	}

	res.close();
	db.close();

	if (Config.mode == 0) {
		Ti.API.info('db.selectFORMMASTERSasOBJECT: ' + Object.keys(result).length);
	}

	return result;
};

exports.insertFORMMASTERS = function(params) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	db.execute('INSERT INTO form_masters (id, name, identifier, forder, json_form) VALUES (?,?,?,?,?)', params['id'], params['name'], params['identifier'], params['forder'], JSON.stringify(params['form']));

	db.close();
};

exports.updateFORMMASTERS = function(params) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	db.execute('UPDATE form_masters SET name = ?, identifier = ?, forder = ?, json_form = ? WHERE id = ?', params['name'], params['identifier'], params['forder'], JSON.stringify(params['form']), params['id']);

	db.close();
};

exports.deleteFORMMASTERS = function(id) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	db.execute('DELETE FROM form_masters WHERE id = ?', id);

	db.close();
};

// RESOURCES

exports.dropRESOURCES = function() {
	Ti.App.Properties.setList('resources', []);
};

exports.selectRESOURCES = function() {
	return Ti.App.Properties.getList('resources', []);
};

// GENERAL

exports.dropGENERAL = function() {
	var db = Ti.Database.open(Config.DATABASE_NAME);
	db.execute('DROP TABLE IF EXISTS general');
	db.execute('CREATE TABLE IF NOT EXISTS general (id INTEGER PRIMARY KEY, code TEXT, name TEXT, json_values TEXT);');
	db.close();

	if (Config.mode == 0) {
		Ti.API.warn('db.dropGENERAL');
	}
};

exports.selectGENERAL = function(code) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	var element = null;

	var res = db.execute('SELECT id, code, name, json_values FROM general WHERE code = ?', code);
	while (res.isValidRow()) {
		element = {
			id : res.fieldByName('id'),
			code : res.fieldByName('code'),
			name : res.fieldByName('name'),
			values : JSON.parse(res.fieldByName('json_values'))
		};
		res.next();
	}

	res.close();
	db.close();

	if (Config.mode == 0) {
		Ti.API.info('db.selectGENERAL: ' + code);
	}

	return element;
};

exports.insertGENERAL = function(params) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	if (Config.mode == 0) {
		Ti.API.info('db.insertGENERAL: ' + params['code']);
	}

	db.execute('INSERT INTO general (code, name, json_values) VALUES (?,?,?)', params['code'], params['name'], JSON.stringify(params['values']));

	db.close();
};

exports.updateGENERAL = function(params) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	if (Config.mode == 0) {
		Ti.API.info('db.updateGENERAL: ' + params['code']);
	}

	db.execute('UPDATE general SET code = ?, name = ?, json_values = ? WHERE code = ?', params['code'], params['name'], JSON.stringify(params['values']), params['code']);

	db.close();
};

exports.deleteGENERAL = function(code) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	if (Config.mode == 0) {
		Ti.API.info('db.deleteGENERAL: ' + JSON.stringify(code));
	}

	db.execute('DELETE FROM general WHERE code = ?', code);

	db.close();
};

// ROUTES

exports.dropROUTES = function() {
	var db = Ti.Database.open(Config.DATABASE_NAME);
	db.execute('DROP TABLE IF EXISTS routes');
	db.execute('CREATE TABLE IF NOT EXISTS routes (id TEXT PRIMARY KEY, qc_id TEXT, qm_id TEXT, json_values TEXT);');
	db.close();

	if (Config.mode == 0) {
		Ti.API.warn('db.dropROUTES');
	}
};

exports.selectROUTESbyID = function(id) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	var element = null;

	var res = db.execute('SELECT id, qc_id, qm_id, json_values FROM routes WHERE id = ?', id);
	while (res.isValidRow()) {
		element = JSON.parse(res.fieldByName('json_values'));
		res.next();
	}

	res.close();
	db.close();

	if (Config.mode == 0) {
		Ti.API.info('db.selectROUTESbyID: ' + id);
	}

	return element;
};

exports.selectROUTESasARRAY = function() {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	var result = [];

	var pos = 0;
	var res = db.execute('SELECT id, qc_id, qm_id, json_values FROM routes ORDER BY id ASC');
	while (res.isValidRow()) {
		var element = JSON.parse(res.fieldByName('json_values'));
		var newTasks = [];
		for (var t in element.tasks) {
			if (moment().local().isBefore(moment(element.tasks[t].available_to).local())) {
				var forms_res = db.execute('SELECT task_id FROM forms WHERE task_id = ? AND priority = ? AND type IN (\'manifest\', \'rejection\') AND sent > 0', element.tasks[t].id, element.tasks[t].priority);
				if (!forms_res.isValidRow()) {
					newTasks.push(element.tasks[t].id);
				}
			}
		}
		if (newTasks.length > 0) {
			element.tasks = newTasks;
			element.position = pos;
			result.push(element);
			pos++;
		}
		res.next();
	}

	res.close();
	db.close();

	if (Config.mode == 0) {
		Ti.API.info('db.selectROUTESasARRAY: ' + result.length);
	}

	return result;
};

exports.selectROUTESasOBJECT = function() {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	var result = {};

	var pos = 0;
	var res = db.execute('SELECT id, qc_id, qm_id, json_values FROM routes ORDER BY id ASC');
	while (res.isValidRow()) {
		var element = JSON.parse(res.fieldByName('json_values'));
		var newTasks = [];
		for (var t in element.tasks) {
			if (moment().local().isBefore(moment(element.tasks[t].available_to).local())) {
				var forms_res = db.execute('SELECT task_id FROM forms WHERE task_id = ? AND priority = ? AND type IN (\'manifest\', \'rejection\') AND sent > 0', element.tasks[t].id, element.tasks[t].priority);
				if (!forms_res.isValidRow()) {
					newTasks.push(element.tasks[t].id);
				}
			}
		}
		if (newTasks.length > 0) {
			element.tasks = newTasks;
			element.position = pos;
			result[res.fieldByName('id')] = element;
			pos++;
		}
		res.next();
	}

	res.close();
	db.close();

	if (Config.mode == 0) {
		Ti.API.info('db.selectROUTESasOBJECT: ' + Object.keys(result).length);
	}

	return result;
};

exports.insertROUTES = function(params) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	if (Config.mode == 0) {
		Ti.API.info('db.insertROUTES: ' + params['id']);
	}

	db.execute('INSERT INTO routes (id, qc_id, qm_id, json_values) VALUES (?,?,?,?)', params['id'], params['qc_id'], params['qm_id'], JSON.stringify(params));

	db.close();
};

exports.updateROUTES = function(params) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	if (Config.mode == 0) {
		Ti.API.info('db.updateROUTES: ' + params['id']);
	}

	db.execute('UPDATE routes SET qc_id = ?, qm_id = ?, json_values = ? WHERE id = ?', params['qc_id'], params['qm_id'], JSON.stringify(params), params['id']);

	db.close();
};

exports.deleteROUTES = function(params) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	if (Config.mode == 0) {
		Ti.API.info('db.deleteROUTES: ' + params['id']);
	}

	db.execute('DELETE FROM routes WHERE id = ?', params['id']);

	db.close();
};

// TASKS

exports.dropTASKS = function() {
	var db = Ti.Database.open(Config.DATABASE_NAME);
	db.execute('DROP TABLE IF EXISTS tasks');
	db.execute('CREATE TABLE IF NOT EXISTS tasks (id TEXT PRIMARY KEY, route_id TEXT, stop INTEGER, priority INTEGER, json_values TEXT);');
	db.close();

	if (Config.mode == 0) {
		Ti.API.warn('db.dropTASKS');
	}
};

exports.loadTASKSbyROUTE = function(route_id) {
	this.dropTASKS();
	var route = this.selectROUTESbyID(route_id);
	var tasks = route.tasks;
	for (var t in tasks) {
		tasks[t].route_id = route_id;
		this.insertTASKS(tasks[t]);
	}
};

exports.selectTASKSbyID = function(task_id) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	var element = null;

	var res = db.execute('SELECT id, route_id, stop, priority, json_values FROM tasks WHERE id = ? AND NOT EXISTS (SELECT type FROM forms WHERE forms.task_id = tasks.id AND forms.priority = tasks.priority AND type IN (\'manifest\', \'rejection\') AND sent > 0)', task_id);
	while (res.isValidRow()) {
		element = JSON.parse(res.fieldByName('json_values'));
		res.next();
	}

	res.close();
	db.close();

	if (element != null) {
		var forms = this.selectFORMSbyTASKasOBJECT(task_id);
		element.filled_forms = forms;
	}

	if (Config.mode == 0) {
		Ti.API.info('db.selectTASKSbyID: ' + task_id);
	}

	return element;
};

exports.selectTASKSbyROUTEasARRAY = function(route_id) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	var result = [];

	var pos = 0;
	var res = db.execute('SELECT id, route_id, stop, priority, json_values FROM tasks WHERE route_id = ? AND NOT EXISTS (SELECT type FROM forms WHERE forms.task_id = tasks.id AND forms.priority = tasks.priority AND type IN (\'manifest\', \'rejection\') AND sent > 0) ORDER BY priority DESC, stop ASC', route_id);
	while (res.isValidRow()) {
		var element = JSON.parse(res.fieldByName('json_values'));
		element.position = pos;
		result.push(element);
		pos++;
		res.next();
	}

	res.close();
	db.close();

	if (Config.mode == 0) {
		Ti.API.info('db.selectTASKSbyROUTEasARRAY: ' + result.length);
	}

	return result;
};

exports.selectTASKSbyROUTEasOBJECT = function(route_id) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	var result = {};

	var pos = 0;
	var res = db.execute('SELECT id, route_id, stop, priority, json_values FROM tasks WHERE route_id = ? AND NOT EXISTS (SELECT type FROM forms WHERE forms.task_id = tasks.id AND forms.priority = tasks.priority AND type IN (\'manifest\', \'rejection\') AND sent > 0) ORDER BY priority DESC, stop ASC', route_id);
	while (res.isValidRow()) {
		var element = JSON.parse(res.fieldByName('json_values'));
		element.position = pos;
		result[res.fieldByName('id')] = element;
		pos++;
		res.next();
	}

	res.close();
	db.close();

	if (Config.mode == 0) {
		Ti.API.info('db.selectTASKSbyROUTEasOBJECT: ' + Object.keys(result).length);
	}

	return result;
};

exports.insertTASKS = function(params) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	if (Config.mode == 0) {
		Ti.API.info('db.insertTASKS: ' + params['id']);
	}

	db.execute('INSERT INTO tasks (id, route_id, stop, priority, json_values) VALUES (?,?,?,?,?)', params['id'], params['route_id'], params['stop'], params['priority'], JSON.stringify(params));

	db.close();
};

exports.updateTASKS = function(params) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	if (Config.mode == 0) {
		Ti.API.info('db.updateTASKS: ' + params['task_id']);
	}

	db.execute('UPDATE tasks SET stop = ?, priority = ?, json_values = ? WHERE id = ?', params['stop'], params['priority'], JSON.stringify(params), params['id']);

	db.close();
};

exports.deleteTASKS = function(params) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	if (Config.mode == 0) {
		Ti.API.info('db.deleteTASKS: ' + params['id']);
	}

	db.execute('DELETE FROM tasks WHERE id = ?', params['id']);

	db.close();
};

exports.orderTASKS = function(order) {

	var db = Ti.Database.open(Config.DATABASE_NAME);

	if (Config.mode == 0) {
		Ti.API.info('db.orderTASKS: ' + JSON.stringify(order));
	}

	for (var i = 0; i < order.length; i++) {
		db.execute('UPDATE tasks SET stop = ? WHERE id = ?', i, order[i]);
	}

	db.close();

};

// FORMS

exports.dropFORMS = function() {
	var db = Ti.Database.open(Config.DATABASE_NAME);
	db.execute('DROP TABLE IF EXISTS forms');
	db.execute('CREATE TABLE IF NOT EXISTS forms (form_id INTEGER PRIMARY KEY, task_id TEXT, route_id TEXT, priority INTEGER, type TEXT, json_audit TEXT, json_values TEXT, sent INTEGER, last_updated TEXT);');
	db.close();

	if (Config.mode == 0) {
		Ti.API.warn('db.dropFORMS');
	}
};

exports.selectFORMSbyID = function(form_id) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	var element = null;

	var res = db.execute('SELECT form_id, task_id, route_id, priority, type, json_audit, json_values, sent, last_updated FROM forms WHERE form_id = ?', form_id);
	while (res.isValidRow()) {
		element = {
			form_id : res.fieldByName('form_id'),
			task_id : res.fieldByName('task_id'),
			route_id : res.fieldByName('route_id'),
			priority : res.fieldByName('priority'),
			type : res.fieldByName('type'),
			audit : JSON.parse(res.fieldByName('json_audit')),
			values : JSON.parse(res.fieldByName('json_values')),
			sent : res.fieldByName('sent')
		};
		res.next();
	}

	res.close();
	db.close();

	if (Config.mode == 0) {
		Ti.API.info('db.selectFORMSbyID: ' + form_id);
	}

	return element;
};

exports.selectFORMSbyVALUES = function(task_id, route_id, priority, type) {
	var init = require('/mods/initializers');

	var db = Ti.Database.open(Config.DATABASE_NAME);

	var element = null;

	var res = db.execute('SELECT form_id, task_id, route_id, priority, type, json_audit, json_values, sent, last_updated FROM forms WHERE task_id = ? AND route_id = ? AND priority = ? AND type = ?', task_id, route_id, priority, type);
	while (res.isValidRow()) {
		element = {
			form_id : res.fieldByName('form_id'),
			task_id : res.fieldByName('task_id'),
			route_id : res.fieldByName('route_id'),
			priority : res.fieldByName('priority'),
			type : res.fieldByName('type'),
			audit : JSON.parse(res.fieldByName('json_audit')),
			values : JSON.parse(res.fieldByName('json_values')),
			sent : res.fieldByName('sent')
		};
		res.next();
	}

	res.close();
	db.close();

	if (element == null) {

		var params = {
			task_id : task_id,
			route_id : route_id,
			priority : priority,
			type : type,
			audit : init.newAudit(),
			values : [],
			sent : 0
		};

		this.insertFORMS(params);

		element = this.selectFORMSbyVALUES(task_id, route_id, priority, type);

	}

	if (Config.mode == 0) {
		Ti.API.info('db.selectFORMSbyVALUES: ' + task_id + ', ' + route_id + ', ' + priority + ', ' + type);
	}

	return element;
};

exports.checkFORMSbyVALUES = function(task_id, route_id, priority, types) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	missing = [];
	for (var t in types) {
		missing.push(types[t]);
	}

	for (var t in types) {
		var type = types[t];
		var res = db.execute('SELECT form_id, task_id, route_id, priority, type, json_audit, json_values, sent, last_updated FROM forms WHERE sent = 1 AND task_id = ? AND route_id = ? AND priority = ? AND type = ?', task_id, route_id, priority, type);
		while (res.isValidRow()) {
			if (missing.indexOf(type) != -1) {
				missing.splice(missing.indexOf(type), 1);
			}
			res.next();
		}
		res.close();
	}

	db.close();

	if (Config.mode == 0) {
		Ti.API.info('db.checkFORMSbyVALUES: ' + JSON.stringify(missing));
	}

	return missing;
};

exports.selectFORMSbyTASKasARRAY = function(task_id) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	var result = [];

	var res = db.execute('SELECT form_id, task_id, route_id, priority, type, json_audit, json_values, sent, last_updated FROM forms WHERE task_id = ? ORDER BY type ASC', task_id);
	while (res.isValidRow()) {
		var element = {
			form_id : res.fieldByName('form_id'),
			task_id : res.fieldByName('task_id'),
			route_id : res.fieldByName('route_id'),
			priority : res.fieldByName('priority'),
			type : res.fieldByName('type'),
			audit : JSON.parse(res.fieldByName('json_audit')),
			values : JSON.parse(res.fieldByName('json_values')),
			sent : res.fieldByName('sent')
		};
		result.push(element);
		res.next();
	}

	res.close();
	db.close();

	if (Config.mode == 0) {
		Ti.API.info('db.selectFORMSbyTASKasARRAY: ' + result.length);
	}

	return result;
};

exports.selectFORMSbyTASKasOBJECT = function(task_id) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	var result = {};

	var res = db.execute('SELECT form_id, task_id, route_id, priority, type, json_audit, json_values, sent, last_updated FROM forms WHERE task_id = ? ORDER BY type ASC', task_id);
	while (res.isValidRow()) {
		var element = {
			form_id : res.fieldByName('form_id'),
			task_id : res.fieldByName('task_id'),
			route_id : res.fieldByName('route_id'),
			priority : res.fieldByName('priority'),
			type : res.fieldByName('type'),
			audit : JSON.parse(res.fieldByName('json_audit')),
			values : JSON.parse(res.fieldByName('json_values')),
			sent : res.fieldByName('sent')
		};
		result[res.fieldByName('type')] = element;
		res.next();
	}

	res.close();
	db.close();

	if (Config.mode == 0) {
		Ti.API.info('db.selectFORMSbyTASKasOBJECT: ' + Object.keys(result).length);
	}

	return result;
};

exports.insertFORMS = function(params) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	if (Config.mode == 0) {
		Ti.API.info('db.insertFORMS: ' + params['task_id'] + ', ' + params['route_id'] + ', ' + params['priority'] + ', ' + params['type']);
	}

	db.execute('INSERT INTO forms (task_id, route_id, priority, type, json_audit, json_values, sent, last_updated) VALUES (?,?,?,?,?,?,?,?)', params['task_id'], params['route_id'], params['priority'], params['type'], JSON.stringify(params['audit']), JSON.stringify(params['values']), 0, moment.utc().format().toString());

	db.close();
};

exports.updateFORMS = function(params) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	if (Config.mode == 0) {
		Ti.API.info('db.updateFORMS: ' + params['task_id'] + ', ' + params['route_id'] + ', ' + params['priority'] + ', ' + params['type']);
	}

	db.execute('UPDATE forms SET task_id = ?, route_id = ?, priority = ?, type = ?, json_audit = ?, json_values = ?, sent = ?, last_updated = ? WHERE form_id = ?', params['task_id'], params['route_id'], params['priority'], params['type'], JSON.stringify(params['audit']), JSON.stringify(params['values']), params['sent'], moment.utc().format().toString(), params['form_id']);

	db.close();
};

exports.deleteFORMS = function(params) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	if (Config.mode == 0) {
		Ti.API.info('db.deleteFORMS: ' + params['task_id'] + ', ' + params['route_id'] + ', ' + params['priority'] + ', ' + params['type']);
	}

	db.execute('DELETE FROM forms WHERE form_id = ?', params['form_id']);

	db.close();
};

exports.deleteFORMSbyVALUES = function(task_id, route_id, priority, type) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	if (Config.mode == 0) {
		Ti.API.info('db.deleteFORMSbyVALUES: ' + task_id + ', ' + route_id + ', ' + priority + ', ' + type);
	}

	var form = this.selectFORMSbyVALUES(task_id, route_id, priority, type);
	if (form.values.images) {
		for (var _f in form.values.images) {
			var _file = Ti.Filesystem.getFile(form.values.images[_f]);
			if (_file.exists()) {
				_file.deleteFile();
			}
		}
	}

	db.execute('DELETE FROM forms WHERE task_id = ? AND route_id = ? AND priority = ? AND type = ?', task_id, route_id, priority, type);

	db.close();
};

// READYTOSEND

exports.dropREADYTOSEND = function() {
	var db = Ti.Database.open(Config.DATABASE_NAME);
	db.execute('DROP TABLE IF EXISTS readytosend');
	db.execute('CREATE TABLE IF NOT EXISTS readytosend (id INTEGER PRIMARY KEY, ref_id TEXT, request_method TEXT, request_url TEXT, json_data TEXT, file TEXT, created_at TEXT);');
	db.close();

	if (Config.mode == 0) {
		Ti.API.warn('db.dropREADYTOSEND');
	}
};

exports.selectREADYTOSENDasARRAY = function() {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	var result = [];

	var res = db.execute('SELECT id, ref_id, request_method, request_url, json_data, file, created_at FROM readytosend ORDER BY id ASC');
	while (res.isValidRow()) {
		var element = {
			id : res.fieldByName('id'),
			ref_id : res.fieldByName('ref_id'),
			request_method : res.fieldByName('request_method'),
			request_url : res.fieldByName('request_url'),
			data : JSON.parse(res.fieldByName('json_data')),
			file : res.fieldByName('file'),
			created_at : res.fieldByName('created_at')
		};
		result.push(element);
		res.next();
	}

	res.close();
	db.close();

	if (Config.mode == 0) {
		Ti.API.info('db.selectREADYTOSENDasARRAY: ' + result.length);
	}

	return result;
};

exports.insertREADYTOSEND = function(params) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	if (Config.mode == 0) {
		Ti.API.info('db.insertREADYTOSEND: ' + params['request_method'] + ' ' + params['request_url']);
	}

	db.execute('INSERT INTO readytosend (ref_id, request_method, request_url, json_data, file, created_at) VALUES (?,?,?,?,?,?)', params['ref_id'], params['request_method'], params['request_url'], JSON.stringify(params['data']), params['file'], moment.utc().format().toString());

	db.close();
};

exports.sendForm = function(form) {
	var last_loc = this.selectKEYVAL('last_loc');
	if (last_loc == '') {
		last_loc = null;
	} else {
		last_loc = JSON.parse(last_loc);
	}
	form.audit.user_sent_date = moment.utc().valueOf();
	form.audit.user_sent_lat = (last_loc != null) ? last_loc.lat : '';
	form.audit.user_sent_lon = (last_loc != null) ? last_loc.lon : '';
	form.audit.user_sent_accu = (last_loc != null) ? last_loc.acc : '';

	form.sent = 1;

	form.task = form.audit.task;
	form.form = form.audit.form;
	delete form.audit.task;
	delete form.audit.form;

	var filepath = '';
	var files = [];

	var _taskId = (form.task != null) ? form.task : 'travelAlert';
	var _formType = form.type;

	for (var f in form.values) {
		for (var stage in form.values[f]) {
			if ( typeof (form.values[f][stage]) != 'undefined' && form.values[f][stage] != null) {
				var _gallery = pictures.files(_taskId, _formType, stage);
				if (_gallery) {
					for (var g in _gallery) {
						files.push(_gallery[g]);
						if (Array.isArray(form.values[f][stage])) {
							if (form.values[f][stage].length > 0) {
								if (!form.values[f][stage][0].img) {
									form.values[f][stage][0].img = [];
								}
								form.values[f][stage][0].img.push(_gallery[g]);
							}
						} else {
							if (!form.values[f][stage].img) {
								form.values[f][stage].img = [];
							}
							form.values[f][stage].img.push(_gallery[g]);
						}
					}
				}
			}
		}
	}

	this.updateFORMS(form);

	if (files.length > 0) {
		var Compression = require('ti.compression');
		var name = moment.utc().valueOf();
		var result = Compression.zip(name + '.zip', files);
		var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, name + '.zip');
		filepath = file.getNativePath().toString().replace('file://', '');

		if (form.type == 'global_alert' || form.type == 'manifest') {
			Ti.API.log('DELETE FILES: ' + form.type);
			for (var _f in files) {
				var _file = Ti.Filesystem.getFile(files[_f]);
				if (_file.exists()) {
					_file.deleteFile();
				}
			}
		}
	}

	var _old = form.values;
	var _new = [];
	for (var _v in _old) {
		if (_old[_v][Object.keys(_old[_v])[0]] != null) {
			_new.push(_old[_v]);
		}
	}
	form.values = _new;

	var ready = {
		ref_id : form.task_id,
		request_method : 'POST',
		request_url : Config.SERVER_BASE_URL + 'form_user',
		file : filepath,
		data : form
	};

	Ti.API.log(JSON.stringify(ready));

	this.insertREADYTOSEND(ready);

	if (form.type == 'global_alert') {
		this.deleteFORMS(form);
	}
};

exports.validateRoutes = function(routes) {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	var newRoutes = [];

	for (var _r in routes) {

		var _route = routes[_r];
		var _tasks = _route.tasks;
		var newTasks = [];

		for (var _t in _tasks) {

			var _task = _tasks[_t];

			var res = db.execute('SELECT type FROM forms WHERE forms.task_id = ? AND forms.priority = ? AND type IN (\'manifest\', \'rejection\') AND sent > 0', _task.task_id, _task.priority);
			while (res.isValidRow()) {
				delete _tasks[_t];
				res.next();
			}
			res.close();

			if (_tasks[_t]) {
				newTasks.push(_tasks[_t]);
			}

		}

		routes[_r].tasks = newTasks;

		if (newTasks.length > 0) {
			newRoutes.push(routes[_r]);
		}

	}

	db.close();
	return newRoutes;
};

exports.selectLOCasARRAY = function() {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	var result = [];

	var res = db.execute('SELECT id, type, json_data, process_date, created_at FROM geoloc ORDER BY id ASC');
	while (res.isValidRow()) {
		var element = {
			id : res.fieldByName('id'),
			type : res.fieldByName('type'),
			data : JSON.parse(res.fieldByName('json_data')),
			process_date : res.fieldByName('process_date'),
			created_at : res.fieldByName('created_at')
		};
		result.push(element);
		res.next();
	}

	res.close();
	db.close();

	if (Config.mode == 0) {
		Ti.API.info('db.selectLOCasARRAY: ' + result.length);
	}

	return result;
};

exports.selectFORMSasARRAY = function() {
	var db = Ti.Database.open(Config.DATABASE_NAME);

	var result = [];

	var res = db.execute('SELECT form_id, task_id, route_id, priority, type, json_audit, json_values, sent, last_updated FROM forms ORDER BY type ASC');
	while (res.isValidRow()) {
		var element = {
			form_id : res.fieldByName('form_id'),
			task_id : res.fieldByName('task_id'),
			route_id : res.fieldByName('route_id'),
			priority : res.fieldByName('priority'),
			type : res.fieldByName('type'),
			audit : JSON.parse(res.fieldByName('json_audit')),
			values : JSON.parse(res.fieldByName('json_values')),
			sent : res.fieldByName('sent')
		};
		result.push(element);
		res.next();
	}

	res.close();
	db.close();

	if (Config.mode == 0) {
		Ti.API.info('db.selectFORMSbyTASKasARRAY: ' + result.length);
	}

	return result;
};
