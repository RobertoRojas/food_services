var Config = require('/libs/Config');
var init = require('/mods/initializers');
var db = require('/mods/db');
var moment = require('/libs/moment');
moment.locale(Ti.Locale.currentLanguage);

exports.queues = function(callback) {

	var me = Ti.App.Properties.getObject('me', null);

	if (Config.mode) {
		Ti.API.debug('xhr.queues url: ' + Config.SERVER_BASE_URL + 'queue?v=' + Config.androidCode + '&p=' + me.phone);
		Ti.API.debug('xhr.queues: ' + JSON.stringify(params));
	}
	var client = Ti.Network.createHTTPClient({
		onload : function(e) {
			if (Config.mode == 0) {
				Ti.API.info('xhr.queues: ' + this.responseText);
			}
			var json = JSON.parse(this.responseText);
			if (json.status.code == '401') {
				Ti.App.Properties.setObject('me', null);
				var dialog = Ti.UI.createAlertDialog({
					title : 'Sesión expirada',
					message : 'Su sesión ha expirado o ha entrado a su cuenta desde otro dispositivo',
					buttonNames : ['Salir']
				});
				dialog.addEventListener('click', function(e) {
					db.updateKEYVAL(Config.KEYVAL_RESET);
					db.dropBD();
					Ti.App.Properties.removeAllProperties();
					init.stopService();
					Config.drawer.close();
				});
				dialog.show();
			} else {
				callback(json);
			}
		},
		onerror : function(e) {
			Ti.API.debug(e.error);
			callback(false);
		},
		timeout : 20000
	});

	//theUrl = Config.SERVER_BASE_URL + "queue?v=" + Config.androidCode + '&p=' + me.phone;
	client.open('GET', Config.SERVER_BASE_URL + 'queue?v=' + Config.androidCode + '&p=' + me.phone);
	client.setRequestHeader('ubqti_api_version', Config.ubqti_api_version);
	client.setRequestHeader('ubqti_api_token', Config.token);
	client.send();
};

exports.formMaster = function(callback) {
	var client = Ti.Network.createHTTPClient({
		onload : function(e) {
			var json = JSON.parse(this.responseText);
			if (json.status.code == '401') {
				Ti.App.Properties.setObject('me', null);
				var dialog = Ti.UI.createAlertDialog({
					title : 'Sesión expirada',
					message : 'Su sesión ha expirado o ha entrado a su cuenta desde otro dispositivo',
					buttonNames : ['Salir']
				});
				dialog.addEventListener('click', function(e) {
					db.updateKEYVAL(Config.KEYVAL_RESET);
					db.dropBD();
					Ti.App.Properties.removeAllProperties();
					init.stopService();
					Config.drawer.close();
				});
				dialog.show();
			} else {
				callback(json);
			}
		},
		onerror : function(e) {
			Ti.API.debug(e.error);
			callback(false);
		},
		timeout : 20000
	});

	client.open('GET', Config.SERVER_BASE_URL + 'form_master');
	client.setRequestHeader('ubqti_api_version', Config.ubqti_api_version);
	client.setRequestHeader('ubqti_api_token', db.selectKEYVAL('ubqti_api_token'));
	client.send();
};

exports.resource = function(callback) {
	var client = Ti.Network.createHTTPClient({
		onload : function(e) {
			var json = JSON.parse(this.responseText);
			if (json.status.code == '401') {
				Ti.App.Properties.setObject('me', null);
				var dialog = Ti.UI.createAlertDialog({
					title : 'Sesión expirada',
					message : 'Su sesión ha expirado o ha entrado a su cuenta desde otro dispositivo',
					buttonNames : ['Salir']
				});
				dialog.addEventListener('click', function(e) {
					db.updateKEYVAL(Config.KEYVAL_RESET);
					db.dropBD();
					Ti.App.Properties.removeAllProperties();
					init.stopService();
					Config.drawer.close();
				});
				dialog.show();
			} else {
				callback(json);
			}
		},
		onerror : function(e) {
			Ti.API.debug(e.error);
			callback(false);
		},
		timeout : 20000
	});

	client.open('GET', Config.SERVER_BASE_URL + 'resource');
	client.setRequestHeader('ubqti_api_version', Config.ubqti_api_version);
	client.setRequestHeader('ubqti_api_token', db.selectKEYVAL('ubqti_api_token'));
	client.send();
};

exports.form_users = function(callback, task_id) {
	var client = Ti.Network.createHTTPClient({
		onload : function(e) {
			if (Config.mode) {
				Ti.API.info('xhr.form_users: ' + this.responseText);
			}
			var json = JSON.parse(this.responseText);
			if (json.status.code == '401') {
				Ti.App.Properties.setObject('me', null);
				var dialog = Ti.UI.createAlertDialog({
					title : 'Sesión expirada',
					message : 'Su sesión ha expirado o ha entrado a su cuenta desde otro dispositivo',
					buttonNames : ['Salir']
				});
				dialog.addEventListener('click', function(e) {
					db.updateKEYVAL(Config.KEYVAL_RESET);
					db.dropBD();
					Ti.App.Properties.removeAllProperties();
					init.stopService();
					Config.drawer.close();
				});
				dialog.show();
			} else {
				callback(json);
			}
		},
		onerror : function(e) {
			Ti.API.debug(e.error);
			callback(false);
		},
		timeout : 20000
	});

	client.open('GET', Config.SERVER_BASE_URL + 'form_user?task=' + task_id);
	client.setRequestHeader('ubqti_api_version', Config.ubqti_api_version);
	client.setRequestHeader('ubqti_api_token', db.selectKEYVAL('ubqti_api_token'));
	client.send();
};

exports.register_device = function(callback, params) {
	if (Config.mode) {
		Ti.API.debug('xhr.register url: ' + Config.SERVER_BASE_URL + 'register_device/');
		Ti.API.debug('xhr.register params: ' + JSON.stringify(params));
	}
	var client = Ti.Network.createHTTPClient({
		onload : function(e) {
			if (Config.mode) {
				Ti.API.info('xhr.login: ' + this.responseText);
			}
			var json = JSON.parse(this.responseText);
			callback(json);
		},
		onerror : function(e) {
			Ti.API.debug(e.error);
			callback(false);
		},
		timeout : 20000
	});
	client.open('POST', Config.SERVER_BASE_URL + 'register_device/');
	client.setRequestHeader('ubqti_api_version', Config.ubqti_api_version);
	client.send(params);
};

exports.login = function(callback, params) {
	if (Config.mode) {
		Ti.API.error('xhr.login url: ' + Config.SERVER_BASE_URL + 'login/');
		Ti.API.error('xhr.login params: ' + JSON.stringify(params));
	}
	var client = Ti.Network.createHTTPClient({
		onload : function(e) {
			if (Config.mode) {
				Ti.API.info('xhr.login: ' + this.responseText);
			}
			var json = JSON.parse(this.responseText);
			callback(json);
		},
		onerror : function(e) {
			Ti.API.debug(e.error);
			callback(false);
		},
		timeout : 20000
	});
	client.open('POST', Config.SERVER_BASE_URL + 'login/');
	client.setRequestHeader('ubqti_api_version', Config.ubqti_api_version);
	client.send(params);
};

exports.distribution = function(url, callback) {
	if (Config.mode) {
		Ti.API.error('xhr.distribution: ' + Config.SERVER_BASE_URL + url);
	}
	var client = Ti.Network.createHTTPClient({
		onload : function(e) {
			var json = JSON.parse(this.responseText);
			if (json.status.code == '401') {
				Ti.App.Properties.setObject('me', null);
				var dialog = Ti.UI.createAlertDialog({
					title : 'Sesión expirada',
					message : 'Su sesión ha expirado o ha entrado a su cuenta desde otro dispositivo',
					buttonNames : ['Salir']
				});
				dialog.addEventListener('click', function(e) {
					db.updateKEYVAL(Config.KEYVAL_RESET);
					db.dropBD();
					Ti.App.Properties.removeAllProperties();
					init.stopService();
					Config.drawer.close();
				});
				dialog.show();
			} else {
				callback(json);
			}
		},
		onerror : function(e) {
			Ti.API.debug(e.error);
			callback(false);
		},
		timeout : 20000
	});
	client.open('GET', Config.SERVER_BASE_URL + url);
	client.setRequestHeader('ubqti_api_version', Config.ubqti_api_version);
	client.setRequestHeader('ubqti_api_token', db.selectKEYVAL('ubqti_api_token'));
	client.send();
};

exports.app_version = function(callback) {
	var client = Ti.Network.createHTTPClient({
		onload : function(e) {
			var json = JSON.parse(this.responseText);
			Ti.App.Properties.setObject('app_version_2', json.appversion[0]);
			callback(true);
		},
		onerror : function(e) {
			Ti.API.debug(e.error);
			callback(false);
		},
		timeout : 20000
	});
	client.open('GET', Config.DOMAIN_URL + 'app/version.json');
	client.setRequestHeader('ubqti_api_version', Config.ubqti_api_version);
	client.send();
};

exports.pin_recovery = function(callback, params) {
	//if (Config.mode) {

	Ti.API.error('xhr.register TOKEN: ' + db.selectKEYVAL('ubqti_api_token'));
	Ti.API.error('xhr.register PARAMS: ' + params);
	Ti.API.error('xhr.register URL: ' + Config.SERVER_BASE_URL + 'pin_recovery');
	//}
	var client = Ti.Network.createHTTPClient({
		onload : function(e) {
			if (Config.mode) {
				Ti.API.info('xhr.login: ' + this.responseText);
			}
			
			var json = JSON.parse(this.responseText);
			if (json.status.code == '401') {
				Ti.App.Properties.setObject('me', null);
				var dialog = Ti.UI.createAlertDialog({
					title : 'Sesión expirada',
					message : 'Su sesión ha expirado o ha entrado a su cuenta desde otro dispositivo',
					buttonNames : ['Salir']
				});
				dialog.addEventListener('click', function(e) {
					db.updateKEYVAL(Config.KEYVAL_RESET);
					db.dropBD();
					Ti.App.Properties.removeAllProperties();
					init.stopService();
					Config.drawer.close();
				});
				dialog.show();
			} else {
				callback(json);
			}
		},
		onerror : function(e) {
			Ti.API.debug(e.error);
			callback(false);
		},
		timeout : 20000
	});
	client.open('POST', Config.SERVER_BASE_URL + 'pin_recovery');
	client.setRequestHeader('ubqti_api_version', Config.ubqti_api_version);
	client.setRequestHeader('ubqti_api_token', db.selectKEYVAL('ubqti_api_token'));
	client.send(params);
};
