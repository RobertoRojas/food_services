// Copyright Stephen Feather and other contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
// Based upon https://www.waze.com/dev/documentation/

exports.NavigateTo = function(latitude, longitude) {
	var url = 'waze://?ll=' + latitude + ',' + longitude + '&navigate=yes';
	if (Ti.Android) {
		try {
			Ti.API.info('Trying to Launch via Intent');
			var intent = Ti.Android.createIntent({
				action : Ti.Android.ACTION_VIEW,
				data : url

			});
			Ti.Android.currentActivity.startActivity(intent);
		} catch (e) {
			Ti.API.info('Caught Error launching intent: ' + e);
			exports.Install();
		}
	} else {
		if (Ti.Platform.canOpenURL(url)) {
			Ti.Platform.openURL(url);
		} else {
			exports.Install();
		}

	}

};

exports.Search = function(query) {
	var url = 'waze://?q=' + query;
	if (Ti.Android) {
		try {
			Ti.API.info('Trying to Launch via Intent');
			var intent = Ti.Android.createIntent({
				action : Ti.Android.ACTION_VIEW,
				data : url

			});
			Ti.Android.currentActivity.startActivity(intent);
		} catch (e) {
			Ti.API.info('Caught Error launching intent');
			exports.install();
		}
	} else {
		if (Ti.Platform.canOpenURL(url)) {
			Ti.Platform.openURL(url);
		} else {
			exports.install();
		}

	}
};

exports.Install = function() {
	var alert = Ti.UI.createAlertDialog({
		title : L('dialog_atention'),
		message : "Para continuar tiene que instalar waze",
		buttonNames : ["OK", "Cancelar"],
		cancel : 1
	});

	alert.addEventListener("click", function(_event) {
		if (_event.index === 0) {
			var installURL;
			if (Ti.Android) {
				installURL = 'market://details?id=com.waze';
			} else {
				installurl = 'http://itunes.apple.com/us/app/id323229106';
			}
			Ti.API.info("Installing Waze application");
			Ti.Platform.openURL(installURL);
		} else {
			Ti.API.info("User aborted Waze installation");
		}
	});

	alert.show();
};

