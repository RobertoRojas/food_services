var Config = require('/libs/Config');
var xhr = require('/mods/xhr');
var db = require('/mods/db');

function sync() {
}

sync.VERSION_DELAY = 1200000;
// 20 minutos
sync.version_timerId = null;

sync.FORMS_DELAY = 300000;
// 5 minutos: nos sirve para verificar que la sesion sigue activa
sync.forms_timerId = null;

sync.RES_DELAY = 3600000;
// 60 minutos
sync.res_timerId = null;

sync.start = function() {

	if (sync.version_timerId) {
		clearInterval(sync.version_timerId);
	}

	if (sync.forms_timerId) {
		clearInterval(sync.forms_timerId);
	}

	if (sync.res_timerId) {
		clearInterval(sync.res_timerId);
	}

	setTimeout(function() {
		xhr.app_version(sync.save_version);
	}, 1000);

	setTimeout(function() {
		xhr.formMaster(sync.save_forms);
	}, 10000);

	setTimeout(function() {
		xhr.resource(sync.save_res);
	}, 20000);

	sync.version_timerId = setInterval(function() {
		xhr.app_version(sync.save_version);
	}, sync.VERSION_DELAY);

	sync.forms_timerId = setInterval(function() {
		xhr.formMaster(sync.save_forms);
	}, sync.FORMS_DELAY);

	sync.res_timerId = setInterval(function() {
		xhr.resource(sync.save_res);
	}, sync.RES_DELAY);

};

sync.stop = function() {

	if (sync.version_timerId) {
		clearInterval(sync.version_timerId);
	}

	if (sync.forms_timerId) {
		clearInterval(sync.forms_timerId);
	}

	if (sync.res_timerId) {
		clearInterval(sync.res_timerId);
	}

};

sync.save_version = function(status) {
	Ti.API.log('>>>> SAVE VERSION: ' + status);

	var mobileSetting = Ti.App.Properties.getObject('app_version_2', null).mobile;

	if (mobileSetting['validate']) {

		if (mobileSetting['mandatory']) {

			// if (Config.androidCode < mobileSetting['version']) {
// 
				// var dialog = Ti.UI.createAlertDialog({
					// cancel : 1,
					// buttonNames : ['Actualizar App'],
					// message : 'Para continuar debe actualizar la aplicación.',
					// title : 'Atención'
				// });
// 
				// dialog.addEventListener('click', function(e) {
					// Ti.Platform.openURL(mobileSetting['store_url']);
					// Config.drawer.close();
				// });
// 
				// dialog.show();
// 
			// }

		}

	}

};

sync.save_forms = function(result) {

	if (result == false) {
		// timeout
	} else {
		switch(result.status.code) {
		case '200':
			for (index in result.response.data) {
				var formData = result.response.data[index];
				if (db.selectFORMMASTERSbyID(formData.id)) {
					db.updateFORMMASTERS(formData);
				} else {
					db.insertFORMMASTERS(formData);
				}
			}
			break;

		default:
			Ti.API.error(result);
			break;
		}
	}

};

sync.save_res = function(result) {

	if (result == false) {
		// timeout
	} else {
		switch(result.status.code) {
		case '200':
			var resources = result.response.data;
			Ti.App.Properties.setList('resources', resources);
			break;

		default:
			Ti.API.error(result);
			break;
		}
	}

};

module.exports = sync;
