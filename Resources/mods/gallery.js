var Config = require('/libs/Config');

exports.gallery = function(taskId, routeId, priority, formType, stage, view) {

    var container = Ti.UI.createView({
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        touchEnabled : false
    });

    var verticalContainer = Ti.UI.createView({
        height : Ti.UI.SIZE,
        width : Ti.UI.FILL,
        layout : 'vertical',
        touchEnabled : false
    });

    var horizontalContainer = Ti.UI.createView({
        top : '0dp',
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        layout : 'horizontal',
        touchEnabled : false
    });

    updatePictures();

    container.add(verticalContainer);

    container.getTaskPictures = getTaskPictures;
    container.getFilePath = getFilePath;

    return container;

    function getTaskPictures() {
        var picturesState = false;
        var pictures = [];

        for (var i = 0; i <= Config.maxPictures; i++) {

            var nameFile = taskId + '_' + formType + '_' + stage + '_' + i + '.png';
            var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, nameFile);

            if (file.exists()) {
                pictures.push(nameFile);
                picturesState = true;
            }
        }

        if (picturesState) {
            return pictures;
        } else {
            return picturesState;
        }
    }

    function getFilePath() {
        var picturesState = false;
        var pictures = [];

        for (var i = 0; i <= Config.maxPictures; i++) {

            var nameFile = taskId + '_' + formType + '_' + stage + '_' + i + '.png';
            var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, nameFile);

            if (file.exists()) {
                pictures.push(file.nativePath);
                picturesState = true;
            }
        }

        if (picturesState) {
            return pictures;
        } else {
            return picturesState;
        }
    }

    function sortFiles() {

        var currentPictures = getTaskPictures();
        var newPictures = [];

        if (!currentPictures) {
            return 0;
        } else {

            if (currentPictures.length <= Config.maxPictures) {
                for (var i = 0; i < currentPictures.length; i++) {

                    var currentFileName = currentPictures[i];
                    var newFileName = taskId + '_' + formType + '_' + stage + '_' + i + '' + '.png';

                    newPictures.push(newFileName);

                    var currentFile = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, currentFileName);
                    currentFile.rename(newFileName);
                }
            }

            return currentPictures.length;
        }
    }

    function savePicture(picture) {

        var oldPictures = [];
        var fileIndex = sortFiles();
        var fileName = taskId + '_' + formType + '_' + stage + '_' + fileIndex + '.png';

        var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, fileName);

        if (file.exists()) {
            file.deleteFile();
        }

        file.write(picture);

        updatePictures();
    }

    function deleteFile(picture) {

        var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, picture);

        if (file.exists()) {
            file.deleteFile();
        }

        updatePictures();
    }

    function updatePictures() {

        horizontalContainer.removeAllChildren();
        verticalContainer.removeAllChildren();
        view.removeAllChildren();

        var pictures = getTaskPictures();
        var currentPicture = 1;

        if (pictures != false) {
            for (var i = 0; i < pictures.length; i++) {

                horizontalContainer.add(imagePicture(pictures[i]));

                if (currentPicture == 3) {

                    verticalContainer.add(horizontalContainer);

                    if (i != (pictures.length - 1)) {
                        currentPicture = 3;
                    }
                }

                if (i == (pictures.length - 1)) {

                    if (i <= Config.maxPictures) {
                        if (currentPicture < 3) {
                            horizontalContainer.add(defaultPicture());
                        } else if (i < Config.maxPictures) {
                            horizontalContainer.add(defaultPicture());
                        }
                        verticalContainer.add(horizontalContainer);
                    }

                }
                currentPicture++;
            }
        } else {
            horizontalContainer.add(defaultPicture());
            verticalContainer.add(horizontalContainer);
        }

        var textReception = Ti.UI.createView({
            bottom : 0,
            height : '40dp',
            left : '0dp',
            right : '0dp',
        });

        textReception.add(Ti.UI.createLabel({
            height : Ti.UI.SIZE,
            text : 'Adjuntar foto',
            textAlign : 'center',
            color : Config.whiteText,
            font : Config.rowFont,
            height : Ti.UI.SIZE,
            width : Ti.UI.FILL,
            touchEnabled : false
        }));

        verticalContainer.add(textReception);

        container.add(verticalContainer);
        view.add(container);
    }

    function defaultPicture() {

        var container = Ti.UI.createView({
            top : '10dp',
            left : '10dp',
            height : '100dp',
            width : '100dp'
        });

        var picView = Ti.UI.createView({
            top : 0,
            left : 0,
            height : '100dp',
            width : '100dp'
        });

        var picTaken = Ti.UI.createImageView({
            image : Config.images + 'white_camera_small.png',
            borderRadius : '40dp',
            height : '80dp',
            width : '80dp'
        });

        var picFrame = Ti.UI.createView({
            borderRadius : '45dp',
            height : '90dp',
            backgroundColor: Config.button_color,
            width : '90dp'
        });

        var addFrame = Ti.UI.createView({
            right : 0,
            bottom : 0,
            backgroundColor: 'white',
            borderRadius : '15dp',
            height : '30dp',
            width : '30dp'
        });
        
        var addPicture = Ti.UI.createView({
            bottom : 0,
            right : 0,
            height : '30dp',
            width : '30dp',
            backgroundImage : Config.images+ 'accent_add.png'
        });

        picFrame.addEventListener('click', takePicture);
        addFrame.addEventListener('click', takePicture);

        picFrame.add(picTaken);
        picView.add(picFrame);

        addFrame.add(addPicture);
        picView.add(addFrame);

        container.add(picView);

        return container;
    }

    function imagePicture(nameFile) {
        
        var container = Ti.UI.createView({
             top : '10dp',
            left : '10dp',
            height : '100dp',
            width : '100dp'
        });

        var picView = Ti.UI.createView({
           top : 0,
            left : 0,
            height : '100dp',
            width : '100dp'
        });

        var picTaken = Ti.UI.createImageView({
            image : Ti.Filesystem.applicationDataDirectory + nameFile,
            borderRadius : '40dp',
            height : '80dp',
            width : '80dp',
            touchEnabled : false
        });

        var picFrame = Ti.UI.createView({
            url : Ti.Filesystem.applicationDataDirectory + nameFile,
            top : '0dp',
            height : Ti.UI.FILL,
            width : Ti.UI.FILL,
            image : Ti.Filesystem.applicationDataDirectory + nameFile,
            //touchEnabled : false
        });

        var deletePicture = Ti.UI.createView({
            bottom : 0,
            right : 0,
            height : '30dp',
            width : '30dp',
            backgroundImage : Config.images+ 'accent_remove_nb.png',
            touchEnabled: false
        });

        var delFrame = Ti.UI.createView({
            name : nameFile,
            right : 0,
            bottom : 0,
            backgroundColor: 'white',
            borderRadius : '15dp',
            height : '30dp',
            width : '30dp'
        });

        picFrame.addEventListener('click', function(e) {
            fullscreen(e.source.url);
        });

        delFrame.addEventListener('click', function(e) {
            deleteFile(e.source.name);
        });

        picView.add(picTaken);
        picView.add(picFrame);

        delFrame.add(deletePicture);
        picView.add(delFrame);

        container.add(picView);

        return container;
    }

    function takePicture() {

        Ti.Media.showCamera({
            saveToPhotoGallery : false,
            mediaTypes : [Ti.Media.MEDIA_TYPE_PHOTO],
            success : function(event) {
                var newH;
                var newW;
                var eventHeight = event.height;
                var eventWidth = event.width;

                if (eventHeight > eventWidth) {
                    newW = 256;
                    newH = (eventHeight / eventWidth) * newW;
                } else {
                    newH = 256;
                    newW = (eventWidth / eventHeight) * newH;
                }

                var resizedImage = event.media.imageAsResized(newW, newH);

                savePicture(resizedImage);
            },
            cancel : function() {
            },
            error : function(error) {
            },
        });
    }

    function fullscreen(url) {
        
        var self = Ti.UI.createWindow({
            fullscreen : true,
            exitOnClose : false,
            navBarHidden : false,
            windowSoftInputMode : Config.softInput,
            backgroundColor : '#000000'
        });

        var content = Ti.UI.createScrollView({
            showVerticalScrollIndicator : false,
            width : Ti.UI.FILL,
            top : '0dp',
            bottom : '0dp'
        });

        var img = Ti.UI.createImageView({
            image : url,
            height : Ti.UI.SIZE,
            width : Ti.UI.FILL,
            enableZoomControls : true
        });

        content.add(img);
        self.add(img);
        self.open();
    }
};

exports.rename = function(taskId, formType, stage, newType, newStage) {

    var picturesState = false;
    var pictures = [];

    for (var i = 0; i <= Config.maxPictures; i++) {

        var nameFile = taskId + '_' + formType + '_' + stage + '_' + i + '.png';
        var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, nameFile);

        if (file.exists()) {
            var newNameFile = taskId + '_' + newType + '_' + newStage + '_' + i + '.png';
            var newFile = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, newNameFile);
            if (newFile.exists()) {
                newFile.deleteFile();
            }
            file.rename(newNameFile);
        }
    }
};

exports.files = function(taskId, formType, stage) {

    var picturesState = false;
    var pictures = [];

    for (var i = 0; i <= Config.maxPictures; i++) {

        var nameFile = taskId + '_' + formType + '_' + stage + '_' + i + '.png';
        var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, nameFile);

        if (file.exists()) {
            pictures.push(file.nativePath);
            picturesState = true;
        }
    }

    if (picturesState) {
        return pictures;
    } else {
        return picturesState;
    }
};

exports.fullscreen = function(url) {
    
    var self = Ti.UI.createWindow({
        fullscreen : true,
        exitOnClose : false,
        navBarHidden : false,
        windowSoftInputMode : Config.softInput,
        backgroundColor : '#000000'
    });

    var content = Ti.UI.createScrollView({
        showVerticalScrollIndicator : false,
        width : Ti.UI.FILL,
        top : '0dp',
        bottom : '0dp'
    });

    var img = Ti.UI.createImageView({
        image : url,
        height : Ti.UI.SIZE,
        width : Ti.UI.FILL,
        enableZoomControls : true
    });

    var indicator = Ti.UI.createActivityIndicator({
        style : Config.isAndroid ? Ti.UI.ActivityIndicatorStyle.BIG_DARK : Ti.UI.ActivityIndicatorStyle.DARK,
        height : '120dp',
        width : '120dp'
    });
    indicator.show();

    img.addEventListener('load', function(e) {
        indicator.hide();
    });

    content.add(img);
    self.add(img);
    self.add(indicator);

    return self;
};
