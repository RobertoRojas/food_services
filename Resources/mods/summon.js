var Config = require('/libs/Config');

/* Food Service*/

exports.customerRoundtitle = function(id, stop, name, phone) {

    var view = Ti.UI.createView({
        backgroundColor : 'white',
        height : '150dp',
        width : Ti.UI.FILL,
        left : '0dp',
        right : '0dp',
        touchEnabled : false
    });

    var textContent = Ti.UI.createView({
        top : '5dp',
        height : '75dp',
        left : '10dp',
        right : '100dp',
        touchFeedback : Config.touchFeedback,
        touchFeedbackColor : Config.accent,
        layout : 'vertical',
        touchEnabled : true
    });

    var stop = Ti.UI.createLabel({
        left : '0dp',
        text : stop,
        font : Config.subtitleFont,
        color : Config.containerTextColor,
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        touchEnabled : false
    });

    var name = Ti.UI.createLabel({
        left : '0dp',
        text : name,
        font : Config.rowFont,
        color : Config.containerTextColor,
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        touchEnabled : false
    });

    var id = Ti.UI.createLabel({
        left : '0dp',
        text : 'ID: ' + id,
        font : Config.rowFont,
        color : Config.containerTextColor,
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        touchEnabled : false
    });

    var underscore = Ti.UI.createView({
        left : '0dp',
        backgroundColor : Config.orange,
        height : Config.underscoreHeight,
        width : Config.underscoreWidth,
        top : '4dp',
        touchEnabled : false
    });

    var imageContent = Ti.UI.createView({
        top : '80dp',
        //backgroundImage: Config.whiteBg,
        height : '150dp',
        width : '150dp',
        touchEnabled : false
    });

    imageContent.add(Ti.UI.createImageView({
        top : '0dp',
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        image : Config.circle,
        touchEnabled : false
    }));

    imageContent.add(Ti.UI.createImageView({
        top : '30dp',
        height : '40dp',
        width : '40dp',
        image : Config.images + 'white_stop.png',
        touchEnabled : false
    }));

    textContent.add(stop);
    textContent.add(name);
    textContent.add(id);
    textContent.add(underscore);

    view.add(textContent);

    if (phone != null) {
        view.add(buttonOpt(phone));
    }

    view.add(imageContent);

    view.setText = function(text) {
        //view.label.text = text;
    };

    return view;

    function buttonOpt(phone) {

        var container = Ti.UI.createView({
            top : '40dp',
            right : '10dp',
            elevation : Config.elevation,
            height : '30dp',
            width : Ti.UI.FILL,
            //top : '5dp',
            width : '100dp',
            //bottom : '5dp',
            borderWidth : 1,
            backgroundColor : Config.whiteText,
            touchEnabled : false
        });

        var action = Ti.UI.createView({
            backgroundColor : Config.orange,
            borderRadius : 1,
            height : Ti.UI.FILL,
            width : Ti.UI.FILL,
            touchFeedback : Config.touchFeedback,
            touchFeedbackColor : Config.accent,
            touchEnabled : true
        });

        action.addEventListener('click', function() {
            Ti.Platform.openURL(phone);
        });

        action.add(Ti.UI.createLabel({
            text : 'LLAMAR CD',
            textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
            color : Config.whiteText,
            font : Config.rowFont,
            height : Ti.UI.SIZE,
            width : Ti.UI.FILL,
            touchEnabled : false
        }));

        container.add(action);

        return container;
    }

};

exports.roundFsTitle = function(formTitle, backColor, circleImage, iconImage) {

    var _titles = formTitle.split(' ');
    var _title1 = '';
    var _title2 = '';

    for (var i in _titles) {
        if (i < _titles.length / 2) {
            _title1 += _titles[i] + ' ';
        } else {
            _title2 += _titles[i] + ' ';
        }
    }

    var view = Ti.UI.createView({
        backgroundColor : backColor,
        height : '150dp',
        //width : Ti.UI.SIZE,
        left : '0dp',
        right : '0dp',
        touchEnabled : false
    });

    var textContent = Ti.UI.createView({
        top : '15dp',
        height : '75dp',
        width : Ti.UI.SIZE,
        touchFeedback : Config.touchFeedback,
        touchFeedbackColor : Config.accent,
        layout : 'vertical',
        touchEnabled : true
    });

    var subTitle = Ti.UI.createLabel({
        text : _title1.toUpperCase(),
        font : Config.rowFont,
        color : Config.containerTextColor,
        height : Ti.UI.SIZE,
        width : Ti.UI.FILL,
        textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        touchEnabled : false
    });

    var title = Ti.UI.createLabel({
        text : _title2.charAt(0).toUpperCase() + _title2.slice(1),
        font : Config.subtitleFont,
        color : Config.containerTextColor,
        height : Ti.UI.SIZE,
        width : Ti.UI.FILL,
        textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        touchEnabled : false
    });

    textContent.add(subTitle);
    textContent.add(title);

    var underscore = Ti.UI.createView({
        backgroundColor : Config.orange,
        height : Config.underscoreHeight,
        width : Config.underscoreWidth,
        top : '4dp',
        touchEnabled : false
    });

    var imageContent = Ti.UI.createView({
        top : '80dp',
        //backgroundImage: Config.whiteBg,
        height : '150dp',
        width : '150dp',
        touchEnabled : false
    });

    imageContent.add(Ti.UI.createImageView({
        top : '0dp',
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        image : circleImage,
        touchEnabled : false
    }));

    imageContent.add(Ti.UI.createImageView({
        top : '30dp',
        height : '40dp',
        width : '40dp',
        image : iconImage,
        touchEnabled : false
    }));

    textContent.add(underscore);

    view.add(textContent);
    view.add(imageContent);

    view.label1 = subTitle;
    view.label2 = title;

    view.setText = function(text1, text2) {

        Ti.API.info('update tab');
        view.label1.text = text1;
        view.label2.text = text2;
    };

    return view;
};
/*
exports.roundFsTitle = function(title, backImage, iconImage, iconBackColor) {

var _titles = title.split(' ');
var _title1 = '';
var _title2 = '';

for (var i in _titles) {
if (i < _titles.length / 2) {
_title1 += _titles[i] + ' ';
} else {
_title2 += _titles[i] + ' ';
}
}

var view = Ti.UI.createView({
top : '0dp',
height : Ti.UI.FILL,
width : Ti.UI.FILL,
borderWidth: 2,
backgroundColor: 'red',
touchEnabled : true
});

view.add(Ti.UI.createImageView({
top : '0dp',
left : 0,
height : Ti.UI.FILL,
width : Ti.UI.FILL,
image : backImage,
touchEnabled : false
}));

subtitle = this.tabtitle(_title1, _title2);
subtitle.top = '10dp';
subtitle.bottom = '0dp';

view.add(subtitle);

var iconContainer = Ti.UI.createView({
top : '90',
height : '150dp',
width : '150dp',
touchEnabled : true
});

// iconContainer.add(Ti.UI.createImageView({
// top: 0,
// height : Ti.UI.FILL,
// width : Ti.UI.FILL,
// image : Config.circle,
// touchEnabled : false
// }));

iconContainer.add(Ti.UI.createView({
top : '0',
height : '150dp',
width : '150dp',
borderRadius: '75dp',
backgroundColor: iconBackColor,
touchEnabled : true
}));

iconContainer.add(Ti.UI.createImageView({
top : '20dp',
height : '40dp',
width : '40dp',
image : iconImage,
touchEnabled : false
}));

view.add(iconContainer);

return view;
};*/

/*
exports.commonRoundtitle = function(title, backImage, iconImage, iconBackColor) {

var _title1 = '';
var _title2 = '';

for (var i in _titles) {
if (i < _titles.length / 2) {
_title1 += _titles[i] + ' ';
} else {
_title2 += _titles[i] + ' ';
}
}

var view = Ti.UI.createView({
//backgroundImage: Config.whiteBg,
backgroundColor : 'white',
height : '150dp',
width : Ti.UI.FILL,
left : '0dp',
right : '0dp',
touchEnabled : false
});

subtitle = this.tabtitle(_title1, _title2);
subtitle.top = '10dp';
subtitle.bottom = '0dp';

view.add(subtitle);

var underscore = Ti.UI.createView({
left : '0dp',
backgroundColor : Config.orange,
height : Config.underscoreHeight,
width : Config.underscoreWidth,
//top: '4dp',
bottom : '4dp',
touchEnabled : false
});

var imageContent = Ti.UI.createView({
top : '80dp',
//backgroundImage: Config.whiteBg,
height : '150dp',
width : '150dp',
touchEnabled : false
});

imageContent.add(Ti.UI.createImageView({
top : '0dp',
height : Ti.UI.FILL,
width : Ti.UI.FILL,
image : '/images/circle_fs_.png',
touchEnabled : false
}));

imageContent.add(Ti.UI.createImageView({
top : '30dp',
height : '40dp',
width : '40dp',
image : '/images/white_stop.png',
touchEnabled : false
}));

//textContent.add(underscore);

view.add(textContent);

view.add(imageContent);

view.setText = function(text) {
//view.label.text = text;
};

return view;
};
*/
// exports.customerRoundtitle = function(id, stop, name, phone) {
//
// var view = Ti.UI.createView({
// // borderRadius : Config.borderRadius,
// // elevation : Config.elevation,
// //backgroundColor : 'cyan',
// // height : '80',
// backgroundImage: Config.whiteBg,
// height : '80dp',
// width : Ti.UI.FILL,
// left : '10dp',
// right : '10dp',
// touchEnabled : false
// });
//
// var content = Ti.UI.createView({
// //backgroundColor : Config.containerBackgroundColor,
// //backgroundColor : 'yellow',
// height : Ti.UI.FILL,
// left : '0dp',
// right : '100dp',
// touchFeedback : Config.touchFeedback,
// touchFeedbackColor : Config.accent,
// layout : 'vertical',
// touchEnabled : true
// });
//
// var stopView = Ti.UI.createLabel({
// left : '0dp',
// text : stop,
// font : Config.subtitleFont,
// color : Config.containerTextColor,
// height : Ti.UI.SIZE,
// width : Ti.UI.SIZE,
// ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
// wordWrap : false,
// //top : '7dp',
// touchEnabled : false
// });
//
// var nameView = Ti.UI.createLabel({
// left : '0dp',
// text : name,
// font : Config.rowFont,
// color : Config.containerTextColor,
// height : Ti.UI.SIZE,
// width : Ti.UI.SIZE,
// ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
// wordWrap : false,
// //top : '7dp',
// touchEnabled : false
// });
//
// var idView = Ti.UI.createLabel({
// left : '0dp',
// text : 'ID: ' + id,
// font : Config.rowFont,
// color : Config.containerTextColor,
// height : Ti.UI.SIZE,
// width : Ti.UI.SIZE,
// ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
// wordWrap : false,
// // bottom : '12dp',
// touchEnabled : false
// });
//
// var underscore = Ti.UI.createView({
// left : '0dp',
// backgroundColor : Config.orange,
// height : Config.underscoreHeight,
// width : Config.underscoreWidth,
// bottom : '8dp',
// touchEnabled : false
// });
//
// content.add(stopView);
// content.add(nameView);
// content.add(idView);
// //content.add(underscore);
//
// view.add(content);
// view.add(underscore);
// view.add(buttonOpt(phone));
//
// view.label = nameView;
//
// view.setText = function(text) {
// view.label.text = text;
// };
//
// return view;
//
// function buttonOpt(phone) {
//
// var container = Ti.UI.createView({
// right : '10dp',
// elevation : Config.elevation,
// height : '30dp',
// width : Ti.UI.FILL,
// //top : '5dp',
// width : '100dp',
// //bottom : '5dp',
// borderWidth : 1,
// backgroundColor : Config.whiteText,
// touchEnabled : false
// });
//
// var action = Ti.UI.createView({
// backgroundColor : Config.orange,
// height : Ti.UI.FILL,
// width : Ti.UI.FILL,
// touchFeedback : Config.touchFeedback,
// touchFeedbackColor : Config.accent,
// touchEnabled : true
// });
//
// action.addEventListener('click', function() {
// Ti.Platform.openURL(phone);
// });
//
// action.add(Ti.UI.createLabel({
// text : 'LLAMAR CD',
// textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
// color : Config.whiteText,
// font : Config.rowFont,
// height : Ti.UI.SIZE,
// width : Ti.UI.FILL,
// touchEnabled : false
// }));
//
// container.add(action);
//
// return container;
// }
//
// };

exports.leftTitle = function(name, id) {

    var view = Ti.UI.createView({
        height : '80',
        width : Ti.UI.FILL,
        left : '10dp',
        right : '10dp',
        touchEnabled : false
    });

    var content = Ti.UI.createView({
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        touchFeedback : Config.touchFeedback,
        touchFeedbackColor : Config.accent,
        touchEnabled : true
    });

    var nameView = Ti.UI.createLabel({
        text : name,
        font : Config.subtitleFont,
        color : Config.containerTextColor,
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        top : '10dp',
        left : '30dp',
        touchEnabled : false
    });

    var idView = Ti.UI.createLabel({
        text : 'ID: ' + id,
        font : Config.subtitleFont,
        color : Config.containerTextColor,
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        bottom : '16dp',
        touchEnabled : false
    });

    var underscore = Ti.UI.createView({
        backgroundColor : Config.orange,
        height : Config.underscoreHeight,
        width : Config.underscoreWidth,
        bottom : '6dp',
        left : '30dp',
        touchEnabled : false
    });

    content.add(nameView);
    content.add(idView);
    content.add(underscore);
    view.add(content);

    view.label = nameView;

    view.setText = function(text) {
        view.label.text = text;
    };

    return view;

};
/* */
exports.newwindow = function(fullscreen, exitOnClose, background, isHome) {

    var self;

    if (isHome) {

        self = Ti.UI.createView({
            backgroundColor : Config.backgroundColor,
            height : Ti.UI.FILL,
            width : Ti.UI.FILL
        });

    } else {

        self = Ti.UI.createWindow({
            fullscreen : fullscreen,
            exitOnClose : exitOnClose,
            navBarHidden : false,
            windowSoftInputMode : Config.softInput,
            backgroundColor : Config.backgroundColor,
            barColor : Config.actionbarBackgroundColor,
            navTintColor : Config.titleButtonColor,
            orientationModes : Config.orientation,
            titleAttributes : {
                color : Config.titleTextColor
            }
        });

    }

    var bg_container = Ti.UI.createScrollView({
        backgroundColor : Config.backgroundColor,
        showVerticalScrollIndicator : false,
        width : Ti.UI.FILL,
        top : '0dp',
        bottom : '0dp',
        scrollType : 'vertical',
        layout : 'vertical',
        touchEnabled : false
    });

    var bg_image = Ti.UI.createImageView({
        image : background,
        height : Ti.UI.SIZE,
        width : Ti.UI.FILL,
        top : '0dp',
        touchEnabled : false
    });

    bg_container.add(bg_image);

    self.add(bg_container);

    return self;
};

exports.tabtitle = function(text1, text2, callback) {

    var view = Ti.UI.createView({
        height : '80dp',
        width : Ti.UI.FILL,
        touchEnabled : false
    });

    var flat = Ti.UI.createView({
        height : view.height,
        width : Ti.UI.FILL,
        bottom : '0dp',
        touchEnabled : false
    });

    var round = Ti.UI.createView({
        height : '80dp',
        width : Ti.UI.FILL,
        left : '48dp',
        right : '48dp',
        touchEnabled : false
    });

    var content = Ti.UI.createView({
        layout : 'vertical',
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        touchFeedback : Config.touchFeedback,
        touchFeedbackColor : Config.accent,
        touchEnabled : true
    });

    var titleView = Ti.UI.createLabel({
        text : text1,
        font : Config.rowFont,
        color : Config.accent,
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        top : '16dp',
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        touchEnabled : false
    });

    var subTitleView = Ti.UI.createLabel({
        text : text2,
        font : Config.subtitleFont,
        color : Config.accent,
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        top : '-5dp',
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        touchEnabled : false
    });

    var underscore = Ti.UI.createView({
        backgroundColor : Config.orange,
        height : Config.underscoreHeight,
        width : Config.underscoreWidth,
        bottom : '12dp',
        touchEnabled : false
    });

    content.add(titleView);
    content.add(subTitleView);
    content.add(underscore);
    round.add(content);
    view.add(flat);
    view.add(round);

    view.label1 = titleView;
    view.label2 = subTitleView;

    view.setText = function(text1, text2) {

        Ti.API.info('update tab');
        view.label1.text = text1;
        view.label2.text = text2;
    };

    if (callback) {
        content.addEventListener('click', callback);
    }

    return view;

};
exports.transparenttitle = function(text1, text2, image, color, contWidth) {

    Ti.API.info('IMAGE: ' + image);
    var view = Ti.UI.createView({
        height : Ti.UI.SIZE,
        left : '5dp',
        right : '5dp',
        width : contWidth,
        touchEnabled : false
    });

    var content = Ti.UI.createView({
        height : Ti.UI.SIZE,
        width : Ti.UI.FILL,
        layout : 'vertical',
        touchEnabled : false
    });

    var contentImage = Ti.UI.createView({
        height : '35dp',
        width : Ti.UI.FILL,
        touchEnabled : false
    });

    contentImage.add(Ti.UI.createImageView({
        image : image,
        height : '32dp',
        width : '32dp',
        touchEnabled : false
    }));

    content.add(contentImage);

    content.add(Ti.UI.createLabel({
        top : '5dp',
        text : text1,
        font : Config.biggerinputFont,
        color : color,
        height : Ti.UI.SIZE,
        left : '5dp',
        right : '5dp',
        touchEnabled : false
    }));

    if (text2 != null) {
        content.add(Ti.UI.createLabel({
            text : text2,
            font : Config.biggerinputFont,
            color : color,
            height : Ti.UI.SIZE,
            left : '5dp',
            right : '5dp',
            touchEnabled : false
        }));
    }

    content.add(Ti.UI.createView({
        height : '2dp',
        left : '5dp',
        width : '40dp',
        backgroundColor : Config.button_lines,
        touchEnabled : false
    }));

    view.add(content);

    return view;

};

exports.transparentCentertitle = function(text1, text2, color) {

    var view = Ti.UI.createView({
        height : Ti.UI.SIZE,
        left : '5dp',
        right : '5dp',
        width : Ti.UI.FILL,
        touchEnabled : false
    });

    var content = Ti.UI.createView({
        height : Ti.UI.SIZE,
        width : Ti.UI.FILL,
        layout : 'vertical',
        touchEnabled : false
    });

    var title = Ti.UI.createLabel({
        top : '5dp',
        text : text1,
        textAlign : 'center',
        font : {
            fontSize : '15dp',
            fontWeight : 'bold'
        },
        color : color,
        height : Ti.UI.SIZE,
        left : '5dp',
        right : '5dp',
        touchEnabled : false
    });

    content.add(title);

    if (text2 != null) {
        subTitle = Ti.UI.createLabel({
            text : text2,
            textAlign : 'center',
            font : Config.biggerinputFont,
            color : color,
            top : '-5dp',
            height : Ti.UI.SIZE,
            left : '5dp',
            right : '5dp',
            touchEnabled : false
        });
        content.add(subTitle);
    }

    content.add(Ti.UI.createView({
        height : '2dp',
        width : '40dp',
        backgroundColor : Config.button_lines,
        touchEnabled : false
    }));

    view.add(content);

    view.label = view.setText = function(text) {
        view.label.text = text;
    };

    return view;
};

exports.transparentTitleLine = function(text1, color, contWidth, top) {

    var view = Ti.UI.createView({
        height : Ti.UI.SIZE,
        left : '5dp',
        right : '5dp',
        width : contWidth,
        touchEnabled : false
    });

    var content = Ti.UI.createView({
        height : Ti.UI.SIZE,
        width : Ti.UI.FILL,
        layout : 'vertical',
        touchEnabled : false
    });

    var text = Ti.UI.createLabel({
        top : '5dp',
        text : text1,
        font : Config.biggerinputFont,
        color : color,
        height : Ti.UI.SIZE,
        textAlign : 'center',
        left : '5dp',
        right : '5dp',
        touchEnabled : false
    });

    if (top != null) {
        text.top = top;
    }

    content.add(text);

    content.add(Ti.UI.createView({
        height : '2dp',
        //left : '5dp',
        width : '40dp',
        backgroundColor : Config.button_lines,
        touchEnabled : false
    }));

    view.add(content);

    return view;

};

/************************************************************************/

exports.actionbar = function(text) {

    var view = Ti.UI.createView({
        top : '0dp',
        height : Ti.UI.SIZE,
        width : Ti.UI.FILL,
        layout : 'vertical'
    });

    var actionBar = Ti.UI.createView({
        top : '0dp',
        height : Config.barHeight,
        width : Ti.UI.FILL,
        backgroundColor : Config.actionbarBackgroundColor
    });

    var centerLabel = Ti.UI.createLabel({
        text : text,
        font : {
            fontSize : '20dp'
        },
        color : Config.titleTextColor
    });

    var leftView = Ti.UI.createView({
        left : '2dp',
        height : Config.barButtonHeight,
        width : Ti.UI.SIZE,
        layout : 'horizontal'
    });

    var rightView = Ti.UI.createView({
        right : '2dp',
        height : Config.barButtonHeight,
        width : Ti.UI.SIZE,
        layout : 'horizontal'
    });

    actionBar.add(centerLabel);
    actionBar.add(leftView);
    actionBar.add(rightView);

    var barShadow = Ti.UI.createView({
        backgroundColor : Config.shadowColor,
        top : '0dp',
        height : Config.shadowHeight,
        width : Ti.UI.FILL
    });

    view.add(actionBar);
    view.add(barShadow);

    view.actionBar = actionBar;
    view.leftView = leftView;
    view.rightView = rightView;
    view.barShadow = barShadow;

    view.add_left = function(_button) {
        view.leftView.add(_button);
    };

    view.add_right = function(_button) {
        view.rightView.add(_button);
    };

    return view;

};

exports.transparentactionbar = function(text) {

    var view = Ti.UI.createView({
        height : Ti.UI.SIZE,
        width : Ti.UI.FILL,
        layout : 'vertical'
    });

    var actionBar = Ti.UI.createView({
        top : '0dp',
        height : Config.barHeight,
        width : Ti.UI.FILL
    });

    var centerLabel = Ti.UI.createLabel({
        text : text,
        font : {
            fontSize : '20dp'
        },
        color : Config.titleTextColor
    });

    var leftView = Ti.UI.createView({
        left : '2dp',
        height : Config.barButtonHeight,
        width : Ti.UI.SIZE,
        layout : 'horizontal'
    });

    var rightView = Ti.UI.createView({
        right : '2dp',
        height : Config.barButtonHeight,
        width : Ti.UI.SIZE,
        layout : 'horizontal'
    });

    actionBar.add(centerLabel);
    actionBar.add(leftView);
    actionBar.add(rightView);

    var barShadow = Ti.UI.createView({
        top : '0dp',
        height : Config.shadowHeight,
        width : Ti.UI.FILL
    });

    view.add(actionBar);
    view.add(barShadow);

    view.actionBar = actionBar;
    view.leftView = leftView;
    view.rightView = rightView;
    view.barShadow = barShadow;

    view.add_left = function(_button) {
        view.leftView.add(_button);
    };

    view.add_right = function(_button) {
        view.rightView.add(_button);
    };

    return view;

};

exports.barButton = function(callback, url) {

    var button = Ti.UI.createView({
        overrideCurrentAnimation : true,
        borderRadius : Config.barButtonBorderRadius,
        height : Config.barButtonHeight,
        width : Config.barButtonHeight,
        left : '4dp',
        right : '4dp',
        touchEnabled : false
    });

    var action = Ti.UI.createView({
        backgroundColor : Config.actionbarBackgroundColor,
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        touchFeedback : Config.barTouchFeedback,
        touchFeedbackColor : Config.barRippleColor,
        touchEnabled : true
    });

    var icon = Ti.UI.createImageView({
        image : url,
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        touchEnabled : false
    });

    action.addEventListener('click', callback);
    button.action = action;
    button.icon = icon;
    button.add(action);
    button.add(icon);

    button.icon_hide = Ti.UI.createAnimation({
        duration : 160,
        opacity : 0.0,
        curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
    });

    button.icon_show = Ti.UI.createAnimation({
        duration : 160,
        opacity : 1.0,
        curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
    });

    button.animate_show = function() {
        button.icon.show();
        button.icon.animate(button.icon_show, function(e) {
            button.action.touchEnabled = true;
        });
    };

    button.animate_hide = function() {
        button.action.touchEnabled = false;
        button.icon.animate(button.icon_hide, function(e) {
            button.icon.hide();
        });
    };

    return button;

};

exports.transparentbarButton = function(callback, url) {

    var button = Ti.UI.createView({
        borderRadius : Config.barButtonBorderRadius,
        height : Config.barButtonHeight,
        width : Config.barButtonHeight,
        left : '4dp',
        right : '4dp',
        touchEnabled : false
    });

    var action = Ti.UI.createView({
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        touchFeedback : Config.barTouchFeedback,
        touchFeedbackColor : Config.barRippleColor,
        touchEnabled : true
    });

    var icon = Ti.UI.createImageView({
        image : url,
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        touchEnabled : false
    });

    action.addEventListener('click', callback);
    button.action = action;
    button.icon = icon;
    button.add(action);
    button.add(icon);

    button.icon_hide = Ti.UI.createAnimation({
        duration : 160,
        opacity : 0.0,
        curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
    });

    button.icon_show = Ti.UI.createAnimation({
        duration : 160,
        opacity : 1.0,
        curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
    });

    button.animate_show = function() {
        button.icon.show();
        button.icon.animate(button.icon_show, function(e) {
            button.action.touchEnabled = true;
        });
    };

    button.animate_hide = function() {
        button.action.touchEnabled = false;
        button.icon.animate(button.icon_hide, function(e) {
            button.icon.hide();
        });
    };

    return button;

};

exports.menuoption = function(id, url, text) {

    var button = Ti.UI.createView({
        height : Config.optionsRowHeight,
        width : Ti.UI.FILL,
        touchEnabled : false
    });

    var action = Ti.UI.createView({
        identifier : id,
        backgroundColor : Config.optionsBackground,
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        touchFeedback : Config.touchFeedback,
        touchFeedbackColor : Config.optionsSelectedBackground,
        touchEnabled : true
    });

    var layout = Ti.UI.createView({
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        layout : 'vertical',
        touchEnabled : false
    });

    var icon = Ti.UI.createImageView({
        image : url,
        height : '36dp',
        width : '36dp',
        touchEnabled : false
    });

    var text = Ti.UI.createLabel({
        text : text,
        textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
        font : Config.optionsTitleFont,
        color : Config.optionsTitleColor,
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        top : '4dp',
        bottom : '8dp',
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        touchEnabled : false
    });

    var underscore = Ti.UI.createView({
        backgroundColor : Config.accent,
        height : Config.underscoreHeight,
        width : Config.underscoreWidth,
        touchEnabled : false
    });

    layout.add(icon);
    layout.add(text);
    layout.add(underscore);

    button.add(action);
    button.add(layout);

    return button;

};

exports.overview = function() {

    var view = Ti.UI.createView({
        backgroundColor : Config.overMask,
        top : (parseInt(Config.barHeight) + parseInt(Config.shadowHeight)) + 'dp',
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        zindex : 666
    });

    view.opacity = 0.0;
    view.hide();

    view.mask_hide = Ti.UI.createAnimation({
        duration : 160,
        opacity : 0.0,
        curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
    });

    view.mask_show = Ti.UI.createAnimation({
        duration : 160,
        opacity : 1.0,
        curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
    });

    view.animate_show = function() {
        view.show();
        view.animate(view.mask_show);
    };

    view.animate_hide = function() {
        view.animate(view.mask_hide, function(e) {
            view.hide();
        });
    };

    return view;

};

exports.fullwork = function() {

    var work = Ti.UI.createView({
        overrideCurrentAnimation : true,
        backgroundColor : Config.overMask,
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        zindex : 999
    });

    var indicator = Ti.UI.createActivityIndicator({
        style : Config.isAndroid ? Ti.UI.ActivityIndicatorStyle.BIG_DARK : Ti.UI.ActivityIndicatorStyle.DARK,
        height : '120dp',
        width : '120dp'
    });
    indicator.show();

    work.add(indicator);
    work.opacity = 0.0;
    work.hide();

    work.mask_hide = Ti.UI.createAnimation({
        duration : 160,
        opacity : 0.0,
        curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
    });

    work.mask_show = Ti.UI.createAnimation({
        duration : 160,
        opacity : 1.0,
        curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
    });

    work.animate_show = function() {
        work.show();
        work.animate(work.mask_show);
    };

    work.animate_hide = function() {
        work.animate(work.mask_hide, function(e) {
            work.hide();
        });
    };

    return work;

};

exports.contextwork = function() {

    var work = Ti.UI.createView({
        overrideCurrentAnimation : true,
        height : '120dp',
        width : '120dp'
    });

    var indicator = Ti.UI.createActivityIndicator({
        style : Config.isAndroid ? Ti.UI.ActivityIndicatorStyle.BIG_DARK : Ti.UI.ActivityIndicatorStyle.DARK,
        height : '120dp',
        width : '120dp'
    });
    indicator.show();

    work.add(indicator);
    work.opacity = 0.0;
    work.hide();

    work.mask_slim = Ti.UI.createAnimation({
        duration : 160,
        height : '0dp',
        curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
    });

    work.mask_fat = Ti.UI.createAnimation({
        duration : 160,
        height : '120dp',
        curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
    });

    work.mask_hide = Ti.UI.createAnimation({
        duration : 160,
        opacity : 0.0,
        curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
    });

    work.mask_show = Ti.UI.createAnimation({
        duration : 160,
        opacity : 1.0,
        curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
    });

    work.animate_show = function() {
        work.animate(work.mask_fat, function(e) {
            work.show();
            work.animate(work.mask_show);
        });
    };

    work.animate_hide = function() {
        work.animate(work.mask_hide, function(e) {
            work.hide();
            work.animate(work.mask_slim);
        });
    };

    return work;

};

exports.addIcon = function(url, text, callback) {

    var view = Ti.UI.createView({
        height : '100dp',
        width : Ti.UI.FILL,
        touchEnabled : false
    });

    var flat = Ti.UI.createView({
        backgroundColor : Config.backgroundColor,
        height : Config.subtitleBorderRadius,
        width : Ti.UI.FILL,
        bottom : '0dp',
        touchEnabled : false
    });

    var round = Ti.UI.createView({
        borderRadius : Config.subtitleBorderRadius,
        elevation : Config.elevation,
        height : '80dp',
        width : Ti.UI.FILL,
        left : '48dp',
        right : '48dp',
        touchEnabled : false
    });

    var content = Ti.UI.createView({
        backgroundColor : Config.backgroundColor,
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        touchFeedback : Config.touchFeedback,
        touchFeedbackColor : Config.accent,
        touchEnabled : true
    });

    var iconView = Ti.UI.createImageView({
        image : url,
        height : '36dp',
        width : '36dp',
        //top : '12dp',
        bottom : '4dp',
        touchEnabled : false
    });

    var titleView = Ti.UI.createLabel({
        text : text,
        top : '8dp',
        color : Config.accent,
        font : Config.subtitleFont,
        // color : Config.containerTextColor,
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        touchEnabled : false
    });

    var underscore = Ti.UI.createView({
        backgroundColor : Config.accent,
        height : Config.underscoreHeight,
        width : Config.underscoreWidth,
        top : '30dp',
        //bottom : '4dp',
        touchEnabled : false
    });

    content.add(titleView);
    content.add(underscore);
    content.add(iconView);
    round.add(content);
    view.add(flat);
    view.add(round);

    if (callback) {
        content.addEventListener('click', callback);
    }

    return view;

};

exports.icontitle = function(url, text, callback) {

    var view = Ti.UI.createView({
        height : '90dp',
        width : Ti.UI.FILL,
        touchEnabled : false
    });

    var flat = Ti.UI.createView({
        backgroundColor : Config.backgroundColor,
        height : Config.subtitleBorderRadius,
        width : Ti.UI.FILL,
        bottom : '0dp',
        touchEnabled : false
    });

    var round = Ti.UI.createView({
        borderRadius : Config.subtitleBorderRadius,
        height : '90dp',
        width : Ti.UI.FILL,
        left : '48dp',
        right : '48dp',
        touchEnabled : false
    });

    var content = Ti.UI.createView({
        backgroundColor : Config.backgroundColor,
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        touchFeedback : Config.touchFeedback,
        touchFeedbackColor : Config.accent,
        touchEnabled : true
    });

    var iconView = Ti.UI.createImageView({
        image : url,
        height : '36dp',
        width : '36dp',
        top : '12dp',
        touchEnabled : false
    });

    var titleView = Ti.UI.createLabel({
        text : text,
        font : Config.subtitleFont,
        color : Config.accent,
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        bottom : '8dp',
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        touchEnabled : false
    });

    var underscore = Ti.UI.createView({
        backgroundColor : Config.accent,
        height : Config.underscoreHeight,
        width : Config.underscoreWidth,
        bottom : '4dp',
        touchEnabled : false
    });

    content.add(iconView);
    content.add(titleView);
    content.add(underscore);
    round.add(content);
    view.add(flat);
    view.add(round);

    view.icon = iconView;

    view.setIcon = function(url) {
        view.icon.image = url;
    };

    if (callback) {
        content.addEventListener('click', callback);
    }

    return view;

};

exports.fsTitle = function(text1, text2, callback) {

    var view = Ti.UI.createView({
        //height : '48dp',
        //height : '85dp',
        height : '80dp',
        width : Ti.UI.FILL,
        backgroundColor : 'cyan',
        touchEnabled : false
    });

    var flat = Ti.UI.createView({
        //backgroundColor : Config.backgroundColor,
        //height : Config.subtitleBorderRadius,
        height : view.height,
        width : Ti.UI.FILL,
        bottom : '0dp',
        touchEnabled : false
    });

    var round = Ti.UI.createView({
        //borderRadius : Config.subtitleBorderRadius,
        //height : '48dp',
        //height : '85dp',
        height : '80dp',
        width : Ti.UI.FILL,
        left : '48dp',
        right : '48dp',
        backgroundColor : 'yellow',
        touchEnabled : false
    });

    var content = Ti.UI.createView({
        //2backgroundColor : Config.backgroundColor,
        layout : 'vertical',
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        backgroundColor : 'red',
        touchFeedback : Config.touchFeedback,
        touchFeedbackColor : Config.accent,
        touchEnabled : true
    });

    var titleView = Ti.UI.createLabel({
        text : text1,
        //font : Config.subtitleFont,
        font : Config.rowFont,
        color : Config.accent,
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        //bottom : '16dp',
        top : '16dp',
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        touchEnabled : false
    });

    var subTitleView = Ti.UI.createLabel({
        text : text2,
        font : Config.subtitleFont,
        color : Config.accent,
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        //bottom : '16dp',
        top : '-5dp',
        //bottom : '16dp',
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        touchEnabled : false
    });

    var underscore = Ti.UI.createView({
        backgroundColor : Config.orange,
        height : Config.underscoreHeight,
        width : Config.underscoreWidth,
        bottom : '12dp',
        touchEnabled : false
    });

    content.add(titleView);
    content.add(subTitleView);
    content.add(underscore);
    round.add(content);
    view.add(flat);
    view.add(round);

    view.label1 = titleView;
    view.label2 = subTitleView;

    view.setText = function(text1, text2) {

        Ti.API.info('update tab');
        view.label1.text = text1;
        view.label2.text = text2;
    };

    if (callback) {
        content.addEventListener('click', callback);
    }

    return view;

};

exports.doubletitle = function(text1, text2, callback) {

    var view = Ti.UI.createView({
        height : '70dp',
        width : Ti.UI.FILL,
        touchEnabled : false
    });

    var flat = Ti.UI.createView({
        backgroundColor : Config.backgroundColor,
        height : Config.subtitleBorderRadius,
        width : Ti.UI.FILL,
        bottom : '0dp',
        touchEnabled : false
    });

    var round = Ti.UI.createView({
        borderRadius : Config.subtitleBorderRadius,
        height : '70dp',
        width : Ti.UI.FILL,
        left : '48dp',
        right : '48dp',
        touchEnabled : false
    });

    var content = Ti.UI.createView({
        backgroundColor : Config.backgroundColor,
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        touchFeedback : Config.touchFeedback,
        touchFeedbackColor : Config.accent,
        touchEnabled : true
    });

    var title1View = Ti.UI.createLabel({
        text : text1,
        font : Config.subtitleFont,
        color : Config.subtitleTextColor,
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        top : '8dp',
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        touchEnabled : false
    });

    var title2View = Ti.UI.createLabel({
        text : text2,
        font : Config.subtitleFont,
        color : Config.accent,
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        bottom : '16dp',
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        touchEnabled : false
    });

    var underscore = Ti.UI.createView({
        backgroundColor : Config.accent,
        height : Config.underscoreHeight,
        width : Config.underscoreWidth,
        bottom : '12dp',
        touchEnabled : false
    });

    content.add(title1View);
    content.add(title2View);
    content.add(underscore);
    round.add(content);
    view.add(flat);
    view.add(round);

    if (callback) {
        content.addEventListener('click', callback);
    }

    return view;

};

exports.roundtitle = function(text, callback) {

    var view = Ti.UI.createView({
        borderRadius : Config.borderRadius,
        elevation : Config.elevation,
        height : '48dp',
        width : Ti.UI.FILL,
        left : '10dp',
        right : '10dp',
        touchEnabled : false
    });

    var content = Ti.UI.createView({
        backgroundColor : Config.containerBackgroundColor,
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        touchFeedback : Config.touchFeedback,
        touchFeedbackColor : Config.accent,
        touchEnabled : true
    });

    var titleView = Ti.UI.createLabel({
        text : text,
        font : Config.subtitleFont,
        color : Config.containerTextColor,
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        touchEnabled : false
    });

    var underscore = Ti.UI.createView({
        backgroundColor : Config.accent,
        height : Config.underscoreHeight,
        width : Config.underscoreWidth,
        bottom : '8dp',
        touchEnabled : false
    });

    content.add(titleView);
    content.add(underscore);
    view.add(content);

    view.label = titleView;

    view.setText = function(text) {
        view.label.text = text;
    };

    if (callback) {
        content.addEventListener('click', callback);
    }

    return view;

};

exports.explanationBox = function(text1, text2, url, callback) {

    var view = Ti.UI.createView({
        borderRadius : Config.borderRadius,
        elevation : Config.elevation,
        height : Ti.UI.SIZE,
        width : Ti.UI.FILL,
        left : '10dp',
        right : '10dp',
        touchEnabled : false
    });

    var content = Ti.UI.createView({
        backgroundColor : Config.containerBackgroundColor,
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        touchFeedback : Config.touchFeedback,
        touchFeedbackColor : Config.accent,
        touchEnabled : true
    });

    var iconView = Ti.UI.createImageView({
        image : url,
        height : '36dp',
        width : '36dp',
        right : '10dp',
        touchEnabled : false
    });

    var verticalLeft = Ti.UI.createView({
        height : Ti.UI.SIZE,
        width : Ti.UI.FILL,
        layout : 'vertical',
        left : '10dp',
        right : '56dp',
        touchEnabled : false
    });

    var title1View = Ti.UI.createLabel({
        text : text1,
        font : Config.inputFont,
        color : Config.containerTextColor,
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        top : '8dp',
        left : '0dp',
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        touchEnabled : false
    });

    var title2View = Ti.UI.createLabel({
        text : text2,
        font : Config.inputFont,
        color : Config.accent,
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        left : '0dp',
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        touchEnabled : false
    });

    var underscore = Ti.UI.createView({
        backgroundColor : Config.accent,
        height : Config.underscoreHeight,
        width : Config.underscoreWidth,
        top : '4dp',
        left : '0dp',
        bottom : '8dp',
        touchEnabled : false
    });

    verticalLeft.add(title1View);
    verticalLeft.add(title2View);
    verticalLeft.add(underscore);
    content.add(verticalLeft);
    content.add(iconView);
    view.add(content);

    if (callback) {
        content.addEventListener('click', callback);
    }

    return view;

};

exports.explanationSteps = function(text1, text2, text3, callback) {

    var view = Ti.UI.createView({
        borderRadius : Config.borderRadius,
        elevation : Config.elevation,
        height : Ti.UI.SIZE,
        width : Ti.UI.FILL,
        left : '10dp',
        right : '10dp',
        touchEnabled : false
    });

    var content = Ti.UI.createView({
        backgroundColor : Config.containerBackgroundColor,
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        touchFeedback : Config.touchFeedback,
        touchFeedbackColor : Config.accent,
        touchEnabled : true
    });

    var verticalLeft = Ti.UI.createView({
        height : Ti.UI.SIZE,
        width : Ti.UI.FILL,
        layout : 'vertical',
        left : '10dp',
        right : '56dp',
        touchEnabled : false
    });

    var title1View = Ti.UI.createLabel({
        text : text1,
        font : Config.containerTextColor,
        color : Config.inputTextColor,
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        top : '8dp',
        left : '0dp',
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        touchEnabled : false
    });

    var title2View = Ti.UI.createLabel({
        text : text2,
        font : Config.inputFont,
        color : Config.accent,
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        left : '0dp',
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        touchEnabled : false
    });

    var title3View = Ti.UI.createLabel({
        text : text3,
        font : Config.subtitleFont,
        color : Config.accent,
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        right : '10dp',
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        touchEnabled : false
    });

    var underscore = Ti.UI.createView({
        backgroundColor : Config.accent,
        height : Config.underscoreHeight,
        width : Config.underscoreWidth,
        top : '4dp',
        left : '0dp',
        bottom : '8dp',
        touchEnabled : false
    });

    verticalLeft.add(title1View);
    verticalLeft.add(title2View);
    verticalLeft.add(underscore);
    content.add(verticalLeft);
    content.add(title3View);
    view.add(content);

    if (callback) {
        content.addEventListener('click', callback);
    }

    return view;

};

exports.contactItem = function(text1, text2, url, key, value) {

    var view = Ti.UI.createView({
        borderRadius : Config.borderRadius,
        elevation : Config.elevation,
        height : Ti.UI.SIZE,
        width : Ti.UI.FILL,
        left : '10dp',
        right : '10dp',
        touchEnabled : false
    });

    var content = Ti.UI.createView({
        itemKey : key,
        itemValue : value,
        backgroundColor : Config.containerBackgroundColor,
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        touchFeedback : Config.touchFeedback,
        touchFeedbackColor : Config.accent,
        touchEnabled : true
    });

    var iconView = Ti.UI.createImageView({
        image : url,
        height : '36dp',
        width : '36dp',
        right : '10dp',
        touchEnabled : false
    });

    var verticalLeft = Ti.UI.createView({
        height : Ti.UI.SIZE,
        width : Ti.UI.FILL,
        layout : 'vertical',
        left : '10dp',
        right : '56dp',
        touchEnabled : false
    });

    var title1View = Ti.UI.createLabel({
        text : text1,
        font : Config.inputFont,
        color : Config.containerTextColor,
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        top : '8dp',
        left : '0dp',
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        touchEnabled : false
    });

    var title2View = Ti.UI.createLabel({
        text : text2,
        font : Config.inputFont,
        color : Config.accent,
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
        left : '0dp',
        ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
        wordWrap : false,
        touchEnabled : false
    });

    var underscore = Ti.UI.createView({
        backgroundColor : Config.accent,
        height : Config.underscoreHeight,
        width : Config.underscoreWidth,
        top : '4dp',
        left : '0dp',
        bottom : '8dp',
        touchEnabled : false
    });

    verticalLeft.add(title1View);
    verticalLeft.add(title2View);
    verticalLeft.add(underscore);
    content.add(verticalLeft);
    content.add(iconView);
    view.add(content);

    var itemFunction;

    if (value && key) {

        function goPhone(itemValue) {
            // Ti.API.error('Click Item: '+ itemValue);
            // Ti.API.error('Click Item T: '+ typeof(itemValue));
            //Ti.Platform.openURL("tel:" + Ti.App.Properties.getString(itemValue));

            var myPhone = "tel:" + itemValue;

            //		Ti.API.error('My Phone: '+ myPhone);
            Ti.Platform.openURL(myPhone);
        }

        function goMail(itemValue) {
            var emailDialog = Ti.UI.createEmailDialog();
            emailDialog.toRecipients = [itemValue];
            emailDialog.open();
        }

        function goWeb(itemValue) {
            var webview = Titanium.UI.createWebView({
                url : itemValue
            });
            var window = Titanium.UI.createWindow();
            window.add(webview);
            window.open({
                modal : true
            });
        }


        content.addEventListener('click', function(e) {
            switch(e.source.itemKey) {
            case 'phone':
                goPhone(e.source.itemValue);
                break;
            case 'mail':
                goMail(e.source.itemValue);
                break;
            case 'web':
                goWeb(e.source.itemValue);
                break;
            }

        });
    }

    return view;

};
