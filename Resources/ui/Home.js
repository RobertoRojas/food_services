var Config = require('/libs/Config');
var summon = require('/mods/summon');
var init = require('/mods/initializers');
var sync = require('/mods/sync');
var xhr = require('/mods/xhr');
var db = require('/mods/db');
var moment = require('/libs/moment');
moment.locale(Ti.Locale.currentLanguage);

var menuButton;
var backButton;
var orderMapButton;
//var distribution;

function Home(drawer) {

	Ti.API.info('Config Mode: ' + Config.mode);
	if (Config.mode) {
		Ti.API.error('-------------------- HOME --------------------');
	}
	var me = Ti.App.Properties.getObject('me', new Object);

	var content = Ti.UI.createView({
		height : Ti.UI.FILL,
		width : Ti.UI.FILL,
		layout : 'vertical'
	});

	var self = summon.newwindow(false, true, Config.colorBg, true);
	var actionbar = summon.actionbar(Config.Home);
	var work = summon.contextwork();

	content.add(actionbar);

	self.add(content);

	menuButton = summon.barButton(openMenu, Config.images+ 'ic_menu_w.png');
	actionbar.add_left(menuButton);

	backButton = summon.barButton(backToRoutes, Config.images+ 'ic_navigate_before_w.png');
	backButton.action.touchEnabled = false;
	backButton.icon.opacity = 0.0;
	backButton.icon.hide();
	actionbar.add_left(backButton);

	orderMapButton = summon.barButton(openMap, Config.images+ 'white_pin.png');
	orderMapButton.action.touchEnabled = false;
	orderMapButton.icon.opacity = 0.0;
	orderMapButton.icon.hide();
	actionbar.add_right(orderMapButton);

	drawer.setCenterWindow(self);

	var topView;
	var mainView;
	var subtitle;
	var scrollRoutes;
	var scrollTasks;
	var scrollable;
	var retryBox;

	var status = 0;
	var displaySeparator = 0;
	var displayHeight = 100;
	var displayHeightTask = 95;
	var displayedRoutes = {};
	var displayedTasks = {};
	var selectedRoute;

	function construct() {

		topView = Ti.UI.createView({
			height : '160dp',
			width : Ti.UI.FILL,
			top : '0dp',
			touchEnabled : false
		});

		var picView = Ti.UI.createView({
			borderRadius : '40dp',
			height : '80dp',
			width : '80dp',
			top : '24dp'
		});

		var picTaken = Ti.UI.createImageView({
			image : Config.getPic(),
			borderRadius : '40dp',
			height : '80dp',
			width : '80dp'
		});

		var picFrame = Ti.UI.createView({
			borderRadius : '40dp',
			borderWidth : '2dp',
			borderColor : Config.inputBorderColor,
			height : '80dp',
			width : '80dp'
		});

		picView.add(picTaken);
		picView.add(picFrame);

		var fullnameView = Ti.UI.createView({
			height : Ti.UI.SIZE,
			width : Ti.UI.SIZE,
			top : '112dp',
			layout : 'horizontal',
			touchEnabled : false
		});

		var nameLabel = Ti.UI.createLabel({
			text : me.name + ' ',
			font : Config.subtitleFont,
			color : Config.orange,
			height : Ti.UI.SIZE,
			width : Ti.UI.SIZE,
			touchEnabled : false
		});

		var lastnameLabel = Ti.UI.createLabel({
			text : me.last_name,
			font : Config.subtitleFont,
			color : Config.accent,
			height : Ti.UI.SIZE,
			width : Ti.UI.SIZE,
			touchEnabled : false
		});

		fullnameView.add(nameLabel);
		fullnameView.add(lastnameLabel);

		var posLabel = Ti.UI.createLabel({
			text : 'Conductor',
			font : Config.subtitleFont,
			color : Config.subtitleTextColor,
			height : Ti.UI.SIZE,
			width : Ti.UI.SIZE,
			top : '136dp',
			touchEnabled : false
		});

		topView.add(picView);
		topView.add(fullnameView);
		topView.add(posLabel);

		// MAINVIEW

		mainView = Ti.UI.createView({
			height : Ti.UI.FILL,
			width : Ti.UI.FILL,
			top : '0dp',
			layout : 'vertical',
			touchEnabled : false
		});

		var iconTitle= Config.images + 'white_truck.png';
		subtitle = summon.roundFsTitle('', 'white', Config.circle, iconTitle);
		
		// subtitle.top = '24dp';
		// subtitle.bottom = '0dp';

		scrollRoutes = Ti.UI.createScrollView({
			showVerticalScrollIndicator : true,
			height : Ti.UI.FILL,
			width : Ti.UI.FILL,
			top : '0dp',
			bottom : '0dp',
			scrollType : 'vertical'
		});

		scrollTasks = Ti.UI.createScrollView({
			//backgroundColor : Config.backgroundColor,
			showVerticalScrollIndicator : true,
			height : Ti.UI.FILL,
			width : Ti.UI.FILL,
			top : '0dp',
			bottom : '0dp',
			scrollType : 'vertical'
		});

		// RETRY

		retryBox = Ti.UI.createView({
			borderRadius : Config.buttonBorderRadius,
			height : Config.buttonHeight,
			width : Ti.UI.FILL,
			left : '80dp',
			right : '80dp',
			touchEnabled : false
		});

		var retryAction = Ti.UI.createView({
			backgroundColor : Config.buttonBackgroundColor,
			height : Ti.UI.FILL,
			width : Ti.UI.FILL,
			touchFeedback : Config.touchFeedback,
			touchFeedbackColor : Config.buttonRippleColor,
			touchEnabled : true
		});

		var retryLabel = Ti.UI.createLabel({
			text : 'VOLVER A INTENTAR',
			textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
			font : Config.buttonFont,
			color : Config.buttonTextColor,
			height : Ti.UI.SIZE,
			width : Ti.UI.SIZE,
			touchEnabled : false
		});

		retryAction.addEventListener('click', unpause);

		retryBox.add(retryAction);
		retryBox.add(retryLabel);

		// END

		scrollRoutes.add(work);

		scrollable = Ti.UI.createScrollableView({
			disableBounce : true,
			cacheSize : 11,
			views : [scrollRoutes, scrollTasks],
			scrollingEnabled : false,
			pagingControlColor : 'transparent',
			height : Ti.UI.FILL,
			width : Ti.UI.FILL,
			top : '0dp',
			left : '0dp',
			right : '0dp',
			bottom : '0dp'
		});

		scrollable.addEventListener('scrollend', function(e) {
			if (e.source.currentPage == 0) {
				backButton.animate_hide();
				orderMapButton.animate_hide();
				subtitle.setText('SELECCIONAR', 'RUTAS');
			} else {
				backButton.animate_show();
				orderMapButton.animate_show();
				subtitle.setText('SELECCIONAR', 'CLIENTES');
			}
		});

		scrollable.backToRoutes = backToRoutes;

		Config.scrollable = scrollable;

		mainView.add(subtitle);
		mainView.add(scrollable);

		content.add(topView);
		content.add(mainView);

		ready();

		if (Object.keys(db.selectROUTESasOBJECT()).length > 0) {
			start();
		}

	}

	function ready() {
		status = 1;
		subtitle.setText('INICIANDO');
		work.animate_show();
		sync.start();
		init.refreshCallbackService(refresh);
		init.startService();
	}

	function start() {
		status = 2;
		subtitle.setText('SELECCIONAR', 'RUTAS');
		work.animate_hide();
		if (topView.height != '0dp') {
			toggleSpace();
		}

		setTimeout(refresh, 1000);
	}

	function unpause() {
		db.updateKEYVAL({
			route_id : ''
		});
		status = 0;
		scrollRoutes.remove(retryBox);
		getStatus();
	}

	function getStatus() {
		status = 0;
		subtitle.setText('CARGANDO');
		work.animate_show();
	}

	function refresh() {

		if (status == 1) {
			start();
			return;
		}

		var routes = db.selectROUTESasOBJECT();
		var timeR = routesRemove(routes);

		setTimeout(function(e) {

			var timeU = routesUpdate(routes);

			setTimeout(function(e) {

				routesCreate(routes);

			}, timeU);

		}, timeR);

		if (selectedRoute) {

			var tasks = db.selectTASKSbyROUTEasOBJECT(selectedRoute);
			var timeR2 = tasksRemove(tasks);

			setTimeout(function(e) {

				var timeU2 = tasksUpdate(tasks);

				setTimeout(function(e) {

					tasksCreate(tasks);

				}, timeU2);

			}, timeR2);
		}
	}

	function routesRemove(routes) {

		var time = 0;
		var delay = 0;

		for (var d in displayedRoutes) {

			if (!routes[d]) {

				time += 320;

				var _displayedRoute = displayedRoutes[d];

				_displayedRoute.action.touchEnabled = false;

				_displayedRoute.animate(Ti.UI.createAnimation({
					delay : 320 * delay,
					duration : 640,
					// left : (Config.fullW),
					// right : -(Config.fullW),
					left : Ti.UI.FILL,
                    right : Ti.UI.FILL,
					curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
				}), function(e) {
					this.animate(Ti.UI.createAnimation({
						duration : 120,
						height : '0dp',
						curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
					}), function(e) {
						scrollRoutes.remove(displayedRoutes[this.routeId]);
						delete displayedRoutes[this.routeId];
					});
				});

				delay++;
			}
		}

		if (time != 0) {
			time += 320;
		}

		return time;
	}

	function routesUpdate(routes) {
        
		var time = 0;
		var delay = 0;

		for (var r in routes) {

			if (displayedRoutes[r]) {

				time = 320;

				var _route = routes[r];
				
                var processDate = moment(routes[r].process_date, 'YYYY/MM/DD');
                var month = processDate.format('M');
                var nameMonth= moment().subtract(month, "month").startOf("month").format('MMMM');
                nameMonth= nameMonth.charAt(0).toUpperCase() + nameMonth.slice(1);
                
                var day = processDate.format('D');
                var currentDate = day + ' ' + nameMonth;
				
				var _displayedRoute = displayedRoutes[r];

				_displayedRoute.qc_name.text = _route.qc_name;
				_displayedRoute.qm_name.text = _route.qm_name + ' (' + _route.tasks.length + ')';
				_displayedRoute.qm_date.text = 'Fecha: ' + currentDate;

				var _top = ((displaySeparator + displayHeight) * _route.position + displaySeparator) + 'dp';

				_displayedRoute.animate(Ti.UI.createAnimation({
					duration : 320,
					top : _top,
					curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
				}));
			}
		}

		return time;
	}

	function routesCreate(routes) {

		var delay = 0;

		for (var r in routes) {

			if (!displayedRoutes[r]) {
				scrollRoutes.add(createRouteRow(routes[r], delay));
				delay++;
			}
		}
	}

	function createRouteRow(route, delay) {

		var _top = ((displaySeparator + displayHeight) * route.position + displaySeparator) + 'dp';
        
        Ti.API.info('displayHeight: '+ displayHeight);
        Ti.API.info('displaySeparator: '+ displaySeparator);
        Ti.API.info('_top: '+ _top);
        
		var row = Ti.UI.createView({
			overrideCurrentAnimation : true,
			routeId : route.id,
			processDate : route.process_date,
			height : displayHeight + 'dp',
			width : Ti.UI.FILL,
			top : _top,
            //backgroundColor: Config.accent,
            backgroundColor: 'rgb(23, 59, 85, 0.5)',
            borderColor: 'yellow',
            //opacity: 0.8,
			bottom : displaySeparator + 'dp',
			touchEnabled : false
		});

		var rowAction = Ti.UI.createView({
			routeId : route.id,
			processDate : route.process_date,
			height : Ti.UI.FILL,
			width : Ti.UI.FILL,
			touchFeedback : Config.touchFeedback,
			touchFeedbackColor : Config.orange,
			touchEnabled : true
		});

		rowAction.addEventListener('click', showRoute);

		var infoBox = Ti.UI.createView({
			height : Ti.UI.SIZE,
			left : '10dp',
			right : '50dp',
			touchEnabled : false
		});

		var verticalBox = Ti.UI.createView({
			height : Ti.UI.SIZE,
			width : Ti.UI.FILL,
			layout : 'vertical',
			touchEnabled : false
		});

		var qm_name = Ti.UI.createLabel({
			text : route.qm_name + ' (' + route.tasks.length + ')',
			textAlign : Ti.UI.TEXT_ALIGNMENT_LEFT,
			font : Config.biginputFont,
			color : Config.lightBlue,
			height : Ti.UI.SIZE,
			width : Ti.UI.FILL,
			wordWrap : false,
			touchEnabled : false
		});

		var qc_name = Ti.UI.createLabel({
			text : route.qc_name,
			textAlign : Ti.UI.TEXT_ALIGNMENT_LEFT,
			font : Config.biginputFont,
			color : Config.whiteText,
			height : Ti.UI.SIZE,
			width : Ti.UI.FILL,
			wordWrap : false,
			touchEnabled : false
		});
		
			
		var processDate = moment(route.process_date, 'YYYY/MM/DD');

		var month = processDate.format('M');
		var nameMonth= moment().subtract(month, "month").startOf("month").format('MMMM');
		nameMonth= nameMonth.charAt(0).toUpperCase() + nameMonth.slice(1);
		var day = processDate.format('D');
		//var year = processDate.format('YYYY');

		var currentDate = day + ' ' + nameMonth;

		var qm_date = Ti.UI.createLabel({
			//text : 'Fecha: ' + moment(route.process_date).format('D MMMM'),
			text : 'Fecha: ' + currentDate,
			textAlign : Ti.UI.TEXT_ALIGNMENT_LEFT,
			font : Config.biginputFont,
			//color : Config.containerTextColor,
			color : Config.whiteText,
			height : Ti.UI.SIZE,
			width : Ti.UI.FILL,
			// ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
			wordWrap : false,
			touchEnabled : false
		});

		verticalBox.add(qm_name);
		verticalBox.add(qc_name);
		verticalBox.add(qm_date);

		infoBox.add(verticalBox);

		var imageBox = Ti.UI.createView({
			//height : Ti.UI.FILL,
			height : '50dp',
			width : '50dp',
			right : '10dp',
			borderRadius: '25dp',
			backgroundColor: Config.fs_colorDark,
			touchEnabled : false
		});

		imageBox.add(Ti.UI.createImageView({
			image : Config.images + 'white_right.png',
			height : '30dp',
			width : '30dp',
			touchEnabled : false
		}));

		row.add(rowAction);
		row.add(infoBox);
		row.add(imageBox);

		row.action = rowAction;
		row.qc_name = qc_name;
		row.qm_name = qm_name;
		row.qm_date = qm_date;

		row.animate(Ti.UI.createAnimation({
			delay : 320 * delay,
			duration : 640,
			// left : '20dp',
			// right : '20dp',
			left : Ti.UI.FILL,
            right : Ti.UI.FILL,
			curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
		}));

		displayedRoutes[route.id] = row;
		return row;
	};

	function tasksRemove(tasks) {

		var time = 0;
		var delay = 0;

		for (var d in displayedTasks) {

			if (!tasks[d]) {

				time += 320;

				var _displayedTask = displayedTasks[d];

				_displayedTask.action.touchEnabled = false;

				_displayedTask.animate(Ti.UI.createAnimation({
					delay : 320 * delay,
					duration : 640,
					opacity : 0.0,
					curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
				}), function(e) {
					this.animate(Ti.UI.createAnimation({
						duration : 120,
						height : '0dp',
						curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
					}), function(e) {
						scrollTasks.remove(displayedTasks[this.taskId]);
						delete displayedTasks[this.taskId];
					});
				});
				delay++;
			}
		}

		if (time != 0) {
			time += 320;
		}

		return time;

	}

	function tasksUpdate(tasks) {
		var time = 0;
		var delay = 0;

		for (var t in tasks) {

			if (displayedTasks[t]) {

				time = 320;

				var _task = tasks[t];
				var _displayedTask = displayedTasks[t];

				//_displayedTask.stop = 'STOP ' + _task.stop;
				_displayedTask.customerName.text = _task.name.toUpperCase();
				_displayedTask.customerAddress.text = _task.address1;
				_displayedTask.customerAddress2.text = _task.address2 + ', ' + _task.city_name;
				_displayedTask.boxes.text = 'Cajas: ' + totalBoxes(_task.id);

				//_displayedTask.arrival.text = (_task.arrival_datetime_str) ? '  Hora programada: ' + _task.arrival_datetime_str + ' hrs' : '  Hora programada: Sin datos';

				var _top = ((displaySeparator + displayHeightTask) * _task.position + displaySeparator) + 'dp';

				_displayedTask.animate(Ti.UI.createAnimation({
					duration : 320,
					top : _top,
					curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
				}));
			}
		}
		return time;
	}

	function tasksCreate(tasks) {

		var delay = 0;

		for (var t in tasks) {

			if (!displayedTasks[t]) {
				scrollTasks.add(createTaskRow(tasks[t], delay));
				delay++;
			}
		}
	}

	function createTaskRow(task, delay) {
		delete task.orders;

		var _top = ((displaySeparator + displayHeightTask) * task.position + displaySeparator) + 'dp';

		var row = Ti.UI.createView({
			overrideCurrentAnimation : true,
			taskId : task.id,
			routeId : selectedRoute,
			//backgroundColor : Config.containerBackgroundColor,
			backgroundColor: 'red',
			borderRadius : Config.borderRadius,
			elevation : Config.elevation,
			height : displayHeightTask + 'dp',
			width : Ti.UI.FILL,
			top : _top,
			// left : '20dp',
			// right : '20dp',
			bottom : displaySeparator + 'dp',
			opacity : 0.0,
			touchEnabled : false
		});

		var rowAction = Ti.UI.createView({
			taskId : task.id,
			routeId : selectedRoute,
			// backgroundColor : Config.containerBackgroundColor,
			//backgroundColor : '#014667',
			//backgroundColor: 'cyan',
			height : Ti.UI.FILL,
			width : Ti.UI.FILL,
			touchFeedback : Config.touchFeedback,
			// touchFeedbackColor : Config.accent,
			touchFeedbackColor : Config.orange,
			touchEnabled : true
		});

		rowAction.addEventListener('click', openCustomer);

		var infoBox = Ti.UI.createView({
			height : Ti.UI.FILL,
			right : '40dp',
			touchEnabled : false
		});

		var verticalBox = Ti.UI.createView({
			top : 0,
			left: '10dp',
			height : '125dp',
			width : Ti.UI.FILL,
			layout : 'vertical',
			touchEnabled : false
		});

		var imageBox = Ti.UI.createView({
			height : '90dp',
			width : '40dp',
			right : '0dp',
			touchEnabled : false
		});

		var stop = Ti.UI.createLabel({
			left : '5dp',
			text : 'STOP ' + task.stop,
			textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
			font : Config.biginputFont,
			color : Config.accent,
			height : Ti.UI.SIZE,
			width : Ti.UI.FILL,
			wordWrap : false,
			touchEnabled : false
		});

		var customerName = Ti.UI.createLabel({
			left : '5dp',
			text : task.name.toUpperCase(),
			textAlign : Ti.UI.TEXT_ALIGNMENT_LEFT,
			font : Config.biginputFont,
			//color : Config.containerTextColor,
			color : Config.lightBlue,
			height : Ti.UI.SIZE,
			width : Ti.UI.FILL,
			wordWrap : false,
			touchEnabled : false
		});

		var customerAddress = Ti.UI.createLabel({
			left : '5dp',
			text : task.address1,
			textAlign : Ti.UI.TEXT_ALIGNMENT_LEFT,
			font : Config.optionsTitleFont,
			//color : Config.containerTextColor,
			color : Config.whiteText,
			height : Ti.UI.SIZE,
			width : Ti.UI.FILL,
			wordWrap : false,
			touchEnabled : false
		});

		var customerAddress2 = Ti.UI.createLabel({
			left : '5dp',
			text : task.address2 + ', ' + task.city_name,
			textAlign : Ti.UI.TEXT_ALIGNMENT_LEFT,
			font : Config.optionsTitleFont,
			//color : Config.containerTextColor,
			color : Config.whiteText,
			height : Ti.UI.SIZE,
			width : Ti.UI.FILL,
			wordWrap : false,
			touchEnabled : false
		});

		var boxes = Ti.UI.createLabel({
			left : '5dp',
			text : 'Cajas: ' + totalBoxes(task.id),
			textAlign : Ti.UI.TEXT_ALIGNMENT_LEFT,
			font : Config.optionsTitleFont,
			//color : Config.containerTextColor,
			color : Config.whiteText,
			height : Ti.UI.SIZE,
			width : Ti.UI.FILL,
			wordWrap : false,
			touchEnabled : false
		});

		var arrival = Ti.UI.createLabel({
			bottom : 0,
			backgroundColor : Config.softMask,
			text : (task.arrival_datetime_str) ? '  Hora programada: ' + task.arrival_datetime_str + ' hrs' : '  Hora programada: Sin datos',
			textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
			color : Config.accent,
			height : Ti.UI.SIZE,
			width : Ti.UI.FILL,
			wordWrap : false,
			touchEnabled : false
		});

		//verticalBox.add(stop);
		verticalBox.add(customerName);
		verticalBox.add(customerAddress);
		verticalBox.add(customerAddress2);
		verticalBox.add(boxes);
		//verticalBox.add(arrival);

		infoBox.add(verticalBox);

		imageBox.add(Ti.UI.createImageView({
			image : Config.images + '192_flecha.png',
			height : '30dp',
			width : '30dp',
			touchEnabled : false
		}));

		row.add(rowAction);
		row.add(infoBox);
		row.add(imageBox);
		//row.add(arrival);

		row.action = rowAction;
		//row.stop = stop;
		row.customerName = customerName;
		row.customerAddress = customerAddress;
		row.customerAddress2 = customerAddress2;
		row.boxes = boxes;
		//row.arrival = arrival;

		row.animate(Ti.UI.createAnimation({
			delay : 320 * delay,
			duration : 640,
			opacity : 1.0,
			curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
		}));

		displayedTasks[task.id] = row;
		return row;
	};

	function totalBoxes(currentTaskId) {

		var myTask = db.selectTASKSbyID(currentTaskId);
		var totalBoxes = 0;
		if (myTask.orders != null && myTask.orders != '') {
			for (var i = 0; i < myTask.orders.length; i++) {
				if (!isNaN(myTask.orders[i].quantity)) {
					totalBoxes += myTask.orders[i].quantity;
				}
			}
		}
		return totalBoxes;
	}

	function toggleSpace() {

		var newheight;
		if (topView.height == '0dp') {
			newheight = '160dp';
		} else {
			newheight = '0dp';
		}

		var anim = Ti.UI.createAnimation({
			duration : 160,
			height : newheight,
			curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
		});

		topView.animate(anim);

	}

	function showRoute(e) {
		db.updateKEYVAL({
			process_date : e.source.processDate
		});

		selectedRoute = e.source.routeId;
		db.loadTASKSbyROUTE(selectedRoute);
		scrollTasks.removeAllChildren();
		displayedTasks = {};
		scrollable.moveNext();
		refresh();
	}

	function backToRoutes(e) {
		scrollable.movePrevious();
		refresh();
	}

	function openCustomer(e) {
		var _taskId = e.source.taskId;
		var Window = require('/ui/CustomerDetail');
		new Window(refresh, selectedRoute, _taskId);
	}

	function openMap() {

		if (selectedRoute != '' && selectedRoute != null) {
			var Window = require('/ui/common/CustomersSortMap');
			new Window(refresh, selectedRoute);
		} else {
			var dialog = Ti.UI.createAlertDialog({

				title : 'Atención',
				message : 'Debe seleccionar una ruta.',
				ok : 'CERRAR'
			});

			dialog.show();
		}
	}

	function openMenu() {
		drawer.toggleLeftWindow();
	}

	//init.stopService();
	construct();

	return self;
}

module.exports = Home;
