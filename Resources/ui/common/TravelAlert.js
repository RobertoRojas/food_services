var Config = require('/libs/Config');
var summon = require('/mods/summon');
var db = require('/mods/db');
var moment = require('/libs/moment');
var pictures = require('/mods/gallery');

moment.locale(Ti.Locale.currentLanguage);

function TravelAlert() {

	var me = Ti.App.Properties.getObject('me', new Object);
	var _travelForm = db.selectFORMMASTERSbyIDENTIFIER('global_alert');
	var _taskId = 'travelAlert';
	var _routeId = 'travelAlert';
	var _priority = 1;
	var stage = '1';
	var formValues = db.selectFORMSbyVALUES(_taskId, _routeId, _priority, _travelForm.identifier);

	var selectedReason = null;

	var found = false;
	for (var f in formValues.values) {
		if ( typeof (formValues.values[f][stage]) != 'undefined') {
			found = true;
			selectedReason = formValues.values[f][stage].key;
		}
	}

	if (!found) {
		var _new = {};
		_new[stage] = {
			key : null
		};
		formValues.values.push(_new);
	}

	var last_loc = db.selectKEYVAL('last_loc');
	if (last_loc == '') {
		last_loc = null;
	} else {
		last_loc = JSON.parse(last_loc);
	}

	formValues.audit.first_validation_date = moment.utc().valueOf();
	formValues.audit.first_validation_lat = (last_loc != null) ? last_loc.lat : '';
	formValues.audit.first_validation_lon = (last_loc != null) ? last_loc.lon : '';
	formValues.audit.first_validation_accu = (last_loc != null) ? last_loc.acc : '';
	formValues.audit.task = null;
	formValues.audit.form = _travelForm.id;

	db.updateFORMS(formValues);

	var reasonContainer;

	var reasons = _travelForm.form[stage].options;

	var optionsKeys = [];
	var optionsTitle = [];

	for (var i = 0; i < reasons.length; i++) {
		optionsKeys.push(reasons[i].value);
		optionsTitle.push(reasons[i].name);
	}

	var content = Ti.UI.createView({
		height : Ti.UI.FILL,
		width : Ti.UI.FILL,
		layout : 'vertical'
	});

	var self = summon.newwindow(false, false, Config.whiteBg);
	var actionbar = summon.actionbar(_travelForm.name);
	var work = summon.fullwork();

	content.add(actionbar);

	self.add(content);
	self.add(work);

	leftButton = summon.barButton(goBack, Config.images+ 'ic_navigate_before_w.png');
	actionbar.add_left(leftButton);

	contactButton = summon.barButton(openContact, Config.images+ '192_contactos.png');
	actionbar.add_right(contactButton);

	var mainView;
	var subtitle;
	var scroll;
	var gallery;
	var okAction;

	function construct() {

		mainView = Ti.UI.createView({
			height : Ti.UI.FILL,
			width : Ti.UI.FILL,
			top : '0dp',
			layout : 'vertical',
			touchEnabled : false
		});

		var title = _travelForm.form[stage].title;
		var _titles = title.split(' ');
		var _title1 = '';
		var _title2 = '';
		for (var i in _titles) {
			if (i < _titles.length / 2) {
				_title1 += _titles[i] + ' ';
			} else {
				_title2 += _titles[i] + ' ';
			}
		}

		subtitle = summon.explanationBox(_title1, _title2, Config.images+ 'accent_alert.png');
		subtitle.top = '10dp';
		subtitle.bottom = '10dp';

		scroll = Ti.UI.createScrollView({
			//backgroundColor : Config.backgroundColor,
			backgroundImage: Config.colorBg,
			showVerticalScrollIndicator : true,
			height : Ti.UI.FILL,
			width : Ti.UI.FILL,
			top : '0dp',
			bottom : '0dp',
			scrollType : 'vertical',
			layout : 'vertical'
		});

		mainView.add(subtitle);
		mainView.add(scroll);

		content.add(mainView);

		scroll.add(getSelectedOption());
		scroll.add(takePicture());
		scroll.add(nextForm());

		refresh();
	}

	function getSelectedOption() {

		reasonContainer = Ti.UI.createView({
			top : '10dp',
			borderRadius : Config.borderRadius,
			elevation : Config.elevation,
			height : '80dp',
			top : '10dp',
			left : '10dp',
			right : '10dp',
			touchEnabled : true
		});

		var centerContainer = Ti.UI.createView({
			borderRadius : Config.borderRadius,
			borderColor : Config.textdarkgraylow,
			bottom : '20dp',
			top : '20dp',
			left : '20dp',
			right : '20dp',
			touchEnabled : false
		});

		var lblReason = Ti.UI.createLabel({
			text : (selectedReason == null) ? 'Seleccione un motivo' : optionsTitle[optionsKeys.indexOf(selectedReason)],
			left : '20dp',
			right : '40dp',
			color : Config.textdarkgray,
			font : Config.rowFont,
			height : Ti.UI.SIZE,
			width : Ti.UI.FILL,
			touchEnabled : false
		});

		reasonContainer.addEventListener('click', function(e) {

			if (selectedReason == null) {
				var dialog = Ti.UI.createOptionDialog({
					title : 'Seleccione motivo',
					options : optionsTitle
				});
			} else {
				dialog = Ti.UI.createOptionDialog({
					title : 'Seleccione motivo',
					options : optionsTitle,
					selectedIndex : optionsKeys.indexOf(selectedReason)
				});
			}

			dialog.addEventListener('click', function(evt) {

				if (evt.button != true) {
					lblReason.text = optionsTitle[evt.index];
					selectedReason = optionsKeys[evt.index];

					formValues = db.selectFORMSbyVALUES(_taskId, _routeId, _priority, _travelForm.identifier);
					for (var f in formValues.values) {
						if ( typeof (formValues.values[f][stage]) != 'undefined') {
							formValues.values[f][stage].key = selectedReason;
						}
					}

					var last_loc = db.selectKEYVAL('last_loc');
					if (last_loc == '') {
						last_loc = null;
					} else {
						last_loc = JSON.parse(last_loc);
					}

					formValues.audit.filled_date = moment.utc().valueOf();
					formValues.audit.filled_lat = (last_loc != null) ? last_loc.lat : '';
					formValues.audit.filled_lon = (last_loc != null) ? last_loc.lon : '';
					formValues.audit.filled_accu = (last_loc != null) ? last_loc.acc : '';

					db.updateFORMS(formValues);

					refresh();
				}
			});

			dialog.show();
		});

		centerContainer.add(lblReason);

		centerContainer.add(Ti.UI.createImageView({
			image : Config.images + 'white_expand_more.png',
			right : '10dp',
			height : '30dp',
			width : '30dp',
			touchEnabled : false
		}));

		reasonContainer.add(centerContainer);

		return reasonContainer;
	}

	function takePicture() {

		var container = Ti.UI.createView({
			borderRadius : Config.borderRadius,
			elevation : Config.elevation,
			height : Ti.UI.SIZE,
			top : '10dp',
			left : '10dp',
			right : '10dp',
		});

		var imagesContainer = Ti.UI.createView({
			top : 0,
			height : Ti.UI.SIZE,
			left : '0dp',
			right : '0dp',
		});

		gallery = pictures.gallery(_taskId, _routeId, _priority, _travelForm.identifier, stage, imagesContainer);
		imagesContainer.add(gallery);

		container.add(imagesContainer);
		return container;
	}

	function nextForm() {

		var text = Config.sendText;

		buttonBox = Ti.UI.createView({
			borderRadius : Config.buttonBorderRadius,
			height : Config.buttonHeight,
			width : Ti.UI.FILL,
			top : '48dp',
			left : '80dp',
			right : '80dp',
			bottom : '16dp',
			touchEnabled : false
		});

		okAction = Ti.UI.createView({
			backgroundColor : Config.neutral,
			height : Ti.UI.FILL,
			width : Ti.UI.FILL,
			touchFeedback : Config.touchFeedback,
			touchFeedbackColor : Config.buttonRippleColor,
			touchEnabled : false
		});

		var buttonLabel = Ti.UI.createLabel({
			text : text.toUpperCase(),
			textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
			font : Config.buttonFont,
			color : Config.buttonTextColor,
			height : Ti.UI.SIZE,
			width : Ti.UI.SIZE,
			touchEnabled : false
		});

		okAction.addEventListener('click', function() {
			goSend();
		});

		buttonBox.add(okAction);
		buttonBox.add(buttonLabel);

		return buttonBox;
	};

	function goSend() {
		// FORM
		formValues = db.selectFORMSbyVALUES(_taskId, _routeId, _priority, _travelForm.identifier);
		db.sendForm(formValues);
		goBack();
	}

	function goBack() {
		self.close();
	}

	function refresh() {
		checkContinue();
	}

	function checkContinue() {
		if (selectedReason != null) {
			sendUnblocked();
		} else {
			sendBlocked();
		}
	}

	function sendBlocked() {
		okAction.backgroundColor = Config.neutral;
		okAction.touchEnabled = false;
	}

	function sendUnblocked() {
		okAction.backgroundColor = Config.buttonBackgroundColor;
		okAction.touchEnabled = true;
	}

	function openContact() {
		var Window = require('/ui/common/Support');
		new Window(refresh);
	}


	self.addEventListener('android:back', function(e) {
		e.cancelBubble = true;
		goBack();
	});

	construct();

	self.open();

}

module.exports = TravelAlert;
