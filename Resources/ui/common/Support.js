var Config = require('/libs/Config');
var summon = require('/mods/summon');
var xhr = require('/mods/xhr');
var db = require('/mods/db');
var moment = require('/libs/moment');
moment.locale(Ti.Locale.currentLanguage);

var leftButton;
//var rightButton;

function SupportContact(_refresh) {
    
    var app_version = Ti.App.Properties.getObject('app_version_2', false);
    var contactInfo = null;
 
    if (Config.mode) {
        Ti.API.info('------------------------------------- SUPPORT -------------------------------------');
        Ti.API.info('APP VERSION: '+ JSON.stringify(app_version));
    }

    var content = Ti.UI.createView({
        height : Ti.UI.FILL,
        width : Ti.UI.FILL
    });

    var self = summon.newwindow(false, true, Config.whiteBg);
    var actionbar = summon.actionbar('Libreta de contactos');
    var overview = summon.overview();
    var work = summon.fullwork();

    if (_refresh != undefined && _refresh != null) {
        leftButton = summon.barButton(goBack, Config.images+ 'ic_navigate_before_w.png');
        actionbar.add_left(leftButton);
    }

    var scroll;

    content.add(actionbar);

    self.add(content);
    self.add(work);

    function constructMask() {

        scroll = Ti.UI.createScrollView({
            showVerticalScrollIndicator : true,
            width : Ti.UI.FILL,
            top : '200dp',
            bottom : '0dp',
            scrollType : 'vertical'
        });

        scroll.add(Ti.UI.createImageView({
            left : '0dp',
            right : '0dp',
            top : '0dp',
            bottom : '0dp',
            image : Config.colorBg,
            touchEnabled : false
        }));

        var circleTop = Ti.UI.createView({
            top : '140dp',
            height : '140dp',
            width : '140dp',
            touchEnabled : false
        });

        circleTop.add(Ti.UI.createImageView({
            top : '0dp',
            image : Config.circle,
            height : '140dp',
            width : '140dp'
        }));

        circleTop.add(Ti.UI.createImageView({
            top : '15dp',
            image : Config.images + 'accent_alert.png',
            height : '40dp',
            width : '40dp'
        }));

        var circleBottom = Ti.UI.createView({
            top : '-70dp',
            height : '140dp',
            width : '140dp',
            touchEnabled : false
        });

        circleBottom.add(Ti.UI.createImageView({
            top : '0dp',
            image : Config.circle,
            height : '140dp',
            width : '138dp'
        }));

        makeWinTitle();
        content.add(circleTop);
        scroll.add(circleBottom);

        content.add(scroll);
    }

    function construct() {

    }

    function makeWinTitle() {

        var container = Ti.UI.createView({
            top : '65dp',
            height : '80dp',
            left : '0dp',
            right : '0dp',
            touchEnabled : false
        });
        
        var title1= 'contacto con'.toUpperCase;
        var title2= 'supervisor'.toUpperCase;
        container.add(summon.transparentCentertitle(title1, title2, Config.darkText));
        content.add(container);
    }

    function returnkey(e) {
        if (e.type == 'return') {

        }
    }

    function goBack() {

    }


    self.addEventListener('android:back', function(e) {
        e.cancelBubble = true;
        goBack();
    });

    constructMask();
    self.open();
}

module.exports = SupportContact;
