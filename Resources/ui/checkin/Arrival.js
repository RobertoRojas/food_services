var Config = require('/libs/Config');
var summon = require('/mods/summon');
var db = require('/mods/db');
var moment = require('/libs/moment');
var pictures = require('/mods/gallery');

moment.locale(Ti.Locale.currentLanguage);

function Arrival(_refresh, _routeId, _taskId, _arrivalForm, _index, _identifier, next, step) {

    Ti.API.info('----------------------- ARRIVAL -----------------------');
    Ti.API.info('_arrival form: ' + JSON.stringify(_arrivalForm));

    _arrivalForm = {
        "field_type" : "timedPickerList",
        "field_name" : "La causal de retraso es obligatoria",
        "validation_fields" : [{
            "name" : "server_time",
            "format" : "datetime"
        }, {
            "name" : "arrival_datetime",
            "format" : "datetime",
            "concat_field" : "process_date"
        }],
        "validation_op" : "diff",
        "delta_time" : 1800000,
        "title" : "INGRESO DE Kilometraje",
        "action" : {
            "type" : "action",
            "value" : "none"
        },
        "mode" : "single",
        "options" : [{
            "value" : "1",
            "name" : "Salida tarde CD",
            "options" : []
        }, {
            "value" : "2",
            "name" : "Accidente en ruta genera congestion",
            "options" : []
        }, {
            "value" : "3",
            "name" : "Falla mecánica en ruta",
            "options" : []
        }, {
            "value" : "4",
            "name" : "Retraso descarga stop anterior",
            "options" : []
        }, {
            "value" : "5",
            "name" : "Tiempo insuficiente entre stop",
            "options" : []
        }, {
            "value" : "6",
            "name" : "Demora en abasto",
            "options" : []
        }, {
            "value" : "7",
            "name" : "Demora por tránsito",
            "options" : []
        }, {
            "value" : "8",
            "name" : "Estacionamiento ocupado",
            "options" : []
        }],
        "isMandatory" : "0"
    };

    var task = db.selectTASKSbyID(_taskId);
    var stage = _index;
    var formValues = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, _identifier);
    var selectedReason;

    for (var f in formValues.values) {
        if ( typeof (formValues.values[f][stage]) != 'undefined') {
            selectedReason = formValues.values[f][stage];
        }
    }

    var reasonContainer;
    var reasons = _arrivalForm.options;

    var optionsKeys = [];
    var optionsTitle = [];

    for (var i = 0; i < reasons.length; i++) {
        optionsKeys.push(reasons[i].value);
        optionsTitle.push(reasons[i].name);
    }

    var content = Ti.UI.createView({
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        layout : 'vertical'
    });

    var mainView;
    var subtitle;
    var scroll;
    var body;
    var gallery;

    function construct() {

        mainView = Ti.UI.createView({
            height : Ti.UI.FILL,
            width : Ti.UI.FILL,
            top : '0dp',
            //layout : 'vertical',
            touchEnabled : false
        });

        scroll = Ti.UI.createScrollView({
            backgroundColor : Config.backgroundColor,
            showVerticalScrollIndicator : true,
            height : Ti.UI.FILL,
            width : Ti.UI.FILL,
            top : '0dp',
            bottom : '0dp',
            scrollType : 'vertical',
            //layout : 'vertical'
        });

        mainView.add(scroll);
        scroll.add(makeTitle());
        
        body= makebody();
        scroll.add(body);
      
        content.add(mainView);

        body.add(getSelectedOption());
        body.add(next);

        refresh();
    }

    function makeTitle(title1, title2, backImage, icon) {

        var tittleView= summon.roundFsTitle(_arrivalForm.title, Config.whiteBg, Config.images+ 'white_truck.png', Config.circleColor);
        return tittleView;
    }

    function makebody() {

        var container = Ti.UI.createView({
            top : '150dp',
            height : Ti.UI.FILL,
            width : Ti.UI.FILL,
            backgroundColor: Config.circleColor,
            layout: 'vertical',
            touchEnabled : true
        });
        
        // container.add(Ti.UI.createImageView({
            // top : '0dp',
            // left : 0,
            // height : Ti.UI.FILL,
            // width : Ti.UI.FILL,
            // image : Config.colorBg,
            // touchEnabled : false
        // }));
        
        return container;
    }

    function getSelectedOption() {

        reasonContainer = Ti.UI.createView({
            top : '10dp',
            height : '80dp',
            top : '10dp',
            left : '10dp',
            right : '10dp',
            touchEnabled : true
        });

        var centerContainer = Ti.UI.createView({
            //borderRadius : Config.borderRadius,
            backgroundColor: Config.button_color,
            borderColor : Config.borderGray,
            bottom : '20dp',
            top : '20dp',
            left : '20dp',
            right : '20dp',
            touchEnabled : false
        });

        var lblReason = Ti.UI.createLabel({
            text : (selectedReason == null) ? 'Seleccione un motivo' : optionsTitle[optionsKeys.indexOf(selectedReason)],
            left : '20dp',
            right : '40dp',
            color : Config.whiteText,
            font : Config.rowFont,
            height : Ti.UI.SIZE,
            width : Ti.UI.FILL,
            touchEnabled : false
        });

        reasonContainer.addEventListener('click', function(e) {

            if (selectedReason == null) {
                var dialog = Ti.UI.createOptionDialog({
                    title : 'Seleccione motivo',
                    options : optionsTitle
                });
            } else {
                dialog = Ti.UI.createOptionDialog({
                    title : 'Seleccione motivo',
                    options : optionsTitle,
                    selectedIndex : optionsKeys.indexOf(selectedReason)
                });
            }

            dialog.addEventListener('click', function(evt) {

                if (evt.button != true) {
                    lblReason.text = optionsTitle[evt.index];
                    selectedReason = optionsKeys[evt.index];

                    formValues = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, _identifier);
                    for (var f in formValues.values) {
                        if ( typeof (formValues.values[f][stage]) != 'undefined') {
                            formValues.values[f][stage] = selectedReason;
                        }
                    }

                    var last_loc = db.selectKEYVAL('last_loc');
                    if (last_loc == '') {
                        last_loc = null;
                    } else {
                        last_loc = JSON.parse(last_loc);
                    }

                    formValues.audit.filled_date = moment.utc().valueOf();
                    formValues.audit.filled_lat = (last_loc != null) ? last_loc.lat : '';
                    formValues.audit.filled_lon = (last_loc != null) ? last_loc.lon : '';
                    formValues.audit.filled_accu = (last_loc != null) ? last_loc.acc : '';

                    db.updateFORMS(formValues);

                    refresh();
                }
            });

            dialog.show();
        });

        centerContainer.add(lblReason);

        centerContainer.add(Ti.UI.createImageView({
            image : Config.images + 'white_expand_more.png',
            right : '10dp',
            height : '30dp',
            width : '30dp',
            touchEnabled : false
        }));

        reasonContainer.add(centerContainer);

        return reasonContainer;
    }

    function refresh() {
        task = db.selectTASKSbyID(_taskId);
        if (task == null) {
            _refresh();
        } else {
            formValues = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, _identifier);
            if (formValues.sent == 1) {
                _refresh();
            } else {
                checkContinue();
            }
        }
    }

    function checkContinue() {
        if (selectedReason == null) {
            sendBlocked();
        } else {
            sendUnblocked();
        }
    }

    function sendBlocked() {
        next.action.backgroundColor = Config.neutral;
        next.action.touchEnabled = false;
    }

    function sendUnblocked() {
        next.action.backgroundColor = Config.buttonBackgroundColor;
        next.action.touchEnabled = true;
    }

    construct();

    return content;
}

module.exports = Arrival;
