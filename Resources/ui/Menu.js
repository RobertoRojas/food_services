var Config = require('/libs/Config');
var summon = require('/mods/summon');
var init = require('/mods/initializers');
var xhr = require('/mods/xhr');
var db = require('/mods/db');
var push = require('/mods/push');
var NappDrawerModule = require('dk.napp.drawer');
var moment = require('/libs/moment');
moment.locale(Ti.Locale.currentLanguage);

var drawer;
var SideMenu;

function Menu() {

	var now = moment();

	var view;

	var close = Ti.UI.createAlertDialog({
		cancel : 1,
		buttonNames : ['Cerrar', 'Cancelar'],
		message : '¿Seguro que quiere salir?',
		title : 'Cerrar Aplicación'
	});
	close.addEventListener('click', function(e) {
		if (e.index == 0) {
			drawer.close();
		}
	});

	var exit = Ti.UI.createAlertDialog({
		cancel : 1,
		buttonNames : ['Salir', 'Cancelar'],
		message : '¿Seguro que quiere cerrar su sesión y salir de la aplicación?',
		title : 'Cerrar Sesión'
	});

	exit.addEventListener('click', function(e) {
		if (e.index == 0) {
			db.updateKEYVAL(Config.KEYVAL_RESET);
			db.dropBD();
			Ti.App.Properties.removeAllProperties();
			init.stopService();
			drawer.close();
		}
	});
	function construct() {

		SideMenu = require('/ui/SideMenu');
		SideMenu = new SideMenu();

		SideMenu.addEventListener('click', function(e) {
			if ('identifier' in e.source) {
				switch(e.source.identifier) {
				case 'userData':
					openUpdateUser();
					drawer.toggleLeftWindow();
					break;
				case 'contact':
					openContact();
					drawer.toggleLeftWindow();
					break;
				case 'travelAlert':
					openTravelAlert();
					drawer.toggleLeftWindow();
					break;
				default:
					break;
				}
			}
		});

		view = Ti.UI.createView({
			backgroundColor : Config.backgroundColor,
			height : Ti.UI.FILL,
			width : Ti.UI.FILL
		});

		drawer = NappDrawerModule.createDrawer({
			navBarHidden : false,
			exitOnClose : true,
			fullscreen : false,
			windowSoftInputMode : Config.softInput,
			leftWindow : SideMenu,
			centerWindow : view,
			fading : 0.0,
			parallaxAmount : 0.0,
			shadowWidth : '0dp',
			leftDrawerWidth : Ti.UI.FILL,
			rightDrawerWidth : '240dp',
			animationMode : NappDrawerModule.ANIMATION_NONE,
			closeDrawerGestureMode : NappDrawerModule.CLOSE_MODE_NONE,
			openDrawerGestureMode : NappDrawerModule.OPEN_MODE_NONE,
			orientationModes : Config.orientation
		});

		drawer.closeApp = close;
		drawer.exitApp = exit;

		drawer.addEventListener('open', function(e) {
			openHome();
		});

		drawer.addEventListener('close', function(e) {
			// init.stopGPS();
		});

		drawer.open();

	}

	function openHome() {
		var Window = require('/ui/Home');
		new Window(drawer);
	}

	function openUpdateUser() {
		// var Window = require('/ui/UpdateUser');
		// new Window();
	}

	function openContact() {
		var Window = require('/ui/common/Support');
		new Window();
	}

	function openTravelAlert() {
		var Window = require('/ui/common/TravelAlert');
		new Window();
	}

	construct();

	drawer.addEventListener('android:back', function(e) {
		e.cancelBubble = true;
		if (drawer.isAnyWindowOpen()) {
			if (drawer.isLeftWindowOpen()) {
				drawer.toggleLeftWindow();
			}
			if (drawer.isRightWindowOpen()) {
				drawer.toggleRightWindow();
			}
		} else {
			if (Config.scrollable && Config.scrollable.currentPage != 0) {
				Config.scrollable.backToRoutes();
			} else {
				drawer.closeApp.show();
			}
		}
	});

	drawer.openHome = openHome;

	Config.drawer = drawer;

}

module.exports = Menu;
