var Config = require('/libs/Config');
var summon = require('/mods/summon');
var db = require('/mods/db');
var moment = require('/libs/moment');
var pictures = require('/mods/gallery');

moment.locale(Ti.Locale.currentLanguage);

function Rejection(_refresh, _routeId, _taskId, _rejectionForm, _index, _identifier, next, step) {
    
	var task = db.selectTASKSbyID(_taskId);
	var stage = _index;
	var _priority = 1;
	
	var formValues = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, _identifier);
	var selectedReason;
	for (var f in formValues.values) {
		if ( typeof (formValues.values[f][stage]) != 'undefined') {
			selectedReason = formValues.values[f][stage];
		}
	}
	db.updateFORMS(formValues);

	var reasonContainer;
	var reasons = _rejectionForm.options;

	var optionsKeys = [];
	var optionsTitle = [];

	for (var i = 0; i < reasons.length; i++) {
		optionsKeys.push(reasons[i].value);
		optionsTitle.push(reasons[i].name);
	}

	var content = Ti.UI.createView({
		height : Ti.UI.FILL,
		width : Ti.UI.FILL,
		layout : 'vertical'
	});

	var mainView;
	var subtitle;
	var scroll;
	var gallery;

	function construct() {

		mainView = Ti.UI.createView({
			height : Ti.UI.FILL,
			width : Ti.UI.FILL,
			top : '0dp',
			layout : 'vertical',
			touchEnabled : false
		});

        var iconTitle= Config.images + 'white_close.png';
		subtitle = summon.roundFsTitle(_rejectionForm.title, 'white', Config.circle, iconTitle);
		
		subtitle.top = '10dp';
		subtitle.bottom = '10dp';

		scroll = Ti.UI.createScrollView({
			showVerticalScrollIndicator : true,
			height : Ti.UI.FILL,
			width : Ti.UI.FILL,
			top : '0dp',
			bottom : '0dp',
			scrollType : 'vertical',
			layout : 'vertical'
		});

		mainView.add(subtitle);
		mainView.add(scroll);

		content.add(mainView);

		scroll.add(getSelectedOption());
		scroll.add(takePicture());		
		scroll.add(next);

		refresh();
	}

	function getSelectedOption() {

		reasonContainer = Ti.UI.createView({
			top : '10dp',
			elevation : Config.elevation,
			height : '80dp',
			top : '0dp',
			left : '0dp',
			right : '0dp',
			touchEnabled : true
		});

		var centerContainer = Ti.UI.createView({
			borderRadius : Config.borderRadius,
			borderColor : Config.borderGray,
			backgroundColor: Config.button_color,
			bottom : '20dp',
			top : '20dp',
			left : '20dp',
			right : '20dp',
			touchEnabled : false
		});

		var lblReason = Ti.UI.createLabel({
			text : (selectedReason == null) ? 'Seleccione un motivo' : optionsTitle[optionsKeys.indexOf(selectedReason)],
			left : '20dp',
			right : '40dp',
			color : Config.whiteText,
			font : Config.rowFont,
			height : Ti.UI.SIZE,
			width : Ti.UI.FILL,
			touchEnabled : false
		});
		
		reasonContainer.addEventListener('click', function(e) {

			if (selectedReason == null) {
				var dialog = Ti.UI.createOptionDialog({
					title : 'Seleccione motivo',
					options : optionsTitle
				});
			} else {
				dialog = Ti.UI.createOptionDialog({
					title : 'Seleccione motivo',
					options : optionsTitle,
					selectedIndex : optionsKeys.indexOf(selectedReason)
				});
			}

			dialog.addEventListener('click', function(evt) {

				if (evt.button != true) {
					lblReason.text = optionsTitle[evt.index];
					selectedReason = optionsKeys[evt.index];

					formValues = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, _identifier);
					for (var f in formValues.values) {
						if ( typeof (formValues.values[f][stage]) != 'undefined') {
							formValues.values[f][stage] = selectedReason;
						}
					}

					var last_loc = db.selectKEYVAL('last_loc');
					if (last_loc == '') {
						last_loc = null;
					} else {
						last_loc = JSON.parse(last_loc);
					}

					formValues.audit.filled_date = moment.utc().valueOf();
					formValues.audit.filled_lat = (last_loc != null) ? last_loc.lat : '';
					formValues.audit.filled_lon = (last_loc != null) ? last_loc.lon : '';
					formValues.audit.filled_accu = (last_loc != null) ? last_loc.acc : '';

					db.updateFORMS(formValues);

					refresh();
				}
			});

			dialog.show();
		});

		centerContainer.add(lblReason);

		centerContainer.add(Ti.UI.createImageView({
			image : Config.images + 'white_expand_more.png',
			right : '10dp',
			height : '30dp',
			width : '30dp',
			touchEnabled : false
		}));

		reasonContainer.add(centerContainer);

		return reasonContainer;
	}
	
	function takePicture() {

        var container = Ti.UI.createView({
            borderRadius : Config.borderRadius,
            height : Ti.UI.SIZE,
            top : '10dp',
            left : '20dp',
            right : '20dp'
        });

        var imagesContainer = Ti.UI.createView({
            top : 0,
            height : Ti.UI.SIZE,
            left : '0dp',
            right : '0dp'
        });

        gallery = pictures.gallery(_taskId, _routeId, _priority, 'rejection', stage, imagesContainer);
        imagesContainer.add(gallery);

        container.add(imagesContainer);
        return container;
    }

	function refresh() {
		task = db.selectTASKSbyID(_taskId);
		if (task == null) {
			_refresh();
		} else {
			formValues = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, _identifier);
			if (formValues.sent == 1) {
				_refresh();
			} else {
				checkContinue();
			}
		}
	}

	function checkContinue() {
		if (_rejectionForm.isMandatory == '1' && selectedReason == null) {
			sendBlocked();
		} else {
			sendUnblocked();
		}
	}

	function sendBlocked() {
		next.action.backgroundColor = Config.neutral;
		next.action.touchEnabled = false;
	}

	function sendUnblocked() {
		next.action.backgroundColor = Config.buttonBackgroundColor;
		next.action.touchEnabled = true;
	}

	construct();
	return content;
}

module.exports = Rejection;
