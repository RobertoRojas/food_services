var Config = require('/libs/Config');
var summon = require('/mods/summon');
var xhr = require('/mods/xhr');
var db = require('/mods/db');
var moment = require('/libs/moment');
moment.locale(Ti.Locale.currentLanguage);

var leftButton;
var productList;
var map;
var waze;
var invoices;

function CustomerDetail(_refresh, _routeId, _taskId) {

    var masterForms = db.selectFORMMASTERSasOBJECT();
    var task = db.selectTASKSbyID(_taskId);
    var taskForms = task.forms;

    var totalBoxes = 0;
    var checkin_option = 'checkin';
    var checkin_form_id = null;
    var checkin_form = null;
    var offstart_option = 'offload_start';
    var offstart_form_id = null;
    var offstart_form = null;
    var deliver_option = 'deliver';
    var deliver_form_id = null;
    var deliver_form = null;
    var manifest_option = 'manifest';
    var manifest_form_id = null;
    var manifest_form = null;
    var rejection_option = 'rejection';
    var rejection_form_id = null;
    var rejection_form = null;
    var formsToCheck = ['checkin', 'offload_start', 'deliver'];

    if (Config.mode) {
        Ti.API.error('-------------------- CUSTOMER DETAIL --------------------');
        Ti.API.info('_ROUTE ID: ' + _routeId);
        Ti.API.info('_TASK ID: ' + _taskId);
        Ti.API.info('TASK: ' + JSON.stringify(task));
        Ti.API.info('MASTER FORMS: ' + JSON.stringify(masterForms));
        Ti.API.info('TASK FORMS: ' + JSON.stringify(taskForms));
        Ti.API.error('---------------------------------------------------------');
    }

    var content = Ti.UI.createView({
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        layout : 'vertical'
    });

    var self = summon.newwindow(false, false, Config.colorBg);
    var actionbar = summon.actionbar('Detalle');
    var overview = summon.overview();
    var work = summon.fullwork();

    content.add(actionbar);

    self.add(content);
    self.add(overview);
    self.add(work);

    leftButton = summon.barButton(goBack, Config.images+ 'ic_navigate_before_w.png');
    actionbar.add_left(leftButton);

    productList = summon.barButton(goList, Config.images+ 'white_list.png');
    actionbar.add_left(productList);

    map = summon.barButton(goMap, Config.images+ 'white_pin.png');
    actionbar.add_right(map);

    waze = summon.barButton(goWaze, Config.images+ 'white_waze.png');
    actionbar.add_right(waze);

    invoices = summon.barButton(goInvoices, Config.images+ 'white_invoice_detail.png');
    actionbar.add_right(invoices);

    var mainView;
    var subtitle;
    var subView;
    var scroll;

    var cpb = require('/libs/circularProgressBar');
    var progress;
    var progressTimeout;
    var progressInterval;
    var done;

    var invoicesNumber = [];
    var invoicesIndex = [];
    var selectedInvoiceKey = null;
    var selectedInvoiceNumber = null;

    function construct() {

        // MAINVIEW
        mainView = Ti.UI.createView({
            height : Ti.UI.FILL,
            width : Ti.UI.FILL,
            layout : 'vertical',
            touchEnabled : false
        });

        subtitle = summon.customerRoundtitle(task.customer_id, 'STOP ' + task.stop, task.name, '+56948463206');

        scroll = Ti.UI.createScrollView({
            showVerticalScrollIndicator : true,
            height : Ti.UI.FILL,
            width : Ti.UI.FILL,
            top : '0dp',
            bottom : '0dp',
            scrollType : 'vertical',
            layout : 'vertical'
        });

        mainView.add(subtitle);
        mainView.add(scroll);

        content.add(mainView);

        // DETAIL

        var addressText = task.address1 + ', ' + task.city_name;
        var addressIcon = Config.images+ 'white_pin.png';
        
        if (task.orders != null && task.orders != '') {
            for (var i = 0; i < task.orders.length; i++) {
                if (!isNaN(task.orders[i].quantity)) {
                    totalBoxes += task.orders[i].quantity;
                }
            }
        }

        var timeIcon = Config.images+ 'white_clock.png';
        var arrivalText = (task.arrival_datetime) ? ('Hora programada: ' + moment.utc(task.arrival_datetime).format('HH:mm').toString()) : 'Hora programada: Sin datos';
        
        scroll.add(uiInfoR(addressText, Config.whiteText, addressIcon));
        scroll.add(uiInfoR('Cantidad de Cajas: ' + totalBoxes, Config.whiteText, Config.images+ 'white_box.png'));
        scroll.add(uiInfoR(arrivalText, Config.whiteText, timeIcon));

        // OVERVIEW
        if (task.comments != null) {
            showComments(JSON.parse(task.comments));
        }
        
        var popup = Ti.UI.createView({
            backgroundColor : Config.backgroundColor,
            borderRadius : Config.borderRadius,
            elevation : Config.elevation,
            height : '240dp',
            width : Ti.UI.FILL,
            left : '32dp',
            right : '32dp',
            touchEnabled : false
        });

        done = 0;

        progress = cpb({
            percent : done,
            size : 100,
            radius : 50,
            margin : 0,
            backgroundColor : Config.hintgray,
            progressColor : Config.orange,
            topper : {
                color : Config.backgroundColor,
                size : 72,
                radius : 36
            },
            font : {
                visible : true,
                color : Config.orange,
                size : 36
            }
        });

        popup.add(progress);
        overview.add(popup);

        // SUBVIEW

        subView = Ti.UI.createView({
            height : Ti.UI.SIZE,
            width : Ti.UI.FILL,
            top : '30dp',
            bottom : '20dp',
            layout : 'vertical',
            touchEnabled : false
        });

        scroll.add(subView);

        refresh();

    }

    function refresh() {

        task = db.selectTASKSbyID(_taskId);

        if (task == null) {

            goBack();

        } else {

            subView.removeAllChildren();

            checkin_option = 'checkin';
            checkin_form_id = null;
            checkin_form = null;
            offstart_option = 'offload_start';
            offstart_form_id = null;
            offstart_form = null;
            deliver_option = 'deliver';
            deliver_form_id = null;
            deliver_form = null;
            manifest_option = 'manifest';
            manifest_form_id = null;
            manifest_form = null;
            rejection_option = 'rejection';
            rejection_form_id = null;
            rejection_form = null;

            for (var f in taskForms) {
                if (masterForms[taskForms[f].form_master_id].identifier == checkin_option) {
                    checkin_form_id = masterForms[taskForms[f].form_master_id].id;
                    checkin_form = masterForms[taskForms[f].form_master_id];
                }
                if (masterForms[taskForms[f].form_master_id].identifier == offstart_option) {
                    offstart_form_id = masterForms[taskForms[f].form_master_id].id;
                    offstart_form = masterForms[taskForms[f].form_master_id];
                }
                if (masterForms[taskForms[f].form_master_id].identifier == deliver_option) {
                    deliver_form_id = masterForms[taskForms[f].form_master_id].id;
                    deliver_form = masterForms[taskForms[f].form_master_id];
                }
                if (masterForms[taskForms[f].form_master_id].identifier == manifest_option) {
                    manifest_form_id = masterForms[taskForms[f].form_master_id].id;
                    manifest_form = masterForms[taskForms[f].form_master_id];
                }
                if (masterForms[taskForms[f].form_master_id].identifier == rejection_option) {
                    rejection_form_id = masterForms[taskForms[f].form_master_id].id;
                    rejection_form = masterForms[taskForms[f].form_master_id];
                }
            }

            var checkinValues = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, checkin_option);
            if (checkin_form && checkinValues.sent != 1) {
                showCheckin();

                var rejectionValues = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, rejection_option);
                if (rejection_form && rejectionValues.sent != 1) {
                    showRejection();
                }

            } else {

                var offstartValues = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, offstart_option);
                if (offstart_form && offstartValues.sent != 1) {
                    showOffstart();

                    var rejectionValues = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, rejection_option);
                    if (rejection_form && rejectionValues.sent != 1) {
                        showRejection();
                    }

                } else {

                    var deliverValues = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, deliver_option);
                    if (deliver_form && deliverValues.sent != 1) {
                        showDeliver();

                    } else {

                        var manifestValues = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, manifest_option);
                        if (manifest_form && manifestValues.sent != 1) {
                            showManifest();
                        }
                    }
                }
            }
        }
    }

    function showComments(comments) {

        var titleContainer = summon.roundtitle('COMENTARIOS ENTREGA');
        titleContainer.top = '10dp';
        titleContainer.bottom = '10dp';

        scroll.add(titleContainer);

        var container = Ti.UI.createView({
            backgroundColor : Config.containerBackgroundColor,
            borderRadius : Config.borderRadius,
            elevation : Config.elevation,
            height : '100dp',
            width : Ti.UI.FILL,
            top : '10dp',
            left : '10dp',
            right : '10dp',
            bottom : '10dp'
        });

        container.add(Ti.UI.createLabel({
            text : comments,
            color : Config.inputTextColor,
            font : Config.rowFont,
            height : Ti.UI.SIZE,
            width : Ti.UI.SIZE,
            left : '10dp',
            right : '10dp',
            // ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
            // wordWrap : false,
            touchEnabled : false
        }));

        scroll.add(container);
    }

    var leftForm = '60dp';
    var rightForm = '60dp';
    var heightForm = '45dp';

    function showCheckin() {

        var container = Ti.UI.createView({
            elevation : Config.elevation,
            width : Ti.UI.FILL,
            top : '5dp',
            height : heightForm,
            left : leftForm,
            right : rightForm,
            bottom : '5dp',
            borderWidth : 1,
            backgroundColor : Config.borderGray,
            touchEnabled : false
        });

        var action = Ti.UI.createView({
            backgroundColor : Config.button_color,
            height : Ti.UI.FILL,
            width : Ti.UI.FILL,
            touchFeedback : Config.touchFeedback,
            touchFeedbackColor : Config.orange,
            touchEnabled : true
        });

        action.addEventListener('click', goCheckin);

        action.add(Ti.UI.createLabel({
            text : checkin_form.name.toUpperCase(),
            textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
            color : Config.whiteText,
            font : Config.rowFont,
            height : Ti.UI.SIZE,
            width : Ti.UI.FILL,
            touchEnabled : false
        }));

        container.add(action);
        subView.add(container);
    }

    function showOffstart() {

        var container = Ti.UI.createView({
            elevation : Config.elevation,
            width : Ti.UI.FILL,
            top : '5dp',
            height : heightForm,
            left : leftForm,
            right : rightForm,
            bottom : '5dp',
            borderWidth : 1,
            backgroundColor : Config.borderGray,
            touchEnabled : false
        });

        var action = Ti.UI.createView({
            backgroundColor : Config.button_color,
            height : Ti.UI.FILL,
            width : Ti.UI.FILL,
            touchFeedback : Config.touchFeedback,
            touchFeedbackColor : Config.orange,
            touchEnabled : true
        });

        action.addEventListener('click', goOffstart);

        action.add(Ti.UI.createLabel({
            text : offstart_form.name.toUpperCase(),
            textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
            color : Config.whiteText,
            font : Config.biginputFont,
            //font : Config.biginputFont,
            font : Config.subtitleFont,
            height : Ti.UI.SIZE,
            width : Ti.UI.FILL,
            touchEnabled : false
        }));

        container.add(action);
        subView.add(container);
    }

    function showDeliver() {

        var container = Ti.UI.createView({
            elevation : Config.elevation,
            width : Ti.UI.FILL,
            top : '5dp',
            height : heightForm,
            left : leftForm,
            right : rightForm,
            bottom : '5dp',
            borderWidth : 1,
            backgroundColor : Config.borderGray,
            touchEnabled : false
        });

        var action = Ti.UI.createView({
            backgroundColor : Config.button_color,
            height : Ti.UI.FILL,
            width : Ti.UI.FILL,
            touchFeedback : Config.touchFeedback,
            touchFeedbackColor : Config.orange,
            touchEnabled : true
        });

        action.addEventListener('click', goDeliver);

        action.add(Ti.UI.createLabel({
            text : deliver_form.name.toUpperCase(),
            textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
            color : Config.whiteText,
            font : Config.biginputFont,
            //font : Config.biginputFont,
            font : Config.subtitleFont,
            height : Ti.UI.SIZE,
            width : Ti.UI.FILL,
            touchEnabled : false
        }));

        container.add(action);
        subView.add(container);
    }

    function showManifest() {

        var container = Ti.UI.createView({
            elevation : Config.elevation,
            width : Ti.UI.FILL,
            top : '5dp',
            height : heightForm,
            left : leftForm,
            right : rightForm,
            bottom : '5dp',
            borderWidth : 1,
            backgroundColor : Config.borderGray,
            touchEnabled : false
        });

        var action = Ti.UI.createView({
            backgroundColor : Config.button_color,
            height : Ti.UI.FILL,
            width : Ti.UI.FILL,
            touchFeedback : Config.touchFeedback,
            touchFeedbackColor : Config.orange,
            touchEnabled : true
        });

        action.addEventListener('click', goManifest);

        action.add(Ti.UI.createLabel({
            text : manifest_form.name.toUpperCase(),
            textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
            color : Config.whiteText,
            font : Config.biginputFont,
            //font : Config.biginputFont,
            font : Config.subtitleFont,
            height : Ti.UI.SIZE,
            width : Ti.UI.FILL,
            touchEnabled : false
        }));

        container.add(action);
        subView.add(container);
    }

    function showRejection() {

        var container = Ti.UI.createView({
            elevation : Config.elevation,
            width : Ti.UI.FILL,
            top : '20dp',
            height : heightForm,
            left : leftForm,
            right : rightForm,
            bottom : '5dp',
            borderWidth : 1,
            backgroundColor : Config.borderGray,
            touchEnabled : false
        });

        var action = Ti.UI.createView({
            backgroundColor : Config.button_color,
            height : Ti.UI.FILL,
            width : Ti.UI.FILL,
            touchFeedback : Config.touchFeedback,
            touchFeedbackColor : Config.orange,
            touchEnabled : true
        });

        action.addEventListener('click', goRejection);

        action.add(Ti.UI.createLabel({
            text : rejection_form.name.toUpperCase(),
            textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
            color : Config.whiteText,
            font : Config.rowFont,
            height : Ti.UI.SIZE,
            width : Ti.UI.FILL,
            touchEnabled : false
        }));

        container.add(action);
        subView.add(container);
    }

    function doubleTextRow(text1, text2, colorText, url) {

        var container = Ti.UI.createView({
            //backgroundColor : Config.containerBackgroundColor,
            backgroundColor : Config.fs_colorLight,
            borderRadius : Config.borderRadius,
            backgroundColor : 'cyan',
            //elevation : Config.elevation,
            height : '70dp',
            width : Ti.UI.FILL,
            top : '10dp',
            left : '10dp',
            right : '10dp',
            touchEnabled : false
        });

        var action = Ti.UI.createView({
            backgroundColor : Config.fs_colorLight,
            height : Ti.UI.FILL,
            width : Ti.UI.FILL,
            touchFeedback : Config.touchFeedback,
            touchFeedbackColor : Config.accent,
            touchEnabled : true
        });

        var textView = Ti.UI.createView({
            height : Ti.UI.SIZE,
            width : Ti.UI.FILL,
            left : '70dp',
            right : '10dp',
            layout : 'vertical',
            touchEnabled : false
        });

        textView.add(Ti.UI.createLabel({
            text : text1,
            textAlign : Ti.UI.TEXT_ALIGNMENT_LEFT,
            color : colorText,
            font : Config.biginputFont,
            height : Ti.UI.SIZE,
            width : Ti.UI.FILL,
            ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
            wordWrap : false,
            touchEnabled : false
        }));

        textView.add(Ti.UI.createLabel({
            text : text2,
            textAlign : Ti.UI.TEXT_ALIGNMENT_LEFT,
            color : colorText,
            font : Config.biginputFont,
            height : Ti.UI.SIZE,
            width : Ti.UI.FILL,
            ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
            wordWrap : false,
            touchEnabled : false
        }));

        var imageView = Ti.UI.createView({
            //backgroundColor : Config.softMask,
            backgroundColor : Config.fs_colorDark,
            height : '50dp',
            width : '50dp',
            left : '10dp',
            borderRadius : '25dp',
            touchEnabled : false
        });

        imageView.add(Ti.UI.createImageView({
            image : url,
            height : '35dp',
            width : '35dp',
            touchEnabled : false
        }));

        container.add(action);
        container.add(textView);
        container.add(imageView);

        return container;
    }

    function uiInfoR(text, colorText, url) {

        var qtyText = false;
        qtyText = text.split(':');

        var container = Ti.UI.createView({
            backgroundColor : Config.fs_colorLight,
            //elevation : Config.elevation,
            borderRadius : Config.borderRadius,
            height : '60dp',
            width : Ti.UI.FILL,
            top : '10dp',
            left : '10dp',
            right : '10dp',
            touchEnabled : false
        });

        var action = Ti.UI.createView({
            backgroundColor : Config.fs_colorLight,
            height : Ti.UI.FILL,
            width : Ti.UI.FILL,
            touchFeedback : Config.touchFeedback,
            touchFeedbackColor : Config.accent,
            borderRadius: 1,
            touchEnabled : true
        });

        var textView = Ti.UI.createView({
            height : Ti.UI.SIZE,
            width : Ti.UI.FILL,
            left : '70dp',
            right : '0dp',
            touchEnabled : false
        });

        textView.add(Ti.UI.createLabel({
            left : 0,
            text : qtyText[0],
            //text : text,
            textAlign : Ti.UI.TEXT_ALIGNMENT_LEFT,
            color : colorText,
            font : Config.biginputFont,
            height : Ti.UI.SIZE,
            width : Ti.UI.SIZE,
            ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
            wordWrap : false,
            touchEnabled : false
        }));

        if (qtyText != false && qtyText[1]) {
            
            var myText= qtyText[1];
            
            if(qtyText[2]){
                myText= qtyText[1]+':'+qtyText[2]; 
            }
            
            textView.add(Ti.UI.createLabel({
                right : '10dp',
                text : myText,
                textAlign : Ti.UI.TEXT_ALIGNMENT_LEFT,
                color : Config.lightBlue,
                font : Config.biginputFont,
                height : Ti.UI.SIZE,
                width : Ti.UI.SIZE,
                ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
                wordWrap : false,
                touchEnabled : false
            }));
        }

        var imageView = Ti.UI.createView({
            backgroundColor : Config.fs_colorLight,
            height : '50dp',
            width : '50dp',
            left : '10dp',
            borderRadius : '25dp',
            touchEnabled : false
        });

        imageView.add(Ti.UI.createImageView({
            image : url,
            height : '30dp',
            width : '30dp',
            touchEnabled : false
        }));

        container.add(action);
        container.add(textView);
        container.add(imageView);

        return container;
    }

    function updateProgress() {

        done += 0.02;

        var img_path = '';

        if (done < 1) {
            img_path = '';
        }

        if (done >= 1) {
            img_path = Config.images+ '192_ticket.png';
            done = 1;
            clearInterval(progressInterval);
            progressInterval = null;

            setTimeout(function(e) {
                refresh();
                overview.animate_hide();
            }, 500);

        }

        progress.update(done, img_path, Config.green);
    }

    function isOnTime() {

        var now = moment();
        var nowString = moment.unix(now).toString();
        var data = task.ontime_conditions;

        var tolOK = true;
        var ranOK = true;

        if (data == null || data == undefined) {
            return true;
        }

        if (data.tolerance == null || Object.keys(data.tolerance).length == 0) {

            tolOK = true;

        } else {

            for (var index in data.tolerance) {
                tolOK = tolOK && now.isBetween(moment(data.tolerance[index].min), moment(data.tolerance[index].max));
            }
        }

        if (data.ranges == null || Object.keys(data.ranges).length == 0) {

            ranOK = true;

        } else {

            ranOK = false;

            for (var index in data.ranges) {
                ranOK = ranOK || now.isBetween(moment(data.ranges[index].min), moment(data.ranges[index].max));
            }

        }

        if (tolOK && ranOK) {
            return true;
        } else {
            return false;
        }
    }

    function goCheckin() {

        // FORM
        var last_loc = db.selectKEYVAL('last_loc');
        if (last_loc == '') {
            last_loc = null;
        } else {
            last_loc = JSON.parse(last_loc);
        }

        var formValues = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, checkin_option);
        formValues.audit.first_validation_date = moment.utc().valueOf();
        formValues.audit.first_validation_lat = (last_loc != null) ? last_loc.lat : '';
        formValues.audit.first_validation_lon = (last_loc != null) ? last_loc.lon : '';
        formValues.audit.first_validation_accu = (last_loc != null) ? last_loc.acc : '';
        formValues.audit.task = _taskId;
        formValues.audit.form = checkin_form_id;
        db.updateFORMS(formValues);

        if (isOnTime()) {

            done = 0;
            progress.update(done, '', Config.green);

            overview.animate_show();

            progressTimeout = setTimeout(function(e) {

                progressInterval = setInterval(updateProgress, 50);
                progressTimeout = null;

            }, 500);

            formValues.audit.filled_date = moment.utc().valueOf();
            formValues.audit.filled_lat = (last_loc != null) ? last_loc.lat : '';
            formValues.audit.filled_lon = (last_loc != null) ? last_loc.lon : '';
            formValues.audit.filled_accu = (last_loc != null) ? last_loc.acc : '';

            formValues.values = [{
                "1" : 1
            }];

            db.sendForm(formValues);

        } else {

            var Window = require('/ui/PrincipalWindow');
            new Window(refresh, _routeId, _taskId, checkin_form);

        }
    }

    function goOffstart() {

        var last_loc = db.selectKEYVAL('last_loc');
        if (last_loc == '') {
            last_loc = null;
        } else {
            last_loc = JSON.parse(last_loc);
        }

        var formValues = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, offstart_option);
        formValues.audit.first_validation_date = moment.utc().valueOf();
        formValues.audit.first_validation_lat = (last_loc != null) ? last_loc.lat : '';
        formValues.audit.first_validation_lon = (last_loc != null) ? last_loc.lon : '';
        formValues.audit.first_validation_accu = (last_loc != null) ? last_loc.acc : '';
        formValues.audit.task = _taskId;
        formValues.audit.form = offstart_form_id;
        db.updateFORMS(formValues);

        var Window = require('/ui/PrincipalWindow');
        new Window(refresh, _routeId, _taskId, offstart_form);
    }

    function goDeliver() {

        var last_loc = db.selectKEYVAL('last_loc');
        if (last_loc == '') {
            last_loc = null;
        } else {
            last_loc = JSON.parse(last_loc);
        }

        var formValues = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, deliver_option);
        formValues.audit.first_validation_date = moment.utc().valueOf();
        formValues.audit.first_validation_lat = (last_loc != null) ? last_loc.lat : '';
        formValues.audit.first_validation_lon = (last_loc != null) ? last_loc.lon : '';
        formValues.audit.first_validation_accu = (last_loc != null) ? last_loc.acc : '';
        formValues.audit.task = _taskId;
        formValues.audit.form = deliver_form_id;
        db.updateFORMS(formValues);

        var Window = require('/ui/PrincipalWindow');
        new Window(refresh, _routeId, _taskId, deliver_form);
    }

    function goManifest() {

        work.animate_show();

        var checkResult = db.checkFORMSbyVALUES(_taskId, _routeId, task.priority, formsToCheck);

        if (checkResult.length > 0) {

            xhr.form_users(gotForms, _taskId);

        } else {

            var last_loc = db.selectKEYVAL('last_loc');
            if (last_loc == '') {
                last_loc = null;
            } else {
                last_loc = JSON.parse(last_loc);
            }

            var formValues = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, manifest_option);
            formValues.audit.first_validation_date = moment.utc().valueOf();
            formValues.audit.first_validation_lat = (last_loc != null) ? last_loc.lat : '';
            formValues.audit.first_validation_lon = (last_loc != null) ? last_loc.lon : '';
            formValues.audit.first_validation_accu = (last_loc != null) ? last_loc.acc : '';
            formValues.audit.task = _taskId;
            formValues.audit.form = manifest_form_id;
            db.updateFORMS(formValues);

            // Timeouts para no pegar la vista mientras trabaja pintando la vista... alcanzamos a hacer visible el work.
            setTimeout(function(e) {

                var Window = require('/ui/PrincipalWindow');
                new Window(refresh, _routeId, _taskId, manifest_form);

                setTimeout(function(ev) {

                    work.animate_hide();

                }, 200);

            }, 200);
        }
    }

    function goRejection() {

        var rejectionAlert = Ti.UI.createAlertDialog({
            cancel : 1,
            buttonNames : ['Confirmar', 'Cancelar'],
            message : '¿Desea rechazar esta entrega?',
            title : 'Atención'
        });

        rejectionAlert.addEventListener('click', function(e) {
            if (e.index == 0) {
                var last_loc = db.selectKEYVAL('last_loc');
                if (last_loc == '') {
                    last_loc = null;
                } else {
                    last_loc = JSON.parse(last_loc);
                }

                var formValues = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, rejection_option);
                formValues.audit.first_validation_date = moment.utc().valueOf();
                formValues.audit.first_validation_lat = (last_loc != null) ? last_loc.lat : '';
                formValues.audit.first_validation_lon = (last_loc != null) ? last_loc.lon : '';
                formValues.audit.first_validation_accu = (last_loc != null) ? last_loc.acc : '';
                formValues.audit.task = _taskId;
                formValues.audit.form = rejection_form_id;
                db.updateFORMS(formValues);

                var Window = require('/ui/PrincipalWindow');
                new Window(refresh, _routeId, _taskId, rejection_form);
            }
        });

        rejectionAlert.show();
    }

    function goList() {

        if (task.orders != null && task.orders.length > 0) {
            var title = 'jajajja ' + task.stop;
            var subtitle = 'Todos los productos';
            var Window = require('/ui/common/ProductList');
            var category = null;

            new Window(refresh, title, subtitle, task.orders, selectedInvoiceNumber, category);
            selectedInvoiceNumber = null;
            selectedInvoiceKey = null;
        } else {
            var dialog = Ti.UI.createAlertDialog({

                title : 'Atención',
                message : 'No hay datos de productos.',
                ok : 'CERRAR'
            });
            dialog.show();
        }
    }

    function goMap() {

        if (task.lat && task.lat != 0 && task.lat != '') {
            var Window = require('/ui/common/CustomersMap');
            new Window(refresh, _taskId);
        } else {
            var dialog = Ti.UI.createAlertDialog({

                title : 'Atención',
                message : 'Cliente sin datos de ubicación.',
                ok : 'CERRAR'
            });

            dialog.show();
        }
    }

    function goWaze() {

        var myWaze = require('/mods/waze');

        if (task.lat && task.lat != 0 && task.lat != '') {

            myWaze.NavigateTo(task.lat, task.lon);
        } else {
            Ti.UI.createAlertDialog({
                title : 'Atención',
                message : 'Cliente sin datos de ubicación.',
                ok : 'CERRAR'
            }).show();
        }
    }

    function goInvoices() {

        if (findInvoices()) {
            selectedInvoiceKey = null;
            selectedInvoiceNumber = null;
            var invoiceDialog = Ti.UI.createOptionDialog({
                //code : e.source.code,
                title : 'Seleccione factura',
                options : invoicesNumber
            });

            invoiceDialog.addEventListener('click', function(evt) {

                if (evt.button != true && evt.index >= 0) {

                    selectedInvoiceKey = evt.index;
                    selectedInvoiceNumber = invoicesNumber[selectedInvoiceKey];
                    goList();
                }
            });

            invoiceDialog.show();

        } else {
            var dialog = Ti.UI.createAlertDialog({
                title : 'Atención',
                message : 'No hay información disponible.',
                ok : 'CERRAR'
            });
            dialog.show();
        }
    }

    function findInvoices() {

        if (task.orders != null && task.orders != '') {
            invoices = [];
            invoicesNumber = [];
            invoicesIndex = [];
            var orders = task.orders;

            for (var i = 0; i < orders.length; i++) {
                var currentInvoice = orders[i].invoice_number.split(', ');

                for (var x = 0; x < currentInvoice.length; x++) {
                    if (currentInvoice[x] != '' && invoicesNumber.indexOf(currentInvoice[x]) == -1) {
                        invoicesNumber.push(currentInvoice[x]);
                        invoicesIndex.push(x);
                    }
                }
            }

            return true;
        } else {
            return false;
        }
    }

    function gotForms(result) {

        if (result == false) {

            work.animate_hide();

            var dialog = Ti.UI.createAlertDialog({
                title : 'Revise su conexión a Internet',
                message : 'No hemos podido comunicarnos con el servidor, por favor compruebe que está conectado a internet.',
                ok : 'OK'
            });
            dialog.show();

        } else {

            switch(result.status.code) {

            case '200':
                var checkResult = db.checkFORMSbyVALUES(_taskId, _routeId, task.priority, formsToCheck);
                var _forms = result.response.data;
                for (var f in _forms) {
                    var _form = _forms[f];
                    if (checkResult.indexOf(_form.identifier) != -1) {
                        var formValues = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, _form.identifier);
                        formValues.sent = 1;
                        formValues.audit.user_sent_date = _form.user_sent_date;
                        formValues.audit.first_validation_date = _form.first_validation_date;
                        formValues.values = _form.values;
                        db.updateFORMS(formValues);
                    }
                }
                goManifest();
                break;

            default:

                work.animate_hide();

                var dialog = Ti.UI.createAlertDialog({
                    title : 'Error inesperado',
                    message : 'Por favor inténtelo nuevamente en unos minutos o contáctese con el administrador del sistema.',
                    ok : 'CERRAR'
                });
                dialog.show();
                break;

            }
        }
    }

    function goBack() {
        if (progressTimeout) {
            clearTimeout(progressTimeout);
            progressTimeout = null;
        }
        if (progressInterval) {
            clearInterval(progressInterval);
            progressInterval = null;
        }
        _refresh();
        self.close();
    }


    self.addEventListener('android:back', function(e) {
        e.cancelBubble = true;
        goBack();
    });

    construct();

    self.open();
}

module.exports = CustomerDetail;
