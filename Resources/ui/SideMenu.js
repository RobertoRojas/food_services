var Config = require('/libs/Config');
var summon = require('/mods/summon');

var options = [{
	id : 'travelAlert',
	url : Config.images+ 'accent_alert.png',
	text : 'ALERTA DE VIAJE'
}];

var contactInfo = false;
var app_version = Ti.App.Properties.getObject('app_version_2', false);

if (app_version) {
	if (app_version['contact_info']) {

		if (app_version['contact_info'] != '' && app_version['contact_info'] != null) {
			contactInfo = true;
		}
	}
}

if (contactInfo) {

	var contactOption = {
		id : 'contact',
		url : Config.images+ 'blue_contact.png',
		text : 'CONTACTO'
	};

	options.push({
		id : 'separator'
	});

	options.push(contactOption);
}

var rightButton;

function SideMenu() {

	var me = Ti.App.Properties.getObject('me', new Object);

	var content = Ti.UI.createView({
		height : Ti.UI.FILL,
		width : Ti.UI.FILL,
		layout : 'vertical'
	});

	var self = summon.newwindow(false, false, Config.images+ 'color_bg.png', true);
	var actionbar = summon.transparentactionbar('');

	content.add(actionbar);

	self.add(content);

	rightButton = summon.transparentbarButton(closeMenu, Config.images+ 'ic_menu_w.png');
	actionbar.add_right(rightButton);

	var topView;
	var mainView;
	var subtitle;
	var scroll;
	var botView;

	function construct() {

		// TOPVIEW

		topView = Ti.UI.createView({
			height : '100dp',
			width : Ti.UI.FILL,
			top : '0dp',
			left : '24dp',
			right : '24dp',
			touchEnabled : false
		});

		var profileView = Ti.UI.createView({
			height : Ti.UI.SIZE,
			width : Ti.UI.SIZE,
			top : '12dp'
		});

		var picView = Ti.UI.createView({
			borderRadius : '40dp',
			height : '80dp',
			width : '80dp',
			left : '0dp'
		});

		var picTaken = Ti.UI.createImageView({
			image : Config.getPic(),
			borderRadius : '40dp',
			height : '80dp',
			width : '80dp'
		});

		var picFrame = Ti.UI.createView({
			borderRadius : '40dp',
			borderWidth : '2dp',
			borderColor : Config.inputBorderColor,
			height : '80dp',
			width : '80dp'
		});

		picView.add(picTaken);
		picView.add(picFrame);

		var profileTextView = Ti.UI.createView({
			height : Ti.UI.SIZE,
			width : Ti.UI.SIZE,
			left : '100dp',
			layout : 'vertical'
		});

		var fullnameLabel = Ti.UI.createLabel({
			text : me.name + ' ' + me.last_name,
			font : Config.subtitleFont,
			color : Config.titleTextColor,
			height : Ti.UI.SIZE,
			width : Ti.UI.SIZE,
			left : '0dp',
			touchEnabled : false
		});

		var posLabel = Ti.UI.createLabel({
			text : 'Conductor',
			font : Config.subtitleFont,
			color : Config.titleTextColor,
			height : Ti.UI.SIZE,
			width : Ti.UI.SIZE,
			left : '0dp',
			touchEnabled : false
		});

		profileTextView.add(fullnameLabel);
		profileTextView.add(posLabel);

		profileView.add(picView);
		profileView.add(profileTextView);

		topView.add(profileView);

		// MAINVIEW

		mainView = Ti.UI.createView({
			height : Ti.UI.FILL,
			width : Ti.UI.FILL,
			top : '0dp',
			bottom : '64dp',
			layout : 'vertical',
			touchEnabled : false
		});

		subtitle = summon.tabtitle('', 'MENÚ');
		subtitle.top = '24dp';
		subtitle.bottom = '0dp';

		scroll = Ti.UI.createScrollView({
			backgroundColor : Config.backgroundColor,
			showVerticalScrollIndicator : true,
			height : Ti.UI.FILL,
			width : Ti.UI.FILL,
			top : '0dp',
			bottom : '128dp',
			scrollType : 'vertical',
			layout : 'vertical'
		});

		for (var index in options) {

			if (options[index].id == 'separator') {

				var _opt = Ti.UI.createView({
					backgroundColor : Config.optionsSeparatorColor,
					height : '1dp',
					width : Ti.UI.FILL
				});
				scroll.add(_opt);

			} else {

				var _opt = summon.menuoption(options[index].id, options[index].url, options[index].text);
				scroll.add(_opt);

			}
		}

		mainView.add(subtitle);
		mainView.add(scroll);

		// BOTVIEW

		var botView = Ti.UI.createView({
			backgroundColor : Config.actionbarBackgroundColor,
			height : '64dp',
			width : Ti.UI.FILL,
			bottom : '0dp',
			touchEnabled : false
		});

		var exitBox = Ti.UI.createView({
			borderRadius : Config.buttonBorderRadius,
			height : Config.buttonHeight,
			width : '180dp',
			right : '16dp',
			touchEnabled : false
		});

		var exitAction = Ti.UI.createView({
			backgroundColor : Config.actionbarBackgroundColor,
			height : Ti.UI.FILL,
			width : Ti.UI.FILL,
			touchFeedback : Config.touchFeedback,
			touchFeedbackColor : Config.barRippleColor,
			touchEnabled : true
		});

		exitAction.addEventListener('click', exit);

		var exitLayout = Ti.UI.createView({
			height : Ti.UI.SIZE,
			width : Ti.UI.SIZE,
			layout : 'horizontal',
			touchEnabled : false
		});

		var exitIcon = Ti.UI.createImageView({
			image : Config.images + 'white_close.png',
			height : '16dp',
			width : '16dp',
			touchEnabled : false
		});

		var exitLabel = Ti.UI.createLabel({
			text : '  CERRAR SESIÓN',
			textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
			font : Config.buttonFont,
			color : Config.buttonTextColor,
			height : Ti.UI.SIZE,
			width : Ti.UI.SIZE,
			touchEnabled : false
		});

		exitLayout.add(exitIcon);
		exitLayout.add(exitLabel);

		exitBox.add(exitAction);
		exitBox.add(exitLayout);

		botView.add(exitBox);

		content.add(topView);
		content.add(mainView);
		self.add(botView);

	}

	function exit() {
		Config.drawer.exitApp.show();
	}

	function closeMenu() {
		if (Config.drawer.isAnyWindowOpen()) {
			if (Config.drawer.isLeftWindowOpen()) {
				Config.drawer.toggleLeftWindow();
			}
			if (Config.drawer.isRightWindowOpen()) {
				Config.drawer.toggleRightWindow();
			}
		}
	}

	construct();

	return self;
}

module.exports = SideMenu;
