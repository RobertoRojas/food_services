var Config = require('/libs/Config');
var summon = require('/mods/summon');
var xhr = require('/mods/xhr');
var db = require('/mods/db');

function Login() {

	var appVersion = Ti.App.Properties.getObject('app_version_2', false);

	if (Config.mode) {
		Ti.API.info('APP VERSION: ' + JSON.stringify(appVersion));
	}

	var inputCode;
	var inputNumber;
	var buttonAction;

	var phoneNumber;

	var userInfo;

	var content = Ti.UI.createView({
		height : Ti.UI.FILL,
		width : Ti.UI.FILL,
		layout : 'vertical'
	});

	var self = summon.newwindow(true, true, Config.colorBg);
	var work = summon.fullwork();
	var subtitle;

	self.add(content);
	self.add(work);

	var horizontalScroll = Ti.UI.createScrollableView({
		cacheSize : 16,
		scrollingEnabled : false,
		width : '100%',
		height : '100%'
	});

	function construct() {

		// VIEW 1

		function flagsContainer() {

			var view1 = Ti.UI.createView({
				height : Ti.UI.FILL,
				width : Ti.UI.FILL,
				top : '0dp',
				bottom : '0dp'
			});

			var containerLeft = Ti.UI.createView({
				left : '0dp',
				height : Ti.UI.FILL,
				width : Config.displayWt,
				backgroundColor : 'green',
				top : '0dp',
				bottom : '0dp'
			});

			containerLeft.add(Ti.UI.createImageView({
				left : 0,
				image : Config.colorBg,
				height : Ti.UI.FILL,
				top : '0dp',
				touchEnabled : false
			}));

			var title1 = summon.transparenttitle('Seleccionar', 'PAIS', Config.images + 'white_pin.png', 'white', '150dp');
			containerLeft.add(title1);

			var containerRight = Ti.UI.createView({
				left : 150,
				height : Ti.UI.FILL,
				width : Config.displayWt,
				backgroundColor : 'green',
				top : '0dp',
				bottom : '0dp'
			});

			containerRight.add(Ti.UI.createImageView({
				left : 0,
				image : Config.whiteBg,
				height : Ti.UI.FILL,
				top : '0dp',
				touchEnabled : false
			}));

			var viewFlags = Ti.UI.createView({
				left : 0,
				width : Config.displayWt,
				top : '100dp',
				bottom : '100dp',
				touchEnabled : false
			});

			var viewScroll = Ti.UI.createView({
				left : '10dp',
				right : '10dp',
				width : '190dp',
				touchEnabled : false
			});

			viewFlags.add(viewScroll);
			containerRight.add(viewFlags);

			var scroll1 = Ti.UI.createScrollView({
				showVerticalScrollIndicator : true,
				width : Ti.UI.FILL,
				top : '0dp',
				bottom : '0dp',
				scrollType : 'vertical',
				layout : 'vertical'
			});

			banderitas(scroll1);
			viewScroll.add(scroll1);

			view1.add(containerLeft);
			view1.add(containerRight);
			return view1;
		}

		function loginContainer() {

			// var scroll = Ti.UI.createScrollView({
			// backgroundColor : 'cyan',
			// showVerticalScrollIndicator : true,
			// width : Ti.UI.FILL,
			// top : '0dp',
			// bottom : '0dp',
			// scrollType : 'vertical',
			// layout : 'vertical'
			// });

			var loginView = Ti.UI.createView({
				top : '0dp',
				bottom : '0dp',
				width : Ti.UI.FILL,
				top : '0dp',
				touchEnabled : false
			});
			
			loginView.add(Ti.UI.createImageView({
				top : 0,
				left : 0,
				image : Config.whiteBg,
				height : Ti.UI.FILL,
				touchEnabled : false
			}));
			
			loginView.add(Ti.UI.createImageView({
				top: '50dp',
				image : Config.images + 'logo.jpg',
				height : '100dp',
				width : '100dp'
			}));

				var log1View = Ti.UI.createView({
					top : '179dp',
					height : '150dp',
					width : '150dp',
					touchEnabled : false
				});
			
			log1View.add(Ti.UI.createImageView({
				top: '0dp',
				image : Config.images + 'circle_fs.png',
				height : '150dp',
				width : '150dp'
			}));
			
			log1View.add(Ti.UI.createImageView({
				top : '30dp',
				height : '32dp',
				width : '32dp',
				image : Config.images+ 'white_pin.png',
				touchEnabled : false
			}));
			
			loginView.add(log1View);
			
			var topView = Ti.UI.createView({
				backgroundColor: 'green',
				top : '250dp',
				height : Ti.UI.FILL,
				height : Config.displayHt,
				width : Ti.UI.FILL,
				touchEnabled : false
			});

			topView.add(Ti.UI.createImageView({
				top : 0,
				left : 0,
				width: Ti.UI.FILL,
				image : Config.colorBg,
				touchEnabled : false
			}));
			
			var log2View = Ti.UI.createView({
				top : '-75dp',
				height : '150dp',
				width : '150dp',
				touchEnabled : false
			});
			
			
			log2View.add(Ti.UI.createImageView({
				top: '0dp',
				image : Config.images + 'circle_fs.png',
				height : '150dp',
				width : '150dp'
			}));
			
			var log2Title = summon.transparentTitleLine('LOGIN', 'white', '150dp', '55dp');
			log2View.add(log2Title);
			
			var inputsview = Ti.UI.createView({
				top : '90dp',
				height : '275dp',
				left: '20dp',
				right: '20dp',
				layout: 'vertical',
				touchEnabled : false
			});
			
			topView.add(log2View);
			topView.add(inputsview);

			loginView.add(topView);

			subtitle = summon.icontitle(Config.images+ '192_login.png', 'LOGIN', goBack);
			subtitle.top = '24dp';
			subtitle.bottom = '0dp';

			var botBox = Ti.UI.createView({
				height : Ti.UI.FILL,
				width : Ti.UI.FILL,
				top : '0dp',
				layout : 'vertical',
				touchEnabled : false
			});

			var phoneLabel = Ti.UI.createLabel({
				text : 'INGRESAR TELÉFONO',
				textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
				font : Config.subtitleFont,
				color : Config.subtitleTextColor,
				height : Ti.UI.SIZE,
				width : Ti.UI.SIZE,
				top : '36dp'
			});

			var phoneView = Ti.UI.createView({
				height : Ti.UI.SIZE,
				width : Ti.UI.FILL,
				top : '24dp',
				left : '40dp',
				right : '40dp',
				layout : 'horizontal'
			});

			inputCode = Ti.UI.createTextField({
				autocorrect : false,
				keyboardType : Ti.UI.KEYBOARD_TYPE_PHONE_PAD,
				textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
				font : Config.biginputFont,
				color : Config.inputTextColor,
				height : Config.biginputHeight,
				width : '72dp',
				touchEnabled : false,
				editable : false,
				hintText : '+',
				hintTextColor : Config.hintdarkgray,
				value : '+569'
			});

			var phoneSpacer = Ti.UI.createView({
				height : '1dp',
				width : '16dp'
			});

			inputNumber = Ti.UI.createTextField({
				autocorrect : false,
				value : '',
				keyboardType : Ti.UI.KEYBOARD_TYPE_PHONE_PAD,
				textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
				font : Config.biginputFont,
				color : Config.inputTextColor,
				height : Config.biginputHeight,
				width : Ti.UI.FILL,
				touchEnabled : true,
			});

			inputNumber.addEventListener('return', returnkey);

			phoneView.add(inputCode);
			phoneView.add(phoneSpacer);
			phoneView.add(inputNumber);

			var buttonBox = Ti.UI.createView({
				//borderRadius : Config.buttonBorderRadius,
				height : Config.buttonHeight,
				width : Ti.UI.FILL,
				top : '48dp',
				left : '80dp',
				right : '80dp',
				bottom : '16dp',
				touchEnabled : false
			});

			buttonAction = Ti.UI.createView({
				backgroundColor : Config.buttonBackgroundColor,
				//backgroundColor : Config.buttonBackgroundColor,
				height : Ti.UI.FILL,
				width : Ti.UI.FILL,
				touchFeedback : Config.touchFeedback,
				touchFeedbackColor : Config.buttonRippleColor,
				touchEnabled : true
			});

			var buttonLabel = Ti.UI.createLabel({
				text : 'INGRESAR',
				textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
				font : Config.buttonFont,
				color : Config.buttonTextColor,
				height : Ti.UI.SIZE,
				width : Ti.UI.SIZE,
				touchEnabled : false
			});

			buttonAction.addEventListener('click', register);
			
			buttonBox.add(buttonAction);
			buttonBox.add(buttonLabel);
			
			botBox.add(phoneLabel);
			botBox.add(phoneView);
			botBox.add(buttonBox);

			inputsview.add(phoneLabel);
			inputsview.add(phoneView);
			inputsview.add(buttonBox);
			//
			// botView.add(subtitle);
			inputsview.add(botBox);

			return loginView;
		}

		var flagstPanel = flagsContainer();
		var loginPanel = loginContainer();

		horizontalScroll.setViews([flagstPanel, loginPanel]);

		content.add(horizontalScroll);

	}

	function banderitas(_view) {

		var box;
		var underscore;

		for (c in appVersion.countries) {

			box = Ti.UI.createView({
				height : '64dp',
				width : '200dp',
				top : '0dp',
				bottom : '16dp'
			});

			var action = Ti.UI.createView({
				country : appVersion.countries[c],
				backgroundColor : 'transparent',
				height : Ti.UI.FILL,
				width : Ti.UI.FILL,
				touchFeedback : Config.barTouchFeedback,
				touchFeedbackColor : Config.barRippleColor,
				touchEnabled : true
			});

			action.addEventListener('click', chooseFlag);

			var icon = Ti.UI.createImageView({
				image : appVersion.countries[c].icon_loc,
				height : '64dp',
				width : '64dp',
				left : '0dp',
				touchEnabled : false
			});

			var name = Ti.UI.createLabel({
				text : appVersion.countries[c].name.toUpperCase(),
				font : Config.subtitleFont,
				color : Config.containerTextColor,
				height : Ti.UI.SIZE,
				width : Ti.UI.SIZE,
				left : '74dp',
				ellipsize : Ti.UI.TEXT_ELLIPSIZE_TRUNCATE_MARQUEE,
				wordWrap : false,
				touchEnabled : false
			});

			underscore = Ti.UI.createView({
				backgroundColor : Config.hintgray,
				height : Config.underscoreHeight,
				width : Ti.UI.FILL,
				bottom : '0dp',
				touchEnabled : false
			});

			box.add(action);
			box.add(icon);
			box.add(name);
			box.add(underscore);

			_view.add(box);

		}

		box.remove(underscore);
	}

	function chooseFlag(e) {

		subtitle.setIcon(e.source.country.icon_loc);
		inputCode.setValue(e.source.country.prefix);
		horizontalScroll.moveNext();
	}

	function returnkey(e) {
		if (e.type == 'return') {
			register();
		}
	}

	function register() {

		inputCode.blur();
		inputNumber.blur();

		if (inputCode.value.length > 0 && inputNumber.value.length > 0) {

			work.animate_show();

			phoneNumber = inputCode.value + inputNumber.value;
			phoneNumber = phoneNumber.replace(/ /g, '');

			var params = {
				phone : phoneNumber,
				hash : Ti.Utils.md5HexDigest("ab" + phoneNumber + "69")
			};

			Ti.API.info('REGISTER PARAMS: ' + JSON.stringify(params));
			xhr.register_device(registerResponse, params);

		}

	}

	function registerResponse(result) {

		if (Config.mode) {
			//{"status":{"code":"200","message":"Success","str":"OK"},"response":{"count":0,"data":[]}}
			Ti.API.info('REGISTER RESPONSE: ' + JSON.stringify(result));
		}

		if (result == false) {

			work.animate_hide();

			var dialog = Ti.UI.createAlertDialog({
				title : 'Revise su conexión a Internet',
				message : 'No hemos podido comunicarnos con el servidor, por favor compruebe que está conectado a internet.',
				ok : 'OK'
			});
			dialog.show();

		} else {

			switch(result.status.code) {

			case '200':

				login();
				break;

			case '500':

				work.animate_hide();

				var dialog = Ti.UI.createAlertDialog({
					title : 'Error interno',
					message : 'Por favor inténtelo nuevamente en unos minutos o contáctese con el administrador del sistema.',
					ok : 'CERRAR'
				});
				dialog.show();
				break;

			default:

				work.animate_hide();

				var dialog = Ti.UI.createAlertDialog({
					title : 'Error inesperado',
					message : 'Dispositivo no autorizado, contacte al administrador del sistema, código R01.',
					ok : 'CERRAR'
				});
				dialog.show();
				break;

			}
		}
	}

	function login() {

		var params = {
			phone : phoneNumber,
			hash : Ti.Utils.md5HexDigest("ab" + phoneNumber + "69")
		};

		Ti.API.info('LOGIN PARAMS: ' + JSON.stringify(params));
		xhr.login(loginResponse, params);
	}

	function loginResponse(result) {

		if (Config.mode) {
			//{"status":{"code":"200","message":"Success","str":"OK"},"response":{"count":0,"data":[]}}
			Ti.API.info('LOGIN RESPONSE: ' + JSON.stringify(result));
		}

		if (result == false) {

			work.animate_hide();

			var dialog = Ti.UI.createAlertDialog({
				title : 'Revise su conexión a Internet',
				message : 'No hemos podido comunicarnos con el servidor, por favor compruebe que está conectado a internet.',
				ok : 'OK'
			});
			dialog.show();

		} else {

			switch(result.status.code) {

			case '200':

				userInfo = result.response.data;
				userInfo.phone = phoneNumber;
				db.updateKEYVAL({
					ubqti_api_token : userInfo.session_token
				});

				xhr.formMaster(tryGetFormMasters);
				break;

			case '500':

				work.animate_hide();

				var dialog = Ti.UI.createAlertDialog({
					title : 'Error interno',
					message : 'Por favor inténtelo nuevamente en unos minutos o contáctese con el administrador del sistema.',
					ok : 'CERRAR'
				});
				dialog.show();
				break;

			default:

				work.animate_hide();

				var dialog = Ti.UI.createAlertDialog({
					title : 'Error inesperado',
					message : 'Su dispositivo aún no ha sido autorizado, favor contactar al administrador del sistema, código L01.',
					ok : 'CERRAR'
				});
				dialog.show();
				break;

			}
		}
	}

	function tryGetFormMasters(result) {

		if (result == false) {

			work.animate_hide();

			Ti.App.Properties.setObject('me', null);
			db.updateKEYVAL({
				ubqti_api_token : ''
			});

			var dialog = Ti.UI.createAlertDialog({
				title : 'Revise su conexión a Internet',
				message : 'no hemos podido comunicarnos con el servidor, por favor compruebe que está conectado a internet.',
				ok : 'OK'
			});
			dialog.show();

		} else {

			switch(result.status.code) {

			case '200':

				db.dropFORMMASTERS();
				for (index in result.response.data) {
					var formData = result.response.data[index];
					db.insertFORMMASTERS(formData);
				}

				xhr.resource(tryGetResources);
				break;

			default:

				work.animate_hide();

				Ti.App.Properties.setObject('me', null);
				db.updateKEYVAL({
					ubqti_api_token : ''
				});

				var dialog = Ti.UI.createAlertDialog({
					title : 'Error inesperado',
					message : 'por favor inténtelo nuevamente en unos minutos o contáctese con el administrador del sistema.',
					ok : 'CERRAR'
				});
				dialog.show();
				break;

			}
		}
	}

	function tryGetResources(result) {

		if (result == false) {

			work.animate_hide();

			Ti.App.Properties.setObject('me', null);
			db.updateKEYVAL({
				ubqti_api_token : ''
			});

			var dialog = Ti.UI.createAlertDialog({
				title : 'Revise su conexión a Internet',
				message : 'no hemos podido comunicarnos con el servidor, por favor compruebe que está conectado a internet.',
				ok : 'OK'
			});
			dialog.show();

		} else {

			switch(result.status.code) {

			case '200':

				var resources = result.response.data;
				Ti.App.Properties.setList('resources', resources);

				work.animate_hide();

				Ti.App.Properties.setObject('me', userInfo);

				db.updateKEYVAL({
					ubqti_api_token : Ti.App.Properties.getObject('me', new Object).session_token
				});

				goMenu();
				break;

			default:

				work.animate_hide();

				Ti.App.Properties.setObject('me', null);
				db.updateKEYVAL({
					ubqti_api_token : ''
				});

				var dialog = Ti.UI.createAlertDialog({
					title : 'Error inesperado',
					message : 'por favor inténtelo nuevamente en unos minutos o contáctese con el administrador del sistema.',
					ok : 'CERRAR'
				});
				dialog.show();
				break;

			}
		}
	}

	function goBack() {
		if (horizontalScroll.getCurrentPage() == 0) {
			self.close();
		} else {
			horizontalScroll.movePrevious();
		}
	}

	function goMenu() {
		var Window = require('/ui/Menu');
		new Window();
		self.close();
	}

	if (Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, Config.profile_pic).exists()) {
		Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, Config.profile_pic).deleteFile();
	}

	self.addEventListener('android:back', function(e) {
		e.cancelBubble = true;
		goBack();
	});

	construct();

	self.open();

}

module.exports = Login;
