var Config = require('/libs/Config');
var summon = require('/mods/summon');
// var xhr = require('/mods/xhr');
// var db = require('/mods/db');
var moment = require('/libs/moment');
var pictures = require('/mods/gallery');

moment.locale(Ti.Locale.currentLanguage);

var leftButton;
var rightButton;
var contactButton;

function Window(_refresh, _routeId, _taskId, _formMaster) {

    Ti.API.info('Principal window form master: ' + JSON.stringify(_formMaster));
    var task = db.selectTASKSbyID(_taskId);
    var forms = _formMaster.form;
    var formValues = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, _formMaster.identifier);

    var content = Ti.UI.createView({
        height : Ti.UI.FILL,
        width : Ti.UI.FILL,
        layout : 'vertical'
    });

    //var self = summon.newwindow(false, false, Config.images+ 'bw_bg.png');
    var self = summon.newwindow(false, false, Config.colorBg);
    var actionbar = summon.actionbar(_formMaster.name);
    var work = summon.fullwork();

    content.add(actionbar);

    self.add(content);
    self.add(work);

    leftButton = summon.barButton(goBack, Config.images+ 'ic_navigate_before_w.png');
    actionbar.add_left(leftButton);

    contactButton = summon.barButton(openContact, Config.images+ '192_contactos.png');
    actionbar.add_right(contactButton);

    var mainView;
    var subtitle;
    //var scroll;
    var horizontalScroll;
    var gallery;
    var okAction;
    //Ti.API.error('object values: ' + JSON.stringify(objValues));
    var formsViews = [];

    function construct() {

        mainView = Ti.UI.createView({
            height : Ti.UI.FILL,
            width : Ti.UI.FILL,
            top : '0dp',
            layout : 'vertical',
            touchEnabled : false
        });

        horizontalScroll = Ti.UI.createScrollableView({
            cacheSize : 16,
            scrollingEnabled : false,
            width : '100%',
            height : '100%',
        });

        mainView.add(horizontalScroll);

        content.add(mainView);

        getViews();
        refresh();
    }

    function getViews() {

        // Encontrar total
        var currentindex = '1';
        var currentform = forms[currentindex];
        var total = 0;
        while (currentform != null) {

            var myView = fakeView(currentform.field_type, currentform, currentindex, currentform.action.type);

            if (myView) {
                total++;
            }

            if (currentform.action.type == 'field') {
                currentindex = currentform.action.value;
                currentform = forms[currentindex];
            } else {
                currentform = null;
            }
        }

        // Encontrar vistas
        currentindex = '1';
        currentform = forms[currentindex];
        var cont = 1;
        while (currentform != null) {

            var myView = selectedView(currentform.field_type, currentform, currentindex, currentform.action.type, cont + '/' + total);

            if (myView != false) {
                formsViews.push(myView);

                if (currentform.field_type != 'hidden' && currentform.field_type != 'pinInput') {
                    cont++;
                }
            }

            if (currentform.action.type == 'field') {
                currentindex = currentform.action.value;
                currentform = forms[currentindex];

            } else {

                currentform = null;

            }
        }

        horizontalScroll.views = formsViews;
    }

    function fakeView(formType, form, index, action) {

        // DETERMINAR SI SE CUENTA PARA EL TOTAL O NO

        switch(formType) {
        case 'hidden':
            return false;
            break;
        case 'pinInput':
            return false;
            break;
        case 'timedPickerList':
            if (_formMaster.identifier == 'manifest') {
                var _id1 = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, form.validation_fields[0].name.split('.')[0]).audit.first_validation_date;
                var _id2 = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, form.validation_fields[2].name.split('.')[0]).audit.first_validation_date;
                var _std = task[form.validation_fields[1].name];
                var _res = (_id2 - _id1) - (_std + form.delta_time);
                if (_res > 0 && _std > 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
            break;
        default:
            return true;
            break;
        }
    }

    function selectedView(formType, form, index, action, step) {

        var view = false;
        var defaultVal = null;
        var button = (action == 'field') ? nextForm() : sendForm();

        // PREPARAR FORMS

        if (_formMaster.identifier == 'manifest') {

            var validForms = ['checkin', 'offload_start', 'deliver'];

            switch(formType) {
            case 'pinInput':
                defaultVal = {
                    default_pin : null
                };
                button = removeViewButton();
                break;

            case 'timeInput':
                defaultVal = [];
                for (var f in form.fields) {
                    var _tempIdentifier = form.fields[f].value.split('.')[0];
                    if (validForms.indexOf(_tempIdentifier) != -1) {
                        defaultVal.push({
                            key : _tempIdentifier,
                            value : db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, _tempIdentifier).audit.first_validation_date
                        });
                    }
                }
                break;

            case 'timedPickerList':
                break;

            case 'inputTemperature':
                var _tempform = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, form.values_from.split('.')[0]);
                var _stage = form.values_from.split('.')[1];
                for (var _f in _tempform.values) {
                    if ( typeof (_tempform.values[_f][_stage]) != 'undefined' && _tempform.values[_f][_stage] != null) {
                        defaultVal = _tempform.values[_f][_stage];
                    }
                }
                break;

            case 'inputPackages':
                var _tempIdentifier = null;
                var _stage = null;
                for (var _i in form.options) {
                    if (form.options[_i].values_from) {
                        _tempIdentifier = form.options[_i].values_from.split('.')[0];
                        _stage = form.options[_i].values_from.split('.')[1];
                    }
                }
                var _tempform = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, _tempIdentifier);
                for (var _f in _tempform.values) {
                    if ( typeof (_tempform.values[_f][_stage]) != 'undefined' && _tempform.values[_f][_stage] != null) {
                        defaultVal = _tempform.values[_f][_stage];
                    }
                }
                break;

            case 'inputItemsWithSearch':
                var _tempform = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, form.values_from.split('.')[0]);
                var _stage = form.values_from.split('.')[1];
                for (var _f in _tempform.values) {
                    if ( typeof (_tempform.values[_f][_stage]) != 'undefined' && _tempform.values[_f][_stage] != null) {
                        defaultVal = _tempform.values[_f][_stage];
                    }
                }
                pictures.rename(_taskId, form.values_from.split('.')[0], form.values_from.split('.')[1], _formMaster.identifier, index);
                break;

            case 'binaryFlow':
                var _tempform = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, form.values_from.split('.')[0]);
                var _stage = form.values_from.split('.')[1];
                for (var _f in _tempform.values) {
                    if ( typeof (_tempform.values[_f][_stage]) != 'undefined' && _tempform.values[_f][_stage] != null) {
                        defaultVal = _tempform.values[_f][_stage];
                    }
                }
                break;

            case 'alphanumeric':
                break;
            }

        }

        formValues = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, _formMaster.identifier);

        var found = false;
        for (var f in formValues.values) {
            if ( typeof (formValues.values[f][index]) != 'undefined') {
                found = true;
            }
        }

        if (found == false) {

            var _newkey = {};
            _newkey[index] = (formType == 'hidden') ? 1 : defaultVal;
            formValues.values.push(_newkey);
            db.updateFORMS(formValues);

        }

        // VIEWS

        if (_formMaster.identifier == 'checkin') {

            switch(formType) {
            case 'hidden':
                break;
            case 'timedPickerList':
                view = arrival_timedPickerList(form, index, button, step);
                break;
            }

        } else if (_formMaster.identifier == 'offload_start') {

            switch(formType) {
            case 'inputTemperature':
                view = offloadStart_temperatureView(form, index, button, step);
                break;

            case 'alphanumeric':
                view = offloadStart_receptionView(form, index, button, step);
                break;
            }

        } else if (_formMaster.identifier == 'deliver') {

            switch(formType) {
            case 'inputPackages':
                view = delivery_inputPackage(form, index, button, step);
                break;
            case 'inputItemsWithSearch':
                view = delivery_anomaly(form, index, button, step);
                break;
            case 'binaryFlow':
                view = delivery_Butt(form, index, button, step);
                break;
            case 'evaluation':
                view = evaluation(form, index, button, step);
                break;
            case 'alphanumeric':
                view = comments(form, index, button, step);
                break;
            }

        } else if (_formMaster.identifier == 'manifest') {

            switch(formType) {
            case 'pinInput':
                view = manifest_pin(form, index, button, step);
                horizontalScroll.viewToRemove = view;
                break;

            case 'timeInput':
                view = manifest_schedules(form, index, button, step);
                break;

            case 'timedPickerList':
                var _id1 = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, form.validation_fields[0].name.split('.')[0]).audit.first_validation_date;
                var _id2 = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, form.validation_fields[2].name.split('.')[0]).audit.first_validation_date;
                var _std = task[form.validation_fields[1].name];
                var _res = (_id2 - _id1) - (_std + form.delta_time);
                if (_res > 0 && _std > 0) {
                    view = manifest_delay(form, index, button, step, _res);
                }
                break;

            case 'inputTemperature':
                view = manifest_temperatureView(form, index, button, step);
                break;

            case 'inputPackages':
                view = manifest_inputPackage(form, index, button, step);
                break;

            case 'inputItemsWithSearch':
                view = manifest_anomaly(form, index, button, step);
                break;

            case 'binaryFlow':
                view = manifest_Butt(form, index, button, step);
                break;

            case 'evaluation':
                view = evaluation(form, index, button, step);
                break;

            case 'alphanumeric':
                view = comments(form, index, button, step);
                break;
            }

        } else if (_formMaster.identifier == 'rejection') {

            switch(formType) {
            case 'pickerList':
                view = rejection(form, index, button, step);
                break;
            }
        }

        return view;

    }

    //CHECK IN

    function arrival_timedPickerList(form, index, button, step) {

        var Arrival = require('ui/checkin/Arrival');
        var myArrival = new Arrival(refresh, _routeId, _taskId, form, index, _formMaster.identifier, button, step);

        return myArrival;
    }

    //OFFLOAD START

    function offloadStart_temperatureView(form, index, button, step) {

        var Temperature = require('ui/offloadStart/Temperature');
        var myTemperature = new Temperature(refresh, _routeId, _taskId, form, index, _formMaster.identifier, button, step);

        return myTemperature;
    }

    function offloadStart_receptionView(form, index, button, step) {

        var Reception = require('ui/offloadStart/Reception');
        var myReception = new Reception(refresh, _routeId, _taskId, form, index, _formMaster.identifier, button, step);

        return myReception;
    }

    // DELIVERY

    function delivery_inputPackage(form, index, button, step) {

        var Containers = require('ui/delivery/Packages');
        var myContainers = new Containers(refresh, _routeId, _taskId, form, index, _formMaster.identifier, button, step);

        return myContainers;
    }

    function delivery_anomaly(form, index, button, step) {

        var Anomaly = require('ui/delivery/Anomaly');
        var myAnomaly = new Anomaly(refresh, _routeId, _taskId, form, index, _formMaster.identifier, button, step);

        return myAnomaly;
    }

    function delivery_Butt(form, index, button, step) {

        var Butt = require('ui/delivery/Butt');
        var myButt = new Butt(refresh, _routeId, _taskId, form, index, _formMaster.identifier, button, step);

        return myButt;
    }

    // REJECTION

    function rejection(form, index, button, step) {

        var Rejection = require('ui/rejection/Rejection');
        var myRejection = new Rejection(refresh, _routeId, _taskId, form, index, _formMaster.identifier, button, step);

        return myRejection;
    }

    // MANIFEST

    function manifest_pin(form, index, button, step) {

        var Pin = require('ui/manifest/Pin');
        var myPin = new Pin(refresh, _routeId, _taskId, form, index, _formMaster.identifier, button, step, work);

        return myPin;
    }

    function manifest_schedules(form, index, button, step) {

        var Schedules = require('ui/manifest/Schedules');
        var mySchedules = new Schedules(refresh, _routeId, _taskId, form, index, _formMaster.identifier, button, step);

        return mySchedules;
    }

    function manifest_delay(form, index, button, step, delay) {

        var Delay = require('ui/manifest/Delay');
        var myDelay = new Delay(refresh, _routeId, _taskId, form, index, _formMaster.identifier, button, step, delay);

        return myDelay;
    }

    function manifest_temperatureView(form, index, button, step) {

        var Temperature = require('ui/manifest/Temperature');
        var myTemperature = new Temperature(refresh, _routeId, _taskId, form, index, _formMaster.identifier, button, step);

        return myTemperature;
    }

    function manifest_inputPackage(form, index, button, step) {

        var Containers = require('ui/delivery/Packages');
        var myContainers = new Containers(refresh, _routeId, _taskId, form, index, _formMaster.identifier, button, step);

        return myContainers;
    }

    function manifest_anomaly(form, index, button, step) {

        var Anomaly = require('ui/delivery/Anomaly');
        var myAnomaly = new Anomaly(refresh, _routeId, _taskId, form, index, _formMaster.identifier, button, step);

        return myAnomaly;
    }

    function manifest_Butt(form, index, button, step) {

        var Butt = require('ui/delivery/Butt');
        var myButt = new Butt(refresh, _routeId, _taskId, form, index, _formMaster.identifier, button, step);

        return myButt;
    }

    // COMMON

    function comments(form, index, button, step) {

        var Comments = require('ui/common/Comments');
        var myComments = new Comments(refresh, _routeId, _taskId, form, index, _formMaster.identifier, button, step);

        return myComments;
    }

    function evaluation(form, index, button, step) {

        var Evaluation = require('ui/common/Evaluation');
        var myEvaluation = new Evaluation(refresh, _routeId, _taskId, form, index, _formMaster.identifier, button, step);

        return myEvaluation;
    }

    function nextForm() {

        var text = Config.nextText;

        var buttonBox = Ti.UI.createView({
            // borderRadius : Config.buttonBorderRadius,
            height : Config.buttonHeight,
            width : Ti.UI.FILL,
            top : '48dp',
            left : '80dp',
            right : '80dp',
            bottom : '16dp',
            touchEnabled : false
        });

        var okAction = Ti.UI.createView({
            backgroundColor : Config.neutral,
            height : Ti.UI.FILL,
            width : Ti.UI.FILL,
            touchFeedback : Config.touchFeedback,
            touchFeedbackColor : Config.buttonRippleColor,
            touchEnabled : false
        });

        var buttonLabel = Ti.UI.createLabel({
            text : text.toUpperCase(),
            textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
            font : Config.buttonFont,
            color : Config.buttonTextColor,
            height : Ti.UI.SIZE,
            width : Ti.UI.SIZE,
            touchEnabled : false
        });

        okAction.addEventListener('click', function() {
            goNext();
        });

        buttonBox.add(okAction);
        buttonBox.add(buttonLabel);

        buttonBox.action = okAction;

        return buttonBox;
    };

    function sendForm() {

        var text = Config.sendText;

        var buttonBox = Ti.UI.createView({
           // borderRadius : Config.buttonBorderRadius,
            height : Config.buttonHeight,
            width : Ti.UI.FILL,
            top : '48dp',
            left : '80dp',
            right : '80dp',
            bottom : '16dp',
            touchEnabled : false
        });

        var okAction = Ti.UI.createView({
            backgroundColor : Config.neutral,
            height : Ti.UI.FILL,
            width : Ti.UI.FILL,
            touchFeedback : Config.touchFeedback,
            touchFeedbackColor : Config.buttonRippleColor,
            touchEnabled : false
        });

        var buttonLabel = Ti.UI.createLabel({
            text : text.toUpperCase(),
            textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
            font : Config.buttonFont,
            color : Config.buttonTextColor,
            height : Ti.UI.SIZE,
            width : Ti.UI.SIZE,
            touchEnabled : false
        });

        okAction.addEventListener('click', function() {
            goSend();
        });

        buttonBox.add(okAction);
        buttonBox.add(buttonLabel);

        buttonBox.action = okAction;

        return buttonBox;
    };

    function removeViewButton() {

        var text = Config.nextText;

        var buttonBox = Ti.UI.createView({
            borderRadius : Config.buttonBorderRadius,
            height : Config.buttonHeight,
            width : Ti.UI.FILL,
            top : '48dp',
            left : '80dp',
            right : '80dp',
            bottom : '16dp',
            touchEnabled : false
        });

        var okAction = Ti.UI.createView({
            backgroundColor : Config.neutral,
            height : Ti.UI.FILL,
            width : Ti.UI.FILL,
            touchFeedback : Config.touchFeedback,
            touchFeedbackColor : Config.buttonRippleColor,
            touchEnabled : false
        });

        var buttonLabel = Ti.UI.createLabel({
            text : text.toUpperCase(),
            textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
            font : Config.buttonFont,
            color : Config.buttonTextColor,
            height : Ti.UI.SIZE,
            width : Ti.UI.SIZE,
            touchEnabled : false
        });

        okAction.addEventListener('click', function() {
            removeView();
        });

        buttonBox.add(okAction);
        buttonBox.add(buttonLabel);

        buttonBox.action = okAction;

        return buttonBox;
    };

    function goBack() {
        if (horizontalScroll.getCurrentPage() == 0) {
            _refresh();
            self.close();
        } else {
            horizontalScroll.movePrevious();
        }
    }

    function goNext() {
        horizontalScroll.moveNext();
    }

    function goSend() {
        formValues = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, _formMaster.identifier);
        db.sendForm(formValues);
        refresh();
    }

    function removeView() {
        horizontalScroll.removeView(horizontalScroll.viewToRemove);
    }

    function refresh() {
        task = db.selectTASKSbyID(_taskId);
        if (task == null) {
            _refresh();
            self.close();
        } else {
            formValues = db.selectFORMSbyVALUES(_taskId, _routeId, task.priority, _formMaster.identifier);
            if (formValues.sent == 1) {
                _refresh();
                self.close();
            }
        }
    }

    function openContact() {
        var Window = require('/ui/common/Support');
        new Window(refresh);
    }


    self.addEventListener('android:back', function(e) {
        e.cancelBubble = true;
        goBack();
    });

    construct();

    self.open();

}

module.exports = Window;
