function Config() {
}

Config.drawer = null;
Config.scrollable = null;

Config.isAndroid = Ti.Platform.osname == 'android' ? true : false;
Config.androidCode = Ti.App.Android.getAppVersionCode();

if (Config.isAndroid) {
	Config.device_type = 'android';
	Config.TiGoosh = require('ti.goosh');
} else {
	Config.device_type = 'ios';
}

Config.maxPictures = 5;
Config.images = '/images/';
Config.profile_pic = 'profile_pic.png';
Config.textSku= 'WRIN';

Config.checkinPictures= false;
Config.offloadStartPictures= false;
Config.deliveryPictures= false;
Config.manifestPictures= false;
Config.rejectionPictures= false;
Config.alertPictures= false;

// Config.getWin = function(win) {
// var folder = Ti.App.Properties.getObject('me', new Object).profile.replace('MOBILE_', '').replace('_PROFILE', '');
// return '/ui/' + folder + '/' + win;
// };
//
// Config.geProfile = function() {
// var profile = Ti.App.Properties.getObject('me', new Object).profile.replace('MOBILE_', '').replace('_PROFILE', '');
// return profile;
// };

Config.getPic = function() {
	if (Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, Config.profile_pic).exists()) {
		return Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, Config.profile_pic).read();
	} else {
		return Config.images+ '192_login_gris.png';
	}
};

Config.nextText = 'continuar';
Config.sendText = 'enviar';

Config.googleBlue = '#4885ed';
Config.textdarkgray = '#474746';
Config.textdarkgraymedium = '#a09f9f';
Config.textdarkgraylow = '#a09f9f';
Config.darkText= '#3C3C3C';

//Config.ax_color = '#01b2b5';

/*FOOD SERVICE ************/
Config.ax_color = '#0A425D';
Config.fs_colorLight = '#014667';
Config.fs_color = '#0A425D';
Config.fs_colorDark = '#023046';

Config.lightBlue= '#8CD3F5';
Config.whiteText= '#FFFFFF';
Config.circleColor= '#014667';

Config.borderGray= '#535353';
Config.button_color = Config.fs_colorDark;


Config.button_lines = '#F2683E';
Config.displayHt=Ti.Platform.displayCaps.platformHeight;
Config.displayWt=Ti.Platform.displayCaps.platformWidth;

Ti.API.info('DISPLAY HEIGHT: ' + Config.displayHt);
Ti.API.info('DISPLAY WIDTH: ' + Config.displayWt);
	
Config.colorBg= '/images/color_bg.webp';
Config.whiteBg= '/images/white_bg.webp';
Config.circle= '/images/circle_fs_.png';

/**************************/

Config.purple = '#781975';
Config.blue = '#0154a0';
Config.red = '#c1392b';
Config.green = '#54b858';


Config.orange = '#FF8000';
Config.yellow = '#c2b456';
Config.darkpurple = '#631a61';
Config.darkerpurple = '#4e134c';
Config.darkred = '#b91927';
Config.darkgreen = '#2e8f4b';
Config.darkorange = '#c57621';
Config.lightpurple = '#a872a7';
Config.darkerred = '#661b1b';
Config.darkerorange = '#754614';
Config.darkergreen = '#235723';

Config.black = '#000000';
Config.almostblack = '#151515';
Config.darkgray = '#333333';
Config.overgray = '#404040';
Config.somegray = '#474747';
Config.lightgray = '#818181';
Config.hintdarkgray = '#9f9f9f';
Config.hintgray = '#cccccc';
Config.somewhite = '#e5e5e5';
Config.notsowhite = '#f7f7f7';
Config.almostwhite = '#fbfbfb';
Config.white = '#fefefe';

Config.clearMask = '#00000000';
Config.whiteMask = '#C0ffffff';
Config.softMask = '#16000000';
Config.overMask = '#C0000000';

// Config.accent = Config.red;
Config.accent = Config.fs_color;
Config.success = Config.green;
Config.darksuccess = Config.red;
Config.warning = Config.yellow;
Config.danger = Config.red;
Config.info = Config.blue;
Config.neutral = Config.somegray;
Config.neutralSelected = Config.lightgray;

Config.orientation = [Ti.UI.PORTRAIT];
//Config.backgroundColor = Config.almostwhite;
Config.backgroundColor = 'white';
Config.containerBackgroundColor = Config.white;
Config.containerTextColor = Config.somegray;
Config.containerMediumTextColor = Config.lightgray;
Config.containerLightTextColor = Config.hintdarkgray;

Config.barHeight = '40dp';
Config.actionbarBackgroundColor = Config.accent;
Config.titleTextColor = Config.white;
Config.titleButtonColor = Config.white;
Config.barTouchFeedback = true;
Config.barRippleColor = Config.red;
Config.barButtonHeight = '32dp';
Config.barButtonBorderRadius = '16dp';
Config.shadowColor = Config.accent;
Config.shadowHeight = '4dp';

Config.sidemenuBackgroundColor = Config.darkgray;
Config.sidemenuBoxesColor = Config.overgray;
Config.sidemenuTextColor = Config.somewhite;
Config.menuActionbarBackgroundColor = Config.darkgray;
Config.menuActionbarTextColor = Config.white;
Config.menuBackgroundColor = Config.somewhite;
Config.menuTextColor = Config.somegray;

Config.subtitleFont = {
	fontSize : '18dp',
	fontWeight : 'bold'
};

Config.rowFont = {
	fontSize : '16dp'
};

Config.subtitleBackgroundColor = Config.white;
Config.subtitleTextColor = 'white';
Config.subtitleHeight = '32dp';
Config.subtitleBorderRadius = '8dp';
Config.underscoreHeight = '4dp';
Config.underscoreWidth = '32dp';

Config.inputBackgroundColor = Config.almostwhite;
Config.inputBackgroundSelectedColor = Config.almostwhite;
Config.inputBorderColor = Config.somewhite;
Config.inputHintColor = Config.hintgray;
Config.inputTextColor = 'white';
Config.inputFont = {
	fontSize : '14dp',
	fontWeight : 'normal'
};
Config.inputPadding = {
	top : 0,
	left : 24,
	right : 24,
	bottom : 0
};
Config.inputLabelMargin = Config.isAndroid ? '16dp' : '16dp';
Config.inputTextMargin = Config.isAndroid ? '13dp' : '13dp';
Config.inputPickerMargin = Config.isAndroid ? '8dp' : '8dp';
Config.inputHeight = Config.isAndroid ? '40dp' : '40dp';
Config.inputIconSize = Config.isAndroid ? '24dp' : '24dp';
Config.inputIconMargin = Config.isAndroid ? '8dp' : '8dp';

Config.biginputFont = {
	fontSize : '18dp',
	fontWeight : 'normal'
};
Config.biginputPadding = {
	top : 0,
	left : 32,
	right : 32,
	bottom : 0
};
Config.biginputLabelMargin = Config.isAndroid ? '16dp' : '16dp';
Config.biginputTextMargin = Config.isAndroid ? '13dp' : '13dp';
Config.biginputPickerMargin = Config.isAndroid ? '8dp' : '8dp';
Config.biginputHeight = Config.isAndroid ? '48dp' : '48dp';
Config.biginputIconSize = Config.isAndroid ? '32dp' : '32dp';
Config.biginputIconMargin = Config.isAndroid ? '8dp' : '8dp';

Config.biggerinputHeight = Config.isAndroid ? '64dp' : '64dp';
Config.biggerinputFont = {
	fontSize : '20dp',
	fontWeight : 'bold'
};

Config.checkBoxRadius = Config.isAndroid ? '4dp' : '4dp';
Config.checkBoxSize = Config.isAndroid ? '32dp' : '32dp';
Config.checkIconSize = Config.isAndroid ? '24dp' : '24dp';

Config.radioBoxRadius = Config.isAndroid ? '16dp' : '16dp';
Config.radioBoxSize = Config.isAndroid ? '32dp' : '32dp';
Config.radioIconRadius = Config.isAndroid ? '8dp' : '8dp';
Config.radioIconSize = Config.isAndroid ? '16dp' : '16dp';

Config.font24 = {
	fontSize : '24dp',
	fontWeight : 'bold'
};

Config.font28 = {
	fontSize : '28dp',
	fontWeight : 'bold'
};

Config.font32 = {
	fontSize : '32dp',
	fontWeight : 'bold'
};

Config.font36 = {
	fontSize : '36dp',
	fontWeight : 'bold'
};

Config.optionsBackground = Config.almostwhite;
Config.optionsSelectedBackground = Config.accent;
Config.optionsSeparatorColor = Config.somewhite;
Config.optionsRowHeight = '100dp';
Config.optionsTitleColor = Config.somegray;
Config.optionsTitleFont = {
	fontSize : '14dp',
	fontWeight : 'normal'
};
Config.optionsMessageColor = Config.somegray;
Config.optionsMessageFont = {
	fontSize : '12dp'
};

Config.touchFeedback = true;
Config.buttonBackgroundColor = '#F2683E';
//Config.buttonRippleColor = Config.white;
Config.buttonRippleColor = Config.orange;
Config.buttonTextColor = Config.white;
Config.buttonHeight = '48dp';
Config.buttonBorderRadius = '20dp';
Config.buttonFont = {
	fontSize : '16dp',
	fontWeight : 'bold'
};

Config.bigborderRadius = '16dp';
Config.borderRadius = '2dp';
Config.borderWidth = '1dp';
Config.elevation = 4;

Config.fileDocs = 'docs';
Config.fileImages = 'images';
Config.fileVideos = 'videos';

Config.mapareaStrokeWidth = Config.isAndroid ? 5 : '2dp';
Config.mapareaStrokeColor = '#0154a0';
Config.mapareaFillColor = '#160154a0';

Config.AppVersion = Titanium.App.getVersion();
Config.ubqti_api_version = '2';
Config.densityScreen = Titanium.Platform.displayCaps.density;

var dev = 'https://axionlog-mobile-test.ubqti.com/';
var dev2 = 'http://192.168.0.16:5000/';
var prod = 'https://axionlog-mobile.ubqti.com/';

//Config.SOCKET_URL = wsDev;
Config.DOMAIN_URL = dev;

// true = dev ; false = prod
Config.mode = true;
Config.Home = 'Home';

if (Config.DOMAIN_URL != 'https://axionlog-mobile.ubqti.com/') {
	Config.mode = true;
	Config.Home = 'DEV MODE';
	// Config.actionbarBackgroundColor = Config.accent;
	// Config.titleTextColor = Config.textdarkgray;
}

Config.SERVER_BASE_URL = Config.DOMAIN_URL + 'api/mobile/';
Config.DATABASE_NAME = 'ubqtidb';
Ti.App.Properties.setString('DATABASE_NAME', Config.DATABASE_NAME);

Config.KEYVAL_INIT = {
	ubqti_api_version : '',
	ubqti_api_token : '',
	SERVER_BASE_URL : '',
	POST_LOC_URL : '',
	SAVE_LOC_IF : '',
	SAVE_LOC_TO : '',
	SAVE_LOC_TYPE : '',
	GET_REFRESH_URL : '',
	GET_REFRESH_IF : '',
	SAVE_REFRESH_TO : '',
	SAVE_REFRESH_KEYS : '',
	READY_TO_SEND_FROM : '',
	user_id : '',
	process_date : '',
	last_loc : ''
};

Config.KEYVAL_STATIC = {
	ubqti_api_version : Config.ubqti_api_version,
	SERVER_BASE_URL : Config.SERVER_BASE_URL,
	POST_LOC_URL : Config.SERVER_BASE_URL + 'queue_master_event',
	SAVE_LOC_IF : 'process_date',
	SAVE_LOC_TO : 'geoloc',
	SAVE_LOC_TYPE : 'loc',
	GET_REFRESH_URL : Config.SERVER_BASE_URL + 'queue',
	GET_REFRESH_IF : 'ubqti_api_token',
	SAVE_REFRESH_TO : 'routes',
	SAVE_REFRESH_KEYS : '["id", "qc_id", "qm_id"]',
	READY_TO_SEND_FROM : 'readytosend'
};

Config.KEYVAL_RESET = {
	ubqti_api_token : '',
	last_loc : ''
};

if (Config.isAndroid) {
	Config.softInput = Ti.UI.Android.SOFT_INPUT_STATE_ALWAYS_HIDDEN | Ti.UI.Android.SOFT_INPUT_ADJUST_RESIZE;
	Config.hideKeyboard = Ti.UI.Android.SOFT_KEYBOARD_HIDE_ON_FOCUS;
	Config.showKeyboard = Ti.UI.Android.SOFT_KEYBOARD_SHOW_ON_FOCUS;
} else {
	Config.softInput = '';
	Config.hideKeyboard = '';
	Config.showKeyboard = '';
}

Config.globalDialog = function(msg) {

	var dialog = Titanium.UI.createAlertDialog({
		title : 'Atención',
		message : msg
	});

	dialog.show();
};

Config.fullW = Ti.Platform.displayCaps.getPlatformWidth() / Ti.Platform.displayCaps.getLogicalDensityFactor();

module.exports = Config;
