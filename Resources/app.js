var Config = require('/libs/Config');
var summon = require('/mods/summon');
var xhr = require('/mods/xhr');
var db = require('/mods/db');
var Permissions = require('/libs/Permissions');

db.checkDB();

(function() {

    var win = Ti.UI.createWindow({
        navBarHidden : true,
        exitOnClose : false,
        windowSoftInputMode : Config.softInput,
        backgroundColor : Config.fs_color,
        //backgroundImage : '/images/color_bg.png',
        backgroundImage : Config.images+ 'color_bg.png',
        orientationModes : Config.orientation,
    });

    var work = Ti.UI.createView({
        overrideCurrentAnimation : true,
        backgroundColor : Config.overMask,
        borderRadius : '10dp',
        height : '160dp',
        width : '160dp'
    });

    var indicator = Ti.UI.createActivityIndicator({
        style : Config.isAndroid ? Ti.UI.ActivityIndicatorStyle.BIG_DARK : Ti.UI.ActivityIndicatorStyle.DARK,
        height : '120dp',
        width : '120dp'
    });
    indicator.show();

    work.add(indicator);
    work.hide();

    win.add(work);

    function blur() {
        if (Config.mode) {
            Ti.API.info('win.blur');
        }
        win.focus = false;
    }

    function focus() {
        if (Config.mode) {
            Ti.API.info('win.focus');
        }
        win.focus = true;
    }

    function gotPermissions() {
        if (Config.mode) {
            Ti.API.log('gotPermissions');
        }
        win.removeEventListener('blur', blur);
        win.removeEventListener('focus', focus);

        if (Config.isAndroid) {
            Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_HIGH;
        } else {
            Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_BEST;
        }

        if (Ti.App.Properties.getObject('app_version_2', null) != null) {
            validate(true);
        } else {
            work.show();
            xhr.app_version(validate);
        }

    }

    function validate(status) {

        work.hide();

        if (status) {

            var mobileSetting = Ti.App.Properties.getObject('app_version_2', null).mobile;

            if (mobileSetting['validate']) {
                //
                // if (mobileSetting['mandatory']) {
                //
                // if (Config.androidCode < mobileSetting['version']) {
                //
                // var msg = 'Para continuar debe actualizar la aplicación.';
                // versionDialog(msg, true, mobileSetting['store_url']);
                //
                // } else {
                // startApp();
                // }
                // } else {
                //
                // if (Config.androidCode < mobileSetting['version']) {
                //
                // var msg = 'Hay una nueva version disponible de la aplicación';
                // versionDialog(msg, false, mobileSetting['store_url']);
                //
                // } else {
                startApp();
                //				}
                //				}

            } else {

                startApp();

            }

        } else {

            var exit = Ti.UI.createAlertDialog({
                buttonNames : ['Salir'],
                message : 'Por favor revise su conexión a internet y vuelva a intentarlo.',
                title : 'Error en la descarga de datos'
            });

            exit.addEventListener('click', function(e) {
                win.close();
            });

            exit.show();

        }

    }

    function versionDialog(txtDialog, isMandatory, storeUrl) {

        // switch(isMandatory) {
        // case true:
            // var buttons = ['Actualizar App'];
            // closeApp = true;
            // break;
        // case false:
            // var buttons = ['Actualizar App', 'Cancelar'];
            // closeApp = false;
            // ;
            // break;
        // }
// 
        // var dialog = Ti.UI.createAlertDialog({
            // cancel : 1,
            // buttonNames : buttons,
            // message : txtDialog,
            // title : 'Atención'
        // });
// 
        // dialog.addEventListener('click', function(e) {
            // if (e.cancel) {
// 
                // if (isMandatory == false) {
                    // startApp();
                // }
// 
                // if (isMandatory && closeApp) {
                    // win.close();
                // }
// 
                // if (isMandatory && closeApp == false) {
                    // startApp();
                // }
// 
                // return;
// 
            // } else {
// 
                // Ti.Platform.openURL(storeUrl);
                // win.close();
// 
            // }
        // });

        dialog.show();
    }

    function startApp() {

        // var Window = require('/ui/BasicWindow');
        // new Window();

        Ti.API.info('APP ME: ' + JSON.stringify(Ti.App.Properties.getObject('me', null)));

        if (Ti.App.Properties.getObject('me', null) != null) {

            db.updateKEYVAL({
                ubqti_api_token : Ti.App.Properties.getObject('me', new Object).session_token
            });
            var Window = require('/ui/Menu');
            new Window();
            win.close();

        } else {

            db.updateKEYVAL({
                ubqti_api_token : ''
            });
            var Window = require('/ui/Login');
            new Window();
            win.close();

        }

    }


    win.addEventListener('blur', blur);
    win.addEventListener('focus', focus);

    win.addEventListener('open', function(e) {
        if (Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_ALWAYS) == true && Ti.Media.hasCameraPermissions() == true && Ti.Contacts.hasContactsPermissions() == true) {
            gotPermissions();
        } else {
            Permissions.getAll(win, gotPermissions);
        }
    });

    win.open();

})();
